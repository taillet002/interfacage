
import os
import time

import cv2
import numpy as np


class webCam():
    def __init__(self, camId=0, pathPictures="", prefixName=''):
        self.isConnected = False
        self.cam = None
        self.pathPictures = pathPictures
        self.prefixName = prefixName
        self.camId = camId

    def connect(self):
        try:
            self.cam = cv2.VideoCapture(self.camId, cv2.CAP_DSHOW)
            if self.cam.isOpened():
                self.isConnected = True
                return True
            else:
                return False
        except Exception as e:
            print("Camera - ", e)
            return False

    def capture(self, n=1, name=''):
        # we connect to the camera here so that we dont have two camera open at the same time because that causes only the first camera to open and the second one to fail
        self.connect()
        time.sleep(2)   # wait for the camera sensor to stabilize the reading
        if self.isConnected:

            try:
                img_counter = 1
                ret, frame = self.cam.read()
                while True:
                    ret, frame = self.cam.read()
                    #cv2.imshow("test", frame)
                    if not ret:
                        return False

                    if img_counter > n:
                        break
                    else:
                        img_name = self.prefixName + name + '-' + str(img_counter) + '.png'
                        time.sleep(0.5)
                        print(os.path.join(os.getcwd(), self.pathPictures, img_name))
                        if cameraId == 1:
                            cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE, frame)
                        cv2.imwrite(os.path.join(os.getcwd(), self.pathPictures, img_name), frame)
                        # print("{} written!".format(img_name))
                        img_counter += 1
                self.disconnect()
                return True
            except Exception as e:
                print("Camera - ", e)
                return False
        else:
            return False

    def disconnect(self):
        try:
            self.cam.release()
            self.isConnected = False
        except Exception as e:
            print("Camera - ", e)


    def __del__(self):
        self.disconnect()


    def keyCapture(self):
        cv2.namedWindow("test")
        while True:
            ret, frame = self.cam.read()
            cv2.imshow("test", frame)
            if not ret:
                break
            k = cv2.waitKey(1)
            if k % 256 == 27:
                # ESC pressed
                break
        cv2.destroyAllWindows()


    def configCam(self, policy='fromUser'):
        if policy == 'fromUser':    # Only used to see which values could be useful. User still has to enter th values below for consistent pictures
            self.cam.set(cv2.CAP_PROP_SETTINGS, 0.0)
        else:
            if self.camId == 0:     # Samples picture
                self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, 960)
                self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

            elif self.camId == 1:      # droplet pictures
                self.cam.set(cv2.CAP_PROP_FRAME_WIDTH, 800)
                self.cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 800)


if __name__ == "__main__":
    cameraId = input("Which Camera's settings would you like to change: (phase/droplet) ")
    if "phase" in cameraId.lower():
        idCam = 0
    elif "drop" in cameraId.lower():
        idCam = 1
    something = webCam(camId=idCam)
    something.connect()
    something.configCam('fromUser')
    something.keyCapture()
    something.disconnect()
