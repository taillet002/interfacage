"""
In this file, all the functions will be created to be used
when programming on:
Universal Robot -> UR3e
Robotiq -> Hand-E and 2F-85  Gripper along with Force Torque Sensor.
RG6 gripper -> simple sysaxes gripper
"""

import os
import sys
import time
import socket
import struct
import threading
import numpy as np
from tkinter import *
from datetime import datetime
from functools import partial
from math import degrees, radians

class UR3e():

    def __init__(self, HOST, PORT=30003):
        ''' Enter the ip address of the robot.
            Use PORT 30001 to only read data stream,
            Use PORT 30002 to send URScript commands,
            Use PORT 30003 to control in Real Time,
            Use PORT 30013 for Secondary connection to get Real Time read only data
            Use PORT 29999 for connection to Dashboard which allows to enable robot after protective stop

            https://www.universal-robots.com/how-tos-and-faqs/how-to/ur-how-tos/remote-control-via-tcpip-16496/

            For all PORTS that can be used:
            https://www.universal-robots.com/how-tos-and-faqs/how-to/ur-how-tos/overview-of-client-interfaces-21744/
        '''
        self.HOST = HOST  # IP address of the Robot
        self.PORT = PORT  # Port the server is running
        self.secondaryPORT = 30013
        self.dashboardPORT = 29999

        self.homePos = [0.0, -300.0, 200.0, 180.0, 0.0, 0.0]
        self.protectiveStop = 0

        # Establish connection with robot
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.s.connect((self.HOST, self.PORT))
            time.sleep(0.5)
            if not self.robotSafetyModeIs_NORMAL():
                self.turnONRobotAndReleaseBrakes()
                self.enableRobotAfter_PROTECTIVE_STOP()
            # self.movePoseJoint(self.homePos)      # JB - I don't want to take the risk to move to home automaticaly
            print('UR3e connected')
        except socket.error:
            print('UR3e not connected !')

    def __del__(self):
        self.disconnectfromRobot()

    ## MOVE FUNCTIONS
    def move_p_Linear(self, pos: list, a=0.1, v=0.1, t=0, r: float = 0):
        """ ENTER POSITIONS IN X,Y,Z,RX,RY,RZ - Move to P positions, in mm, using Linear type
            Higher chance of getting into Singularity state ? Certainly
            T is time to perform the movement. TIME takes priority over A and V.
            R is the blend radius for smoothness between 2 mouvements (not working with TCP connection !)
        """
        x = self.convertMillimeterToMeter(pos[0])
        y = self.convertMillimeterToMeter(pos[1])
        z = self.convertMillimeterToMeter(pos[2])
        rx = radians(pos[3])
        ry = radians(pos[4])
        rz = radians(pos[5])
        r = self.convertMillimeterToMeter(r)

        cmd = f'movel(p[{x}, {y}, {z}, {rx}, {ry}, {rz}], {a}, {v}, {t}, {r})'.encode() + b'\n'
        self.s.sendall(cmd)
        # self.waitArmMoving()  # <-- # JB - I prefere to let the user use this or not in his project code

    def relative_move_p_Linear(self, d, axis, a=0.1, v=0.1, t=0, r=0):
        ''' relative movevement from current position using move_p_Linear
            d: distance of relative move, in mm for (x,y,z) or in degrees for (rx,ry,rz)
            axis: x, y, z, rx, ry or rz depending on what direction/rotation you want to move
        '''
        p = self.getCurrentPosition(native=True)
        if axis == 'x':
            d = self.convertMillimeterToMeter(d)  # tranform mm to meters
            p[0] = p[0] + d
        elif axis == 'y':
            d = self.convertMillimeterToMeter(d)  # tranform mm to meters
            p[1] = p[1] + d
        elif axis == 'z':
            d = self.convertMillimeterToMeter(d)  # tranform mm to meters
            p[2] = p[2] + d
        elif axis == 'rx':
            d = radians(d)                        # converts deg to rad
            p[3] = p[3] + d
        elif axis == 'ry':
            d = radians(d)                        # converts deg to rad
            p[4] = p[4] + d
        elif axis == 'rz':
            d = radians(d)                        # converts deg to rad
            p[5] = p[5] + d
        else:
            print('axis available are: x, y, z, rx, ry or rz. no movement done here !')
            return
        cmd = f'movel(p[{p[0]}, {p[1]}, {p[2]}, {p[3]}, {p[4]}, {p[5]}], {a}, {v}, {t}, {r})'.encode() + b'\n'
        self.s.sendall(cmd)

    def move_q_Linear(self, pos: list, a=0.1, v=0.1, t=0, r: float = 0):
        """ ENTER POSITIONS IN motors degrees [base, shoulder, elbow, wrist1, wrist2, wrist3]
            Move to Q positions, in degrees, using Linear type
            T is time to perform the movement. TIME takes priority over A and V. R is the blend radius
        """
        pos = [radians(i) for i in pos]  # from degrees to radians (default)
        r = self.convertMillimeterToMeter(r)

        cmd = f'movel([{pos[0]}, {pos[1]}, {pos[2]}, {pos[3]}, {pos[4]}, {pos[5]}], {a}, {v}, {t}, {r})'.encode() + b'\n'
        self.s.sendall(cmd)

    def move_p_Joint(self, pos: list, a=0.1, v=0.1, t=0, r: float = 0):
        """ ENTER POSITIONS IN X,Y,Z,RX,RY,RZ - Move to P positions, in mm, using Joint type
        """
        x = self.convertMillimeterToMeter(pos[0])
        y = self.convertMillimeterToMeter(pos[1])
        z = self.convertMillimeterToMeter(pos[2])
        rx = radians(pos[3])
        ry = radians(pos[4])
        rz = radians(pos[5])
        r = self.convertMillimeterToMeter(r)

        cmd = f'movej(p[{x}, {y}, {z}, {rx}, {ry}, {rz}], {a}, {v}, {t}, {r})'.encode() + b'\n'
        self.s.sendall(cmd)
        # self.waitArmMoving()  # <-- # JB - I prefere to let the user use this or not in his project code

    def move_q_Joint(self, pos: list, a=0.1, v=0.1, t=0, r: float = 0):
        """ ENTER POSITIONS IN motors degrees [base, shoulder, elbow, wrist1, wrist2, wrist3]
            Move to Q positions, in degrees, using Joint type
        """
        pos = [radians(i) for i in pos]  # from degrees to radians (default)
        r = self.convertMillimeterToMeter(r)

        cmd = f'movej([{pos[0]}, {pos[1]}, {pos[2]}, {pos[3]}, {pos[4]}, {pos[5]}], {a}, {v}, {t}, {r})'.encode() + b'\n'
        self.s.sendall(cmd)

    def relative_move_q_Joint(self, deg, axis, a=0.1, v=0.1, t=0, r=0):
        ''' relative rotation from current position using move_q_Joint
            deg: rotation of relative move, in degrees
            axis: base, shoulder, elbow, wrist1, wrist2 or wrist3 depending on what motor you want to move
        '''
        q = self.getCurrentJointsPos()
        rad = radians(deg)
        if axis == 'base':
            q[0] = q[0] + rad
        elif axis == 'shoulder':
            q[1] = q[1] + rad
        elif axis == 'elbow':
            q[2] = q[2] + rad
        elif axis == 'wrist1':
            q[3] = q[3] + rad
        elif axis == 'wrist2':
            q[4] = q[4] + rad
        elif axis == 'wrist3':
            q[5] = q[5] + rad
        else:
            print('axis available are: base, shoulder, elbow, wrist1, wrist2 or wrist3. no movement done here !!!')
            return
        cmd = f'movej([{q[0]}, {q[1]}, {q[2]}, {q[3]}, {q[4]}, {q[5]}], {a}, {v}, {t}, {r})'.encode() + b'\n'
        self.s.sendall(cmd)

    def MoveInverseKin(self, p, q, a=0.1, v=0.1, t=0):
        ''' send move using Joint type movement with get_inv_kin function,
        that uses cartesian (p) and joint (q) positions to calculate movement
        p and q given in mm and degrees !
        '''
        p[0] = self.convertMillimeterToMeter(p[0])
        p[1] = self.convertMillimeterToMeter(p[1])
        p[2] = self.convertMillimeterToMeter(p[2])
        p[3], p[4], p[5] = radians(p[3]), radians(p[4]), radians(p[5])

        q = [radians(i) for i in q]

        r = self.convertMillimeterToMeter(r)

        cmd_sent = f'movej(get_inverse_kin(p[{p[0]}, {p[1]}, {p[2]}, {p[3]}, {p[4]}, {p[5]}], qnear=[{q[0]}, {q[1]}, {q[2]}, {q[3]}, {q[4]}, {q[5]}]), a={a}, v={v}, t={t})'.encode() + b'\n'
        self.s.sendall(cmd_sent)

    def pose_trans_p_L(self, pos: list, a=1.2, v=0.25, t=0, r=0):
        ''' send move linéar with pse_trans and get_forward ...
            see page 63 of UR3e manual. not sure what it does...
        '''
        pos[0] = self.convertMillimeterToMeter(pos[0])
        pos[1] = self.convertMillimeterToMeter(pos[1])
        pos[2] = self.convertMillimeterToMeter(pos[2])
        p[3], p[4], p[5] = radians(p[3]), radians(p[4]), radians(p[5])
        r = self.convertMillimeterToMeter(r)
        cmd_sent = f'movel(pose_trans(get_forward_kin(), p[{x},{y},{z},{rx},{ry},{rz}]), {a}, {v}, {t}, {r})'.encode() + b"\n"
        self.s.sendall(cmd_sent)

    ## GET FUNCTIONS
    def get_from_robot(self, idxStart, nValues, restart=True):
        ''' base script to get informations from robot.
        take RealTime 5.4 sheet. Add row 1 to n-1 of 'Size in bytes' column to get the idxStart number and 'Number of values' column for nValues
        You can find descriptions in Client_Interface_V3.10andV5.4.xlsx
        More info - http://www.zacobria.com/universal-robots-knowledge-base-tech-support-forum-hints-tips/knowledge-base/client-interfaces-cartesian-matlab-data/
        restart = True, mandatory !!! if False, value not updated
        '''
        idxEnd = idxStart + (nValues * 8)
        if restart:
            c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            c.connect((self.HOST, self.secondaryPORT))
        else:
            c = self.s
        c.recv(idxStart)
        i = idxStart
        ret_val = []
        while i < idxEnd:
            pack = c.recv(8)
            message_size = str(pack)
            message_size = struct.unpack('!d', pack)[0]
            # print(message_size)
            ret_val.append(message_size)
            i += 8
        c.recv(1108 - idxEnd)
        return ret_val

    def getCurrentPosition(self, native=True):
        ''' Get Current P positions [x, y, z, rx, ry, rz]
        if native = True: returns positions in native format (meters and radians)
        if native = False: returns positions in millimeters and degrees '''
        ret_val = self.get_from_robot(444, 6, restart=True)
        if not native:
            ret_val[0] = self.convertMeterToMillimeter(ret_val[0])
            ret_val[1] = self.convertMeterToMillimeter(ret_val[1])
            ret_val[2] = self.convertMeterToMillimeter(ret_val[2])
            ret_val[3] = degrees(ret_val[3])
            ret_val[4] = degrees(ret_val[5])
            ret_val[5] = degrees(ret_val[5])
        return ret_val

    def getCurrentJointsPos(self, native=True):
        ''' Get Current Q positions [base, shoulder, elbow, wrist1, wrist2 or wrist3]
        if native = True: returns positions in native format (radians)
        if native = False: returns positions in degrees '''
        ret_val = self.get_from_robot(252, 6)
        if not native:
            ret_val = [degrees(i) for i in ret_val]
        return ret_val

    def getRobotMode(self):
        """ Get robot modes
        -1		ROBOT_MODE_NO_CONTROLLER
        0		ROBOT_MODE_DISCONNECTED
        1		ROBOT_MODE_CONFIRM_SAFETY
        2		ROBOT_MODE_BOOTING
        3		ROBOT_MODE_POWER_OFF
        4		ROBOT_MODE_POWER_ON
        5		ROBOT_MODE_IDLE
        6		ROBOT_MODE_BACKDRIVE
        7		ROBOT_MODE_RUNNING
        8		ROBOT_MODE_UPDATING_FIRMWARE """
        ret_val = self.get_from_robot(756, 1, restart=True)
        return ret_val

    def getToolAcceleration(self):
        """ Get tool acceleration """
        ret_val = self.get_from_robot(868, 3)
        return ret_val

    def getActualJointVelocity(self):
        '''  get actual joint (base, elbow, wrist1, ...) velocity '''
        ret_val = self.get_from_robot(300, 6)
        return ret_val

    def getSafetyMode(self):
        '''  get safety mode                    Value  CB3  e-Series
            SAFETY_MODE_UNDEFINED_SAFETY_MODE	    11	X	X
            SAFETY_MODE_VALIDATE_JOINT_ID	        10		X
            SAFETY_MODE_FAULT	                    9	X	X
            SAFETY_MODE_VIOLATION	                8	X	X
            SAFETY_MODE_ROBOT_EMERGENCY_STOP	    7	X	X
            SAFETY_MODE_SYSTEM_EMERGENCY_STOP	    6	X	X
            SAFETY_MODE_SAFEGUARD_STOP	            5	X	X
            SAFETY_MODE_RECOVERY	                4	X	X
            SAFETY_MODE_PROTECTIVE_STOP	            3	X	X
            SAFETY_MODE_REDUCED	                    2	X	X
            SAFETY_MODE_NORMAL	                    1	X	X '''
        ret_val = self.get_from_robot(812, 1)
        return ret_val

    def getTargetPosition(self):
        """ Get Target Position """
        ret_val = self.get_from_robot(588, 6, restart=True)
        return ret_val

    def getTCPSpeed(self):
        """ Get Speed of TCP in mm/s
        """
        ret_val = self.get_from_robot(492, 6, restart=True)
        return ret_val

    def getTCPForce(self):
        """ Get Force being applied at the Tool Center Point (TCP)
        """
        ret_val = self.get_from_robot(540, 6, restart=True)
        return ret_val

    ## WAIT FUNCTIONS
    def velocity_near_zero(self, joint_vect, threshold=1e-02):
        ''' check if velocity is near 0 (delta: 1e-04 very safe, 1e-02 hmm risky) '''
        return any([abs(i) > 1e-02 for i in joint_vect])

    def isBusy(self, threshold=1e-02):
        ''' check if robot is busy or not. Was not able to implement 'is_steady()' function '''
        return self.velocity_near_zero(self.getActualJointVelocity(), threshold)

    def WaitforMovement(self, threshold=1e-02):
        ''' wait for arm to finish his mouvement '''
        is_busy = self.isBusy(threshold)
        while is_busy:
            is_busy = self.isBusy(threshold)

    def waitArmMoving(self, threshold=2, terminateScriptIfNotNormal: bool = False):
        """ killing time to make sure the arm stopped before moving to next step"""
        busy = True
        while busy:
            if self.robotSafetyModeIs_NORMAL():
                if any([abs(i) > threshold for i in self.getTCPSpeed()]):
                    busy = True
                else:
                    busy = False
            else:
                print("Robotic Arm stopped due to error/crash!")
                if terminateScriptIfNotNormal:
                    print("Terminating Script")
                    sys.exit()  # terminate script as there is no reason to continue if the arm stopped
                else:
                    if self.protectiveStop < 1:
                        self.enableRobotAfter_PROTECTIVE_STOP()
                        self.movePoseJoint(self.homePos)
                        self.protectiveStop += 1
                    else:
                        print("Protective Stop hit Again. Terminating script now!")
                        sys.exit()

    ## Robotiq gripper FUNCTIONS
    def zeroTCPForce(self):
        """ Zeroes the TCP Force
        """
        self.s.sendall("zero_ftsensor()".encode() + b"\n")
        time.sleep(1)

    def forceSearch(self, direction='z', force=5, maxDist=10):
        """ STILL WORKING ON THIS
            Move in TCP axis till force is reached or maximum distance is reached
            Force in Newton and distance in mm
        """
        # need to zero TCP before to make sure the values are good
        self.zeroTCPForce()

        currentPos = self.getCurrentPosition()
        currentForce = self.getTCPForce()

        if direction.lower() == 'z':
            pass

    def gripperActivate(self):
        """ Activate gripper.
        """
        with open(os.path.dirname(os.path.realpath(__file__)) + "/Robotiq_init/Gripper.script", "rb") as f:
            for l in iter(partial(f.read, 1024), b''):
                self.s.sendall(l)
            self.s.sendall("  rq_activate_and_wait()".encode() + b"\n")
            self.s.sendall("  rq_open_and_wait()".encode() + b'\n')
            self.s.sendall("end".encode() + b"\n")
        time.sleep(2.5)

    def gripperNormPositionandWait(self, position):
        ''' Position normalized. 0% to 100%. 0 is fully open and 100 is fully closed.
        '''
        with open(os.path.dirname(os.path.realpath(__file__)) + "/Robotiq_init/Gripper.script", "rb") as f:
            for l in iter(partial(f.read, 1024), b''):
                self.s.sendall(l)
            self.s.sendall("  rq_move_and_wait_norm({})".format(
                position).encode() + b"\n")
            self.s.sendall("end".encode() + b"\n")
        time.sleep(2.5)

    def gripper_mm_PositionandWait(self, position, gripperType):
        ''' Position in millimeters 0 to whatever size your gripper is. 0 is fully closed.

            The max value for Robotiq Hand E standard fingers is 52.0 mm

            The max value for Robotiq 2F-85 standard fingers is 85.0 mm
        '''
        if gripperType == 'hand_e':
            openDist = 52.0
        elif gripperType == "2f-85":
            openDist = 85.0
        pos_in_mm = round(100-((position/openDist)*100))
        with open(os.path.dirname(os.path.realpath(__file__)) + "/Robotiq_init/Gripper.script", "rb") as f:
            for l in iter(partial(f.read, 1024), b''):
                self.s.sendall(l)
            self.s.sendall("  rq_move_and_wait_norm({})".format(
                pos_in_mm).encode() + b"\n")
            self.s.sendall("end".encode() + b"\n")
        time.sleep(2.5)

    def gripperOpenandWait(self):
        ''' Gripper will Open and Wait for the action to complete
        '''
        with open(os.path.dirname(os.path.realpath(__file__)) + "/Robotiq_init/Gripper.script", "rb") as f:
            for l in iter(partial(f.read, 1024), b''):
                self.s.sendall(l)
            self.s.sendall("  rq_open_and_wait()".encode() + b"\n")
            self.s.sendall("end".encode() + b"\n")
        time.sleep(2.5)

    def gripperCloseandWait(self):
        ''' Gripper will Close and Wait for the action to complete
        '''
        with open(os.path.dirname(os.path.realpath(__file__)) + "/Robotiq_init/Gripper.script", "rb") as f:
            for l in iter(partial(f.read, 1024), b''):
                self.s.sendall(l)
            self.s.sendall("  rq_close_and_wait()".encode() + b"\n")
            self.s.sendall("end".encode() + b"\n")
        time.sleep(2.5)

    def linearSearch(self, direction="Z+", force=10, speed=0.003, max_distance=10, timeDelay=5):
        """ This is used to find surface with a given force and maximum travel distance.
            Direction default is Z+ to find the surface in the Positive Z direction (downward)
            Force is in N
            Speed is in m/s
            max_distance is in mm
            Give enough time delay to make sure the surface is found as there is no feedback to see if the surface was found or not.
        """
        max_distance = self.convertMillimeterToMeter(max_distance)
        with open(os.path.dirname(os.path.realpath(__file__)) + "/Robotiq_init/Force_torque_sensor.script", "rb") as f:
            for l in iter(partial(f.read, 1024), b''):
                self.s.sendall(l)
            self.s.sendall("  rq_linear_search_urcap({}, {}, {}, {})".format(
                direction, force, speed, max_distance).encode() + b"\n")
            self.s.sendall("end".encode() + b"\n")
        time.sleep(timeDelay)

    ## RG6 gripper FUNCTION
    def RG6Active(self, script_name, distance, force, payload, set_payload, depth_compensation, slave):
        ''' OnRobot RG6 Gripper opening / closing
            RG6(target_width=110, target_force=40, payload=0.0, set_payload=False, depth_compensation=False, slave=False)
        '''
        time.sleep(0.3)
        with open(script_name, "rb") as f:
            for l in iter(partial(f.read, 1024), b''):
                self.s.sendall(l)
            self.s.sendall(f'  RG6({distance},{force},{payload},{set_payload},{depth_compensation},{slave})'.encode() + b"\n")
            self.s.sendall("end".encode() + b"\n")
            time.sleep(0.7)

    ## freedrive mode function
    def setInterval(interval):
        ''' kind of threading thing that is hard to understand !!!
        it runs the infinite_fdm() function every seconds '''
        def decorator(function):
            def wrapper(*args, **kwargs):
                stopped = threading.Event()

                def loop():  # executed in another thread
                    while not stopped.wait(interval):  # until stopped
                        function(*args, **kwargs)

                t = threading.Thread(target=loop)
                t.daemon = True  # stop if the program exits
                t.start()
                return stopped
            return wrapper
        return decorator

    @setInterval(1)
    def infinite_fdm(self):
        ''' sends freedrive mode command to active free drive mode '''
        self.s.sendall("freedrive_mode()".encode() + b"\n")
        print('freedrive_mode activated')

    ## SIMPLE UI
    def UI_move(self, script_name):
        '''
        :return: small UI to allow user to move the arm.
        program blocked when UI is opened
        '''
        window = Tk()
        window.title("UR3 UI move")
        window.configure(background='white')
        window.geometry('300x350')
        lbl = Label(window)
        lbl.grid(column=0, row=0)
        stopfd = []

        def clicked(axis, sens):
            distance = float(ent_d.get())
            if sens == 'neg':
                distance = -distance
            speed = ent_speed.get()
            acc = ent_acc.get()
            print(f'{axis} - d: {distance} - speed: {speed} - acc: {acc}')
            self.relative_move_p_Linear(distance, f'{axis}', a=acc, v=speed)
            self.WaitforMovement(threshold=1e-04)
            # update position on buton text directly
            q_updated = self.getCurrentJointsPos(native=False)
            btn_base.config(text=f"base\n{q_updated[0]:.2f}")
            btn_shoulder.config(text=f"shoulder\n{q_updated[1]:.2f}")
            btn_elbow.config(text=f"elbow\n{q_updated[2]:.2f}")
            btn_w1.config(text=f"wrist 1\n{q_updated[3]:.2f}")
            btn_w2.config(text=f"wrist 2\n{q_updated[4]:.2f}")
            btn_w3.config(text=f"wrist 3\n{q_updated[5]:.2f}")

        def grip_action():
            distance = float(ent_grip.get())
            print(f'gripper - d: {distance}')
            self.RG6Active(script_name, distance, 40, 0.0, True, True, False)  # open gripper

        def print_pos():
            print('\n\nP position, in mm:')
            print(self.getCurrentPosition(native=False))
            print('\nQ positions in degrees:')
            print(self.getCurrentJointsPos(native=False))

        # top line
        lbl_speed = Label(window, text="speed:", height=3, width=7)
        lbl_speed.grid(column=0, row=0)
        ent_speed = Entry(window, width=7);
        ent_speed.grid(column=1, row=0);
        ent_speed.insert(0, '0.1')
        lbl_acc = Label(window, text="acc:", height=3, width=7);
        lbl_acc.grid(column=2, row=0)
        ent_acc = Entry(window, width=7);
        ent_acc.grid(column=3, row=0);
        ent_acc.insert(0, '0.1')
        btn_prt = Button(window, text="Print pos", command=print_pos, bg=("#%02x%02x%02x" % (62, 102, 57)), height=3, width=7)
        btn_prt.grid(column=4, row=0)

        # x, y, z, up, down, gripper
        btn_front = Button(window, text="front (-y)", command=partial(clicked, 'y', 'neg'), bg=("#%02x%02x%02x" % (80, 124, 166)), height=3, width=7)
        btn_front.grid(column=1, row=1)
        btn_back = Button(window, text="back (+y)", command=partial(clicked, 'y', 'pos'), bg=("#%02x%02x%02x" % (80, 124, 166)), height=3, width=7)
        btn_back.grid(column=1, row=3)
        btn_left = Button(window, text="left (+x)", command=partial(clicked, 'x', 'pos'), bg=("#%02x%02x%02x" % (80, 124, 166)), height=3, width=7)
        btn_left.grid(column=0, row=2)
        btn_right = Button(window, text="right (-x)", command=partial(clicked, 'x', 'neg'), bg=("#%02x%02x%02x" % (80, 124, 166)), height=3, width=7)
        btn_right.grid(column=2, row=2)
        btn_grip = Button(window, text="Gripper", command=grip_action, bg=("#%02x%02x%02x" % (250, 241, 147)), height=3, width=7)
        btn_grip.grid(column=4, row=2)
        ent_grip = Entry(window, width=7, bg=("#%02x%02x%02x" % (250, 241, 147)))
        ent_grip.grid(column=3, row=2)
        ent_grip.insert(0, '5')
        btn_up = Button(window, text="up (+z)", command=partial(clicked, 'z', 'pos'), bg=("#%02x%02x%02x" % (80, 124, 166)), height=3, width=7)
        btn_up.grid(column=4, row=1)
        btn_down = Button(window, text="down (-z)", command=partial(clicked, 'z', 'neg'), bg=("#%02x%02x%02x" % (80, 124, 166)), height=3, width=7)
        btn_down.grid(column=4, row=3)
        ent_d = Entry(window, width=7, bg=("#%02x%02x%02x" % (80, 124, 166)))
        ent_d.grid(column=1, row=2);
        ent_d.insert(0, '10')

        # freedrive mode
        def fd_ONOFF(state):
            global stopfd
            if state:
                stopfd = self.infinite_fdm()
            else:
                stopfd.set()

        btn_fd = Button(window, text="freedrive", command=partial(fd_ONOFF, True), bg=("#%02x%02x%02x" % (156, 82, 82)), height=3, width=7)
        btn_fd.grid(column=0, row=4)
        btn_fd = Button(window, text="stop", command=partial(fd_ONOFF, False), bg=("#%02x%02x%02x" % (156, 82, 82)), height=3, width=7)
        btn_fd.grid(column=0, row=5)

        # Base, Shoulder, Elbow, wrist1, wrist2, wrist3
        def clicked_joints(axis):
            angle = float(ent_bsewww.get())
            speed = ent_speed.get()
            acc = ent_acc.get()
            print(f'{axis} - angle: {angle} - speed: {speed} - acc: {acc}')
            self.relative_move_q_Joint(angle, axis, a=acc, v=speed)
            self.WaitforMovement(threshold=1e-04)
            # update position on buton text directly
            q_updated = self.getCurrentJointsPos(native=False)
            btn_base.config(text=f"base\n{q_updated[0]:.2f}")
            btn_shoulder.config(text=f"shoulder\n{q_updated[1]:.2f}")
            btn_elbow.config(text=f"elbow\n{q_updated[2]:.2f}")
            btn_w1.config(text=f"wrist 1\n{q_updated[3]:.2f}")
            btn_w2.config(text=f"wrist 2\n{q_updated[4]:.2f}")
            btn_w3.config(text=f"wrist 3\n{q_updated[5]:.2f}")

        lbl_bsewww = Label(window, text="angle in °", height=3, width=7, bg=("#%02x%02x%02x" % (212, 157, 196)))
        lbl_bsewww.grid(column=1, row=4)
        ent_bsewww = Entry(window, width=7, bg=("#%02x%02x%02x" % (212, 157, 196)));
        ent_bsewww.grid(column=1, row=5);
        ent_bsewww.insert(0, '5')
        btn_base = Button(window, text="base", command=partial(clicked_joints, 'base'), bg=("#%02x%02x%02x" % (212, 157, 196)), height=3, width=7)
        btn_base.grid(column=2, row=4)
        btn_shoulder = Button(window, text="shoulder", command=partial(clicked_joints, 'shoulder'), bg=("#%02x%02x%02x" % (212, 157, 196)), height=3, width=7)
        btn_shoulder.grid(column=3, row=4)
        btn_elbow = Button(window, text="elbow", command=partial(clicked_joints, 'elbow'), bg=("#%02x%02x%02x" % (212, 157, 196)), height=3, width=7)
        btn_elbow.grid(column=4, row=4)
        btn_w1 = Button(window, text="wrist1", command=partial(clicked_joints, 'wrist1'), bg=("#%02x%02x%02x" % (212, 157, 196)), height=3, width=7)
        btn_w1.grid(column=2, row=5)
        btn_w2 = Button(window, text="wrist2", command=partial(clicked_joints, 'wrist2'), bg=("#%02x%02x%02x" % (212, 157, 196)), height=3, width=7)
        btn_w2.grid(column=3, row=5)
        btn_w3 = Button(window, text="wrist3", command=partial(clicked_joints, 'wrist3'), bg=("#%02x%02x%02x" % (212, 157, 196)), height=3, width=7)
        btn_w3.grid(column=4, row=5)

        window.mainloop()

    ## rack definition functions
    def rack_pos_def(self, nRow, nCol, limits):
        '''
        Define all inner vials 'p' pos for a rack given:
        :param nRow: rack rows number
        :param nCol: rack columns number
        :param limits: [[upper-left], [bottom-left], [upper-right], [bottom-right}] 'p' positions
        :return: a list of all the 'p' positions of the rack [going from top left to bottom right]
        '''

        # positions rack definition
        xs, ys = np.meshgrid(np.linspace(0, 1, nCol), np.linspace(0, 1, nRow))

        # x corrections
        xs[0, :] = np.linspace(limits[0][0], limits[2][0], nCol)
        xs[-1, :] = np.linspace(limits[1][0], limits[3][0], nCol)
        for c in range(xs.shape[1]):
            xs[:, c] = np.linspace(xs[0, c], xs[-1, c], nRow)

        # y corrections
        ys[0, :] = np.linspace(limits[0][1], limits[2][1], nCol)
        ys[-1, :] = np.linspace(limits[1][1], limits[3][1], nCol)
        for c in range(ys.shape[1]):
            ys[:, c] = np.linspace(ys[0, c], ys[-1, c], nRow)

        # p_rack = [[],] * ((nRow*nCol))
        p_rack = [[] for i in range(nRow*nCol)]
        for i in range((nRow*nCol)):
            # print(f'\ni: {i} - x: {np.mod(i,4)}, u: {int(np.floor(i/4))}')
            if i == 0:
                # print('upper left')
                p_rack[i] = limits[0]
            elif i == nRow-1:
                # print('bottom left')
                p_rack[i] = limits[1]
            elif i == (nRow*nCol) - nRow:
                # print('upper right')
                p_rack[i] = limits[2]
            elif i == (nRow*nCol)-1:
                # print('bottom right')
                p_rack[i] = limits[3]
            else:
                p_rack[i] = [xs[np.mod(i,nRow)][int(np.floor(i/nRow))], ys[np.mod(i,nRow)][int(np.floor(i/nRow))],  limits[0][-4], limits[0][-3], limits[0][-2], limits[0][-1]]
            # print(p_rack[i])
        return p_rack

    def rack_p_other_z(self, rack_ref, z):
        '''
        From a rack p position list (given by rack_pos_def funciton for example) will create
        the same list with a different z.
        Usefull for the creation of safe positions for a given rack (safe = more high in z)
        :param rack_ref:
        :param z: heigh in mm
        :return: a list of all the 'p' positions of the rack [going from top left to bottom right]
        '''
        safe_z_rack_p = [[] for i in range(len(rack_ref))]
        for i in range(len(safe_z_rack_p)):
            safe_z_rack_p[i] = rack_ref[i].copy()
            safe_z_rack_p[i][2] = rack_ref[i].copy()[2] + z / 1000  # /1000 cause: from mm to meters
        return safe_z_rack_p

    ## OTHER FUNCTIONS
    def runScriptFile(self, t2CompleteAction=10):
        """ Use this to run already programmed .script file via Polyscope.
            But do keep in mind to change the time.sleep value to the time it takes for the file to run,
            else the script will not execute properly. Additionally, there are some concerns as to if the
            script will execute properly or not as according to some documentation it seems that
            all the commands will be sent without any wait and it might result in not completion of some commands. But that has to be tested.
        """
        with open(os.path.dirname(os.path.realpath(__file__)) + "/Robotiq_init/Pick_place_test.script", "rb") as f:
            for l in iter(partial(f.read, 1024), b''):
                self.s.sendall(l)
        time.sleep(t2CompleteAction)

    def setDigitalOut(self, idPort, state):
        """ Use this to turn ON or OFF the DIGITAL_OUT ports. Use the value TRUE/FALSE for ON/OFF state
        """
        self.s.sendall("set_digital_out({},{})".format(
            idPort, state).encode() + "\n")

    def disconnectfromRobot(self):
        """ Close connection to the Robot.
        """
        self.s.close()

    def turnONRobotAndReleaseBrakes(self):
        """ Turn Robot ON and wait for a while before releasing Brakes and wait for a while again
        """
        c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            c.connect((self.HOST, self.dashboardPORT))
            time.sleep(0.1)
            # OUTPUT if printed -> b'Connected: Universal Robots Dashboard Server\n'
            c.recv(1024)
            c.sendall("power on".encode() + b"\n")
            robotON = c.recv(1024)
            print(robotON.decode('utf-8'))
            time.sleep(10)
            c.sendall("brake release".encode() + b"\n")
            brakesReleased = c.recv(1024)
            print(brakesReleased.decode('utf-8'))
            time.sleep(10)
            c.close()
        except socket.error:
            print("Unable to Turn ON Robot and Release Brakes")

    def robotSafetyModeIs_NORMAL(self):
        """ Get Robot Safety Mode
        """
        c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            c.connect((self.HOST, self.dashboardPORT))
            time.sleep(0.1)
            # OUTPUT if printed -> b'Connected: Universal Robots Dashboard Server\n'
            c.recv(1024)
            c.sendall("safetymode".encode() + b"\n")
            robotMode = c.recv(1024)
            # print(roboteMode)
            time.sleep(0.1)
            robotState = True
            if 'NORMAL' in robotMode.decode('utf-8'):
                robotState = True
            else:
                robotState = False
            c.close()
        except socket.error:
            print("Unable to get Robot Safety Mode")
        return robotState

    def enableRobotAfter_PROTECTIVE_STOP(self):
        """ Enable robot after PROTECTIVE STOP.
            The integrator needs to make sure that the area around the arm is cleared before calling this method.
        """
        c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            c.connect((self.HOST, self.dashboardPORT))
            time.sleep(0.1)
            # OUTPUT if printed -> b'Connected: Universal Robots Dashboard Server\n'
            c.recv(1024)
            print("\nRe enabling Robot. Step Away.\n")
            time.sleep(5)
            c.sendall("unlock protective stop".encode() + b"\n")
            robotMode = c.recv(1024)
            print(robotMode.decode('utf-8'))
            time.sleep(1)
            c.close()
        except socket.error:
            print("Unable to Enable robot from Protective Stop")

    def convertMillimeterToMeter(self, millimeter: float):
        ''' Convert Millimeter to Meter as robot only takes in meters
        '''
        return millimeter / 1000

    def convertMeterToMillimeter(self, meter: float):
        ''' Convert Meter to Millimeter '''
        return meter * 1000



# JB - not needed for me
# sys.path.append(os.getcwd())
# sys.path.append(os.path.join(os.getcwd(), '/UniversalRobot'))
# sys.path.append(os.path.join(os.getcwd(), '/UniversalRobot/UR3e'))