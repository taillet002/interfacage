''' Examples on how to use UR3e Solvay class
'''

# adding class folder to the windows path (if no admin rights)
import sys
sys.path.append(r'C:\Users\LoF\Documents\Python\lib\Instruments')   # <-- !!! write here the path of the UR3e class !!!

# importing UR3e class
from UR3e import UR3e

# instanciation of the robot as 'ur' variable
ur = UR3e(HOST='169.254.42.214', PORT=30003)    # <-- !!! write here the good IP adress !!!

# if using RG6 gripper, enter here which script do you want to use:
rg6_script = r'C:\Users\LoF\Documents\Python\lib\Instruments\UR3e\RG6\rg6_8ml.script'

# general acceleration and speed or time (s)
acc = 0.2      # safe acc:     0.02
spd = 0.2      # safe speed:   0.02

# command to show movement UI (will block the code until closed)
# use ur.UI_move('') if you do not use a gripper
ur.UI_move(rg6_script)


# from the UI you can print positions:
# P position, in mm:
# [243.55129668888338, 289.3135509148989, 92.75354691853983, -2.202462811899367, -0.6634338785885268, -0.6634338785885268]
# Q positions in degrees:
# [-285.7308998711125, -101.29824303206387, -64.29319903473366, 256.46697371302133, 85.26537684352786, 342.9010139213793]
#
# You can then use these positions as "waypoints" for your code

# for example home Q position is:
q_home = [-0.0006183594909721115, -89.99860665316088, 0.0006183594909721115, -89.99937163434792, -0.0025854539719357837, -0.0010554915978529275]

# and if you want to move to home position using 'q' position, just use:
ur.move_q_Joint(q_home, a=acc, v=spd)
ur.WaitforMovement(threshold=1e-04)     # <-- if you want to pause the program until the end of the movement




