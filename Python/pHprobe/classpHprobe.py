
import time

import serial
import serial.tools.list_ports

ports = list(serial.tools.list_ports.comports())

for p in ports:
    if p.vid == 1027:
        pHport = p.device
        break
    else:
        pHport = None

class pHprobe():

    def __init__(self, port=pHport):
        self.isConnected = False
        self.port = port
        self.ser = None

    def connect(self):
        try:
            self.ser = serial.Serial(self.port, timeout=3)
            self.isConnected = True
            self.ser.write(b'*ok,0\r')  # disable acknowledge message printing
            self.ser.write(b'c,0\r')  # disable cont. mode
            return True
        except Exception as e:
            print("pHprobe - ", e)
            return False

    def disconnect(self):
        try:
            self.ser.close()
            self.isConnected = False
            return True
        except Exception as e:
            print("pHprobe - ", e)
            return False

    def pHread(self):
        if self.isConnected:
            readVal = 0
            try:
                # self.ser.flush()
                self.ser.write(b'r\r')  # take single reading from the probe
                # only 1 reading can be done every second - limitation of the probe
                time.sleep(1)
                readVal1 = self.ser.readline().decode('utf-8').split('\r')[0]
                while readVal1 == '*ER\r':
                    self.ser.write(b'r\r')
                    readVal1 = self.ser.readline().decode(
                        'utf-8').split('\r')[0]
                while abs(float(readVal)-float(readVal1)) >= 0.005:
                    readVal = readVal1
                    self.ser.write(b'r\r')
                    time.sleep(1)
                    readVal1 = self.ser.readline().decode(
                        'utf-8').split('\r')[0]
                    print("Current pH reading:", readVal1)
                # put the probe to sleep so that it consumes less power
                # self.ser.write(b'Sleep\r')
                return readVal1
            except Exception as e:
                print("pHprobe - ", e)
                return None

    def pHslope(self):
        if self.isConnected:
            self.ser.write(b'slope,?\r')
            slope = self.ser.readline().decode('utf-8').split(',')
            while slope == '*ER\r':
                self.ser.write(b'slope,?\r')
                slope = self.ser.readline().decode('utf-8').split(',')
            slopeAcid = slope[1]
            slopeBase = slope[2]
            return slopeAcid, slopeBase

    def pHtemperature(self):
        if self.isConnected:
            tempDeviation = input(
                "Enter the temperature deviation for pH probe ")
            self.ser.write(f'T,{tempDeviation}\r'.encode())
            anyError = self.ser.readline().decode('utf-8')
            while anyError == '*ER\r':
                self.ser.write(f'T,{tempDeviation}\r'.encode())
                anyError = self.ser.readline().decode('utf-8')

    def pHcalibrate(self):
        if self.isConnected:


            print("\nCalibrate for MID point pH around 7.0")
            input(
                "\nPress Enter after you dip the pHprobe inside the buffer and wait for it to calibrate")
            print("\nCalibrated pH value: ", self.pHread())
            self.ser.write(b'cal,mid,7\r')
            time.sleep(0.1)
            if self.ser.readline().decode('utf-8') == '*ER\r':
                print("Error occured while calibrating")

            print("\nCalibrate for LOW point pH around 4.0")
            input(
                "\nPress Enter after you dip the pHprobe inside the buffer and wait for it to calibrate")
            print("\nCalibrated pH value: ", self.pHread())
            self.ser.write(b'cal,low,4\r')
            time.sleep(0.1)
            if self.ser.readline().decode('utf-8') == '*ER\r':
                print("Error occured while calibrating")

            print("\nCalibrate for HIGH point pH around 10.0")
            input(
                "\nPress Enter after you dip the pHprobe inside the buffer and wait for it to calibrate")
            print("\nCalibrated pH value: ", self.pHread())
            self.ser.write(b'cal,high,10\r')
            time.sleep(0.1)
            if self.ser.readline().decode('utf-8') == '*ER\r':
                print("Error occured while calibrating")

            print("Calibration Completed")


# print(pH.pHread())
# print(pH.pHslope())

if __name__ == "__main__":
    pH = pHprobe()
    pH.connect()
    pH.pHcalibrate()
    pH.pHread()
    pH.disconnect()

#     from libraries.uArm.classUarm import rack, uArmRobot
#     import pandas as pd

#     pH = pHprobe('COM7')
#     jsonConfigFile = 'config/webApp_config.json'
#     print("Loading webApp configuration json file: ", jsonConfigFile)
#     df_config = pd.read_json(jsonConfigFile)

#     xlsPath = ''
#     configFileRackSamples = df_config.loc['configFileRackSamples'][0]
#     configpHrack = df_config.loc['configFilepHrack'][0]
#     dict_mappingCoordinates = df_config.loc['mapping_Opentron2uArm'][0]

#     rack1 = rack(nCols=10, nRows=4, diameter=14, name='samples')
#     pHrack = rack(nCols=2, nRows=1, diameter=28.5, name='pHrack')
#     uArm1 = uArmRobot(effector='gripper')

#     uArm1.connect()
#     # pHrack.setPosition(uArm1, grid=(1, 2))
#     # pHrack.storedConfig("config/pHrack.rack")

# pH.connect()

#     # time.sleep(10)
#     rack1.loadConfig(path='', filename=configFileRackSamples)
#     pHrack.loadConfig(path='', filename=configpHrack)

# def measurepH(pos):
#     # before grabbing pH probe - intermediate position
#     uArm1.moveCoordinates(150, 100, 115)
#     uArm1.moveTo(pHrack, 'A1', 'gripOut')
#     uArm1.pickVial(pHrack, pos='A1', stepsN=5)
#     uArm1.moveXY(rack1, pos)  # position of vial in arm mapping system
#     uArm1.moveZ(zTarget=rack1.ZPos['top'], stepsN=5)
#     time.sleep(1)
#     pHValue = pH.pHread()
#     time.sleep(1)  # pH measurement will go here
#     uArm1.moveZ(zTarget=pHrack.ZPos['transfer'], stepsN=5)
#     uArm1.dropVial(pHrack, pos='A1', stepsN=5)
#     uArm1.moveZ(zTarget=pHrack.ZPos['gripOut'], stepsN=5)
#     # pH probe - intermediate position
#     uArm1.moveCoordinates(150, 100, 115)
#     return pHValue

#     pHVal = measurepH("A1")
#     print(pHVal)

# pH.disconnect()
