''' script to test tricontinent pumps C series directly '''
import serial
import time
import re

# serial init.
ser = serial.Serial()
ser.baudrate = 9600
ser.port = 'COM7'
ser.bytesize = 8
ser.parity = 'N' 
ser.stopbits = 1
ser.xonxoff = True
ser.timeout = 1
ser.open()

# get firmware number
command = '/' + str(0+1) + '&\r'
print('cmd: '+ command)
ser.write(bytes(command, 'utf8'))
repbytes = ser.readline()
reptxt = repbytes.decode('utf-8')
print('rep: ' + reptxt)

# initialization
command = '/' + str(0+1) + 'ZR\r'
print('cmd: '+ command)
ser.write(bytes(command, 'utf8'))
repbytes = ser.readline()
reptxt = repbytes.decode('utf-8')
print('rep: ' + reptxt)
print('init. done')

time.sleep(1)
ser.close()

# move to absolute position X
command = '/' + str(0+1) + 'Q\r' # get statu
ser.write(bytes(command, 'utf8'))
reptxt = ser.readline().decode('utf-8').strip()
print('rep: ' + reptxt)
while reptxt != '':
    reptxt = ser.readline().decode('utf-8').strip()
    print('rep: ' + reptxt)

command = '/' + str(0+1) + 'A500R\r'
print('cmd: '+ command)
ser.write(bytes(command, 'utf8'))

command = '/' + str(0+1) + 'Q\r' # get statu
ser.write(bytes(command, 'utf8'))
reptxt = ser.readline().decode('utf-8')
print('rep: ' + reptxt)
while reptxt[2] == '@':
    ser.write(bytes(command, 'utf8'))
    reptxt = ser.readline().decode('utf-8')
    print('rep: ' + reptxt)


# get statut
command = '/' + str(0+1) + 'Q\r'
print('cmd: '+ command)
ser.write(bytes(command, 'utf8'))
repbytes = ser.readline()
reptxt = repbytes.decode('utf-8')
print('rep: ' + reptxt)

# move to absolute position 500
command = '/' + str(0+1) + 'A500R\r'
print('cmd: '+ command)
ser.write(bytes(command, 'utf8'))
repbytes = ser.readline()
reptxt = repbytes.decode('utf-8')
print('rep: ' + reptxt)

ser.close()
print('closed')

# set valve to Input (left)
command = '/' + str(0+1) + 'IR\r'
print('cmd: '+ command)
ser.write(bytes(command, 'utf8'))
repbytes = ser.readline()
reptxt = repbytes.decode('utf-8')
print('rep: ' + reptxt)

# get speed
command = '/' + str(0+1) + '?2\r'
print('cmd: '+ command)
ser.write(bytes(command, 'utf8'))
repbytes = ser.readline()
reptxt = repbytes.decode('utf-8')
print('rep: ' + re.match('[0-9]+', reptxt[3:])[0])


# ser.flushInput()



''' script to test tricontinent pumps C series with the home python library '''
import sys
# cannot write PYTHONPATH (admin error)
sys.path.append(r'C:\Users\LoF\Documents\Python\lib\Instruments')       
from CSeriesPumps import *

pump1 = CSeriesPumps('COM4', 5)
pump1.initPump()
# in increments
pump1.SetSpeed(1500)
pump1.AbsoluteMove(2000)
# in mL
pump1.SetSpeed_mLSec(2)
pump1.ValveInput()
pump1.AbsoluteMove_mL(5)
pump1.ValveOutput()
pump1.AbsoluteMove_mL(0)

pump1.getSpeed()

pump2 = CSeriesPumps('COM6', 5)
pump2.initPump()
pump2.SetSpeed(2000)
pump2.AbsoluteMove(0)

pump3 = CSeriesPumps('COM7', 1)
pump3.initPump()
pump3.SetSpeed(2000)
pump3.AbsoluteMove(0)

CseriesPumps.stop()