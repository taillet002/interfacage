import serial
from time import sleep
import re

FERME = 0
OUVERT = 1

class CSeriesPumps(serial.Serial):

    ''' serial com connexion with C Series Pumps from tricontinent '''

    def __init__(self, port: str = "", s_volume: int = 0, baudrate: int = 9600, timeout: int = 1):
        # Connection to C series pumps

        serial.Serial.__init__(self)
        self.port = port
        self.s_volume = s_volume    # syringe volume in mL 0 if not initialized !!! ZeroDivisionError
        self.baudrate = baudrate
        self.bytesize = 8
        self.parity = 'N'
        self.stopbits = 1
        self.xonxoff = True
        self.timeout = timeout
        if port != "":
            try:
                print('Please wait ' + str(self.timeout) + ' secondes')
                self.open()
                sleep(self.timeout)
            except ConnectionError:
                pass

            if self.is_open:
                # get firmware number
                command = '/' + str(0+1) + '&\r'
                self.write(bytes(command, 'utf8'))
                repbytes = self.readline()
                reptxt = repbytes.decode('utf-8')
                print('Successful connection on ' + reptxt[2:-1])
            else:
                print('Connection Failure')


    def __delete__(self, instance):
        self.close()

    def initPump(self):
        # initialization of the pump
        command = '/' + str(0+1) + 'ZR\r'
        print('initialization of the pump with cmd: '+ command)
        self.write(bytes(command, 'utf8'))
        
        # wait while init. done
        command = '/' + str(0+1) + 'Q\r'
        self.write(bytes(command, 'utf8'))
        reptxt = self.readline().decode('utf-8').strip()
        while reptxt[2] == '@':
            self.write(bytes(command, 'utf8'))
            reptxt = self.readline().decode('utf-8').strip()
        print('init. done')
        # self.flushSerial()
        
    def AbsoluteMove(self, pos):
        # Absolute move
        command = '/' + str(0+1) + 'A' + str(pos) + 'R\r'
        return self.sendMove(command)
        
    def AbsoluteMove_mL(self, pos_mL):
        # Absolute move
        pos = pos_mL * 3000 / self.s_volume
        command = '/' + str(0+1) + 'A' + f'{pos:.0f}' + 'R\r'
        print(command)
        return self.sendMove(command)

    def sendMove(self, cmd):
        # flush serial before
        self.flushSerial()
        
        if not self.isBusy():
            # send command to move
            self.write(bytes(cmd, 'utf8'))
    
    def ValveInput(self):
        # flush serial before
        # self.flushSerial()
        
        if not self.isBusy():
            # set valve to Input (left)
            command = '/' + str(0+1) + 'IR\r'
            self.write(bytes(command, 'utf8'))
        
    def ValveOutput(self):
        # flush serial before
        # self.flushSerial()
        
        if not self.isBusy():
            # set valve to Output (right)
            command = '/' + str(0+1) + 'OR\r'
            self.write(bytes(command, 'utf8'))
        
    def SetSpeed(self, speed):
        # in increments / seconds [1 to 6000] p65
        self.flushSerial()      # flush serial before
        if speed > 6000:
            print('speed too high !')
        else:
            command = '/' + str(0+1) + 'V' + str(speed) + 'R\r'
            self.write(bytes(command, 'utf8'))
            # print(f'order: {speed:.0f}, actual: {self.getSpeed()}')
    
    def SetSpeed_mLSec(self, speed):
        # in mL / seconds [0 to 5]. Please refer to p127 of C-Series_manual
        self.flushSerial()      # flush serial before
        speedincr = speed/(self.s_volume/6000)
        if speedincr > 6000:
            print('speed too high !')
        else:
            command = '/' + str(0+1) + 'V' + f'{speedincr:.0f}' + 'R\r'
            self.write(bytes(command, 'utf8'))
            # print(f'order: {speedincr:.0f}, actual: {self.getSpeed()}')
        
    def getSpeed(self):
        self.flushSerial()                  # flush serial before
        command = '/' + str(0+1) + '?2\r'
        self.write(bytes(command, 'utf8'))
        reptxt = self.readline().decode('utf-8').strip()
        return re.match('[0-9]+', reptxt[3:])[0]
        
    def flushSerial(self):
        command = '/' + str(0+1) + 'Q\r' # get statut
        self.write(bytes(command, 'utf8'))
        try:
            reptxt = self.readline().decode('utf-8').strip()
        except UnicodeDecodeError:
            # print('well... invalid start byte')
            reptxt = 'a'
        while reptxt != '':
            reptxt = self.readline().decode('utf-8').strip()
        
    def isBusy(self):
        command = '/' + str(0+1) + 'Q\r'
        self.write(bytes(command, 'utf8'))
        try:
            reptxt = self.readline().decode('utf-8').strip()
        except UnicodeDecodeError:
            # print('well... invalid start byte')
            reptxt = 'aa`'
        if (len(reptxt) >= 2 and reptxt[2] != '`'):
            # print('pump busy, try later or terminate action')
            State = True
        else:
            State = False
        return State
        
    @staticmethod
    def serial_ports():
        """ Lists serial port names

            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system

            Copyright :
                Thomas - https://stackoverflow.com/questions/12090503/listing-available-com-ports-with-python
        """

        ports = ['COM%s' % (i + 1) for i in range(256)]

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result
