# README #
01.10.2018<br>
This repository aim at defining a API to handle C-Series Syringe Pumps from Tricontinent with python.

### What is this repository for? ###

* API to control [C-Series syringe pumps] (https://www.tricontinent.com/c-series-syringe-pumps.html)
* tricontinent contact : Rizwan.Rabbani@gardnerdenver.com
* version 00

### How do I get set up? ###

* intall [the drivers] (https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers) to the computer you want to plug the syringe-pumps
	CP210x USB to UART Bridge Virtual COM Port (VCP) drivers
* clone this reposytory where you want
* load library this way if you did not clone directly into your python environment path :
	import sys
	sys.path.append(r'C:\Users\YOURLOGIN\Documents\python') (or whatever python folder)
	from CSeriesPumps import *
* Dependencies : [pyserial] (https://pythonhosted.org/pyserial) (import serial)
* How to run tests :
	pump1 = CSeriesPumps('COM4', 5) (port com, syringe volume in mL)
	pump1.initPump()
	syringe pump sould move

### Contribution guidelines ###

* __init__.py is the lybrary
* CseriesPump_Excel.py and Classeur1.xlsx
	are a fonction and the template of to run a workflow defined in the template
* server_tricontinent_01.py
	is a function that run a server to communicate with this API (to be improve)
	this work aim to use a webpage to run the syringe-pumps
* tricontinent_pump_test.py
	some test codes, from raw text input to API tests

### Who do I talk to? ###

* jordy.bonnet@solvay.com
* LoF - RIC Bordeaux - DTM Team (thomas.clerico@solvay.com)