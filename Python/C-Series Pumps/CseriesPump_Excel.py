'''
Tri continent CSeriesPumps with Excel workflow
'''

import os                               # operating system (cmd)
import tkinter as tk                    # interface fichiers / folders
from tkinter import filedialog          # interface fichiers / folders
import pandas as pd
import numpy as np
import time

import sys
# cannot write PYTHONPATH (admin error)
sys.path.append(r'C:\Users\u31518\Documents\python')       
from CSeriesPumps import *


root = tk.Tk(); root.withdraw()
file_name = os.path.abspath(filedialog.askopenfilename(title='Select Excel workflow',
filetypes= (("Excel file","*.xls"),("Excel file","*.xlsx")) ))

def PumpsNotBusyAnymore(pumps):
    Busy = True
    while Busy:
        Busy = any([pumps[i].isBusy() for i in range(len(pumps))])
        time.sleep(2)

# read excel config and workflow
cfg = pd.read_excel(file_name,'config');
df = pd.read_excel(file_name,'workflow');

# initialization of the pumps
pumps = [[],]*len(cfg['Pump'])
for i in range(len(cfg['Pump'])):
    print('\nPump', cfg['Pump'].iloc[i], cfg['Port com'].iloc[i], cfg['Syringe volume (mL)'].iloc[i])
    pumps[i] = CSeriesPumps(cfg['Port com'].iloc[i], cfg['Syringe volume (mL)'].iloc[i])
    pumps[i].initPump()

# going through Excel file
for i in df['Bloc'].unique():   # Blocs of executions
    dff = df[df['Bloc']==i]
    print('\nBloc n°: ', i)
    
    # refill syringes and wait for refill to be done !
    print('refill all...')
    for index, row in dff.iterrows():
        print('pompe %d, valve, speed, refill' % (row['Pump']-1))
        pumps[row['Pump']-1].ValveInput()
        pumps[row['Pump']-1].SetSpeed(50)   # 3000
        pumps[row['Pump']-1].AbsoluteMove(3000)
    
    PumpsNotBusyAnymore(pumps)
    
    for index, row in dff.iterrows():
        pumps[row['Pump']-1].ValveOutput()
    
    Flow = [];     Vol = [];    TimetoWait = 0;
    for index, row in dff.iterrows():   # Syringe targets on this Bloc

        if sum(np.isnan([row['Flow (mL/s)'], row['Time (s)'], row['Volume (mL)']])) != 1:
            # make sure 2 of the columns were fill
            print('Choose only, or at least, two of: Flow (mL/s), Time (s), Volume (mL) to fill')
        
        else:
            if all(np.isnan([row['Flow (mL/s)'], row['Time (s)'], row['Volume (mL)']]) == [True,False,False]):
                # Time and Volume given     print('cas 1')
                Flow = row['Volume (mL)'] / row['Time (s)']
                Vol = row['Volume (mL)']
                
            elif all(np.isnan([row['Flow (mL/s)'], row['Time (s)'], row['Volume (mL)']]) == [False,True,False]):
                # Flow and Volume given  print('cas 2')
                Flow = row['Flow (mL/s)']
                Vol = row['Volume (mL)']
                
            elif all(np.isnan([row['Flow (mL/s)'], row['Time (s)'], row['Volume (mL)']]) == [False,False,True]):
                # Flow and Time given    print('cas 3')
                Flow = row['Flow (mL/s)']
                Vol = row['Flow (mL/s)'] * row['Time (s)']
        
        if Vol > cfg['Syringe volume (mL)'].iloc[row['Pump']-1]:
            print('target volume bigger than syringe volume')
        else:
            TimetoWait = max(TimetoWait, Vol / Flow)
            print(f'Flow: {Flow} mL/s, Vol: {Vol} mL, Time: {TimetoWait} s')
            pumps[row['Pump']-1].SetSpeed_mLSec(Flow)
            # it is Vol - Syringe volume bacause after refill initial state is at 5 mL
            pumps[row['Pump']-1].AbsoluteMove_mL(cfg['Syringe volume (mL)'].iloc[row['Pump']-1] - Vol)
    
    PumpsNotBusyAnymore(pumps)

[pumps[i].close() for i in range(len(pumps))]