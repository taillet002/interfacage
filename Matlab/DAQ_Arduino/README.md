## DAQ Arduino 
by Tristan AILLET (LOF)
Version: v1.7b

# Update sur 1.7b, Mars 2020 (Tristan AILLET)
- Fiabilisation de la class acquisition. Le temps écoulé depuis le déclenchement est maintenant basé sur le timestamp (clock) et la fonction etime();
- Change in the display_plot() function

# Update sur 1.7a, Janvier 2018 (Tristan AILLET)
- Suppression du contrôle PID sur Arduino (calculs réalisés sur interface matlab (class_PID) ou autre)
- Fiabilisation du code avec gestion des exceptions (lecture/ecriture SPI et I2C)
- Cartographie des fonctions codées sous ino (dans dossier arduino)
- Epuration du code (supression moteurs et autres, => objectifs juste d'acquisition digit ou analog
- Correction bug dans acquisition + amélioration du code (fiabilisation etc.)
- Réalisation d'une documentation simplifiée sous matlab pour class_acquisition et class_PID (via "help class_PID" par ex)

## Codes Arduino:
Fichier *.ino permettant de piloter la carte arduino via communication RS232:
- entrées/sorties analogiques et digitales
- gestion des protocoles I2C et SPI

** les librairies nécessaires à la compilation sous arduino sont disponibles dans le dossier "libraries_compilation_Arduino". 

## Codes Python:
A développer

L'aide à l'utilisation des fonctions est disponible dans chaque fichier

## Codes Matlab:
- Class arduino_('COMx'): communication en RS232 pour piloter la carte arduino
- class class_acquisition(): acquisition et stockage de données 
- class class_PID(): rétroaction régulée via PID

------- Class acquisition ---------
 
    Tristan AILLET, le 05 Janv. 2018
-----------------------------------

 class_acquisition(F,frequence,[options]);
 F : function handle, par ex. F = @() function_Test(2) 
 frequence: nombre
 option: - xlabel 'str' - ylabel 'str'- legend {'str1' 'str2'}

 ~~~~~~~~~~~ Exemple ~~~~~~~~~~
 
 acq1 = class_acquisition(@() f_Test(2,3),1,'xlabel','x','ylabel','y','legend',{'plot1' 'plot2'});

 acq1.start(); # lance le Timer
 acq1.open_figure(); # Ouvre le plot 
 acq1.open_figure(ax); #dessine dans l'objet ax donné en arg.
 acq1.X # matrice des valeurs acquises (ligne 1 => temps, ligne >1 => valeurs)
 acq1.stop(); # stop le timer
 acq1.export(path); # export la matrice acq1.X au format excel



