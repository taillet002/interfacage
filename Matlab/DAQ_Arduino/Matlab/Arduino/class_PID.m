% ------- Class PID ---------
% 
%    Tristan AILLET, le 05 Janv. 2018
% -----------------------------------
% ~~~~~~ Exemple d'utilisation ~~~~~
%
% -> Creation de l'instance de l'objet class_arduino()
% a = arduino_('COMX');
% 
% -> Modification de la vitesse du cycle (utile si le relai est � faible
% freq.)
% a.PWM_setFreq([2 2]); on regle freq des deux horloges 1 et 2 respecti*
% a.PWM_setPin2Freq(6,1); On regle le pin 6 sur l'horloge 1
% a.PWM_setDuty(6,0); On met un "write" sur le pin 6 � 0 ici
%
% -> Cr�ation de l'instance de l'objet class_PID()
% acq = class_acquisition(); le PID fonctionne qu'avec la classe
% acquisition (voir aide sur class_acquisition)
% 
% freq = 1; hz
% PID1 = class_PID(acq,freq);
%
% -> Param�trage de l'objet PID
%
% coeffsPID = [1 2 3];
% row_in_acqX = 1; (si plusieurs lignes acquises dans acq.X, sp�cifi�es la
% ligne)
% pin_digOutput = 1; (numero du o� le relai est branch� par ex.)
% setPoint = 50; (consigne)
%
% PID1.setup(row_in_acqX,pin_digOutput,setPoint,coeffsPID)
% PID1.setCoeffs([P I D]) = ; %pour mettre � jour les coefficients P, I et D
%
% -> Lancement de la r�gulation
% PID1.On();
%
% -> arr�t de la r�gulation
% PID1.Off();

classdef class_PID < handle
    properties
        acq;
        Kp_ = 0;
        Ki_ = 0;
        Kd_ = 0;
        state_ = 0;
        setPoint_ = 0;
        error_ = 0;
        error_prev_ = 0;
        Iterm_ = 0;
        Dterm_ = 0;
        digOutput_ = 0;
        pinOutput_ = 0;
        input_ = 1;
        freq = 1;
        DigMax = 255;
        DigMin = 0;
        hw;
        t;
    end
    
    methods
        function obj=class_PID(obj_acq,hw,freq)
            obj.acq = obj_acq;
            obj.freq = freq;
            obj.hw = hw;
            obj.t = timer;
            F = @(x,y) obj.refresh; 
            obj.t.TimerFcn = F;
            obj.t.Period = 1/freq;
            obj.t.ExecutionMode = 'fixedSpacing';
            obj.t.TasksToExecute = inf;
        end 
        % Fonction relatives au PID
        function t=setup(obj,input,pin_digOutput,setPoint,coeffs)
            % set Kp Ki Kd
            obj.Kp_ = coeffs(1); 
            obj.Ki_ = coeffs(2);
            obj.Kd_ = coeffs(3);

            obj.pinOutput_ = pin_digOutput;
            obj.setPoint_ = setPoint;
            
            obj.input_ = input;
            
            t = 1;
        end
        function setPoint(obj,val)
            % set Kp Ki Kd
            obj.setPoint_ = val;
        end        
        function setCoeffs(obj,val)
            % set Kp Ki Kd
            % set Kp Ki Kd
            obj.Kp_ = val(1); 
            obj.Ki_ = val(2);
            obj.Kd_ = val(3);
        end     
        function t=state(obj)
            t = obj.state_;
        end
        function f=On(obj)
            obj.state_ = 1;
            start(obj.t);
            f = 1;
        end
        function f=Off(obj)
            obj.state_ = 0;
            stop(obj.t);
            obj.digOutput_ = 0;
            obj.Dterm_ = 0;
            obj.Iterm_ = 0;
            obj.error_ = 0;
            obj.hw.PWM_setDuty(obj.pinOutput_,0);
            f = 0;
        end
        function t=PWM(obj)
            t=obj.digOutput_;
        end
        function t=PWMpc(obj)
            t=100*(obj.digOutput_/obj.DigMax);
        end
        function refresh(obj,~)
           %Check T
               if (obj.state_)
                  obj.digOutput_ = obj.compute();
                  obj.hw.PWM_setDuty(obj.pinOutput_,obj.digOutput_);
               end                  
        end
        function t=compute(obj)
            readVal = obj.acq.X(obj.input_+1,end);
            obj.error_ = obj.setPoint_ - readVal;
            obj.Iterm_ = obj.Iterm_ + obj.error_*(1/obj.freq); 
            obj.Iterm_(isnan(obj.Iterm_)) = 0;
            obj.Dterm_ = (obj.error_ - obj.error_prev_)*obj.freq;
            obj.Dterm_(isnan(obj.Dterm_)) = 0;
            obj.error_prev_ = obj.error_;
            %obj.
%             out = obj.Kp_*obj.error_+obj.Ki_*obj.Iterm_+obj.Kd_*obj.Dterm_;
            out = obj.Kp_*(obj.error_+obj.Ki_*obj.Iterm_+obj.Kd_*obj.Dterm_);
            t = min(max(fix(out),obj.DigMin),obj.DigMax);
        end        
        function delete(obj)
            delete(obj.t);
        end
        function start(obj)
            start(obj.t);
        end
        function stop(obj)
            stop(obj.t);
        end
    end
end