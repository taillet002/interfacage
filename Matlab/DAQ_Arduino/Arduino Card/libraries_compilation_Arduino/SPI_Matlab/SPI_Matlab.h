#ifndef SPI_Matlab_H
#define SPI_Matlab_H

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <stdio.h>

class SPI_Matlab {
   public:
		bool setBitOrder = false;
		int setDataMode = 0;
		int pin_CLK = 255;
		int pin_CS = 255;
		int pin_MISO = 255;
		int readSelect = 0;
		int nb_bytes2read = 0;
		bool OnOff = false;
		void set();
		//(int _pin_CLK, int _pin_CS, int _pin_MISO,bool _OnOff,bool _setBitOrder,int _setDataMode)
		double read(int S);
 };
  
#endif

