#ifndef PID_config_H
#define PID_config_H

#if (ARDUINO >= 100)
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif
#include <stdio.h>

class PID_config {
   public:
     //PID_config();
     int PinAnaInput, PinDigOutput, PinType;
     double AnaInput, DigOutput, SetPoint, Kp, Ki, Kd, Error;
     bool OnOff = false; 
  };
  
#endif

