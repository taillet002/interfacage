%-%
% Class to control Mini core flow Bronkhorst
%
% Feb 2018 Solvay - J. Bonnet
%-%
classdef MiniCoreiFlow < handle
    properties(SetAccess = private, GetAccess = private)
        
        % is simulation mode on ?
        simulation_mode = false;
        
        % [set poiint target, measure]
        val = [];
        
        % Keep track of the serial connection used.
        ComPort = 'COM0';
        
        % Handle to the serial connection of the Bronkhorst
        sc = [];
    end
    
    methods(Access = public)
        function A = MiniCoreiFlow(com_port)
            % Initialization of the interface to the Bronkhorst by setting the com
            % port with which we are supposed to discuss.
            %
            % Parameter:
            %  - com_port: string defining the com port of the XY table.
            
            % To keep track of the com port used.
            A.ComPort = com_port;
            
            % Creation of the serial interface.
            try A.sc = serial(A.ComPort, 'BaudRate', 38400, 'Timeout', 2,...
                    'Parity', 'none', 'DataBits', 8, ...
                    'Terminator', 'CR/LF',...
                    'StopBits',1,...
                    'Tag', 'MiniCoreFlow');
                
                % Open the connection.
                fopen(A.sc);
                
                disp('MinicoreFlow port com open')
                
            catch % if it's not possible to launch serial -> simulation_mode ON
                % Simulation mode will send random values
                
                A.simulation_mode = true;
                
            end
            
        end
                
        function disp(A)
            % Disp function.
            
            % Display information of the serial port used..
            disp('--- MiniCoreFlow ---');
            disp(['Com port used (B 115200, To 2, P none, DB 8, Trm CR/LF): ', A.ComPort]);
        end
                
        function delete(A)
            % Every action to apply before quiting.
            
            % Close the serial handler.
            fclose(A.sc);
            delete(A.sc);
        end
                
        function val = setPoint_Measure(A)
            % Gets Set point and measure
            %
            % Result:
            %  - val: [set point (%), pressure (%)]
            if ~A.simulation_mode
                
                % Build command
                command = ':0A80048121012101210120';
                
                % Send command
                fprintf(A.sc, command);
                
                % Get answer
                r = fscanf(A.sc);
%                 disp(length(r))
                if length(r) < 22;
                    val = [NaN; NaN];
                else
                    % transform r.
                    val = [hex2dec(r(12:15)); hex2dec(r(20:23))];
                    if val(2) > 64000; % whenever setpoint target is 0 value is ~65000
                       val(2) = 0;
                    end
                    
                    %                     disp(val)
%Write val as a mass flow in g/h (full scale = 10 g/h)
                    val = val/32000 *10;

                end
            else
                
                val = randi([0 100],1,2);
                
            end

        end
        
        function set_setpoint(A, setpoint)
            suffix = [];
            % process
            suffix = [suffix, dec2hex(1,2)];
            % parameter
            suffix = [suffix, '21'];
            % valeur
            suffix = [suffix, dec2hex(setpoint*32000/100,4)];
            % Prefix avec somme longueur message
            prefix = [':', dec2hex((length(suffix)/2)+2,2), '03', '01'];
            % commande finale
            command = [prefix, suffix];
            
            fprintf(A.sc, command);
        end
    end
    
    
    
    methods(Access = private)
        function reply = com(a, command)
            % Send a command to the Lauda and save the reply.
            %
            % Parameter:
            %  - command: The command to send to the Agilent.
            %  - reply: reply from the Agilent table.
            
            if a.simulation_mode
                
                reply = 'No Agilent connected';
                
            else
                
                % Clear the output buffer.
                flushinput(a.sc);
                flushoutput(a.sc);
                
                % Send the command.
                fprintf(a.sc, command);
                reply = fgetl(a.sc);
                
            end
        end
        
        
        function command = chanConstr(~, prefix, chans)
            % Build command using channels
            %
            % Parameter:
            %  - prefix:    begenning of the command.
            %  - chans:     list of channels [104 105 ...].
            
            % Build command with channels list.
            com_temp = prefix;
            for i = 1 : length(chans)
                com_temp = [com_temp num2str(chans(i)) ','];
            end
            command = [com_temp(1:end-1) ')'];
            
        end
        
    end
end


