function val=function_ReadAnalogInput(arduino,pins)    
    for i1=1:length(pins)     
       val(i1,1) = arduino.analogRead(pins(i1)); 
    end
end