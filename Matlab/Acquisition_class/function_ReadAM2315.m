function t = function_ReadAM2315(arduino,AM2315_I2CADDR)
    t = -1;
    h = -1;
    temperature = -1;
    % On appelle le capteur
    arduino.I2C_start(AM2315_I2CADDR);
    arduino.I2C_stop(1);
    
    %On va lire le registre datasheet
    AM2315_READREG = '03';
    arduino.I2C_start(AM2315_I2CADDR);
    arduino.I2C_write(AM2315_READREG);
    arduino.I2C_write(0);  % start at address 0x0
    arduino.I2C_write(4);  % request 4 bytes data
    arduino.I2C_stop(1); 
    
    
    t = arduino.I2C_requestFrom(AM2315_I2CADDR, 8,1);
    
    if ~(t(1) == hex2dec(AM2315_READREG)) 
        h = -1;
    else
        h = t(3);
        h = h*256;
        h = h+t(4);
        h = h./10;
        
        temperature = bitand(t(5),127);
        temperature = temperature*256;
        temperature = temperature + t(6);
        temperature = temperature./10;
        
        if (bitshift(t(5),7))
            temperature = - temperature;
        end
    end
    t = [h; temperature];
     %   if (reply[1] != 4) return false; // bytes req'd

end