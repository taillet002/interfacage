function val=function_T31855K(arduino,id)
    if (isnumeric(id))
       val = arduino.SPI_read(id);
    else
       val = arduino.SPI_readm()'; 
    end    
end
