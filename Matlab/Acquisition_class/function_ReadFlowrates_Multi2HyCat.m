function val=function_ReadFlowrates_Multi2HyCat(pump,mfc)
val(1,1)=str2num(pump.read_flowrate());
temp=mfc.setPoint_Measure();
val(2,1)=temp(1);
val(3,1)=temp(2);
end