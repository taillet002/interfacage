% coeffs= [coeff1_capteur1 coeff1_capteur2 ...; coeff_2_capteur1 ....]
% pression = coeff_2*x+coeff_1
function val=function_ReadPressures_Multi2HyCat(arduino,pins,coeffs,pump,Alicat)
for i1=1:length(pins)
    b = coeffs(1,i1);
    a = coeffs(2,i1);
             val(i1,1) = a*arduino.analogRead(pins(i1))+b;
end
val(i1+1,1)= str2num(pump.read_pressure());
val(i1+2,1)= Alicat.get_Measure();
end