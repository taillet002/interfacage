function val=function_MonitoringPID(PID)
    for i1=1:length(PID)
        val(i1,1) = PID(i1).PWMpc();
    end
end