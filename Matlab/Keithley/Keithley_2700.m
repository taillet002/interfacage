%-%
% Class that give access to the Keithley2400 hardware. The goal is to
% isolate all information linked to the communication with the Keithley
% from every ohters parts of the implementation.
%
% 2013.11.04 Solvay - G. Lebrun.
%-%
classdef Keithley_2700 < handle
  % Not accessible properties of the class
  properties(SetAccess = private)
    %-- Keithley setup parameters.
    
    % Used com port.
    ComPort = 'COM6';
    % Handle to the serial connection to the Keithley.
    sc = [];
    % Type of measurement.
    meas_mode = 'VOLT';
    % Range of the source.
    meas_range = 'AUTO';
    % Number of acquisitions.
    n_samples = 1;
    % Acquisition period.
    period = 1;
    
    
    %-- Results of Keithley acquisition (one object for each ).
    
    % Acquired voltage.
    meas_Voltage;
    % Acquired current.
    meas_Current;
    % Acquired resistance.
    meas_Res;
  end
  
  
  % Accessible methods.
  methods
    % Basic constructor implementing a default setup of the Keithley and
    % asking the Com Port which will be used to communicate with the
    % Keithley.
    %
    % Parameter:
    %  - ComPort: string defining the choosen Com Port used to communicate
    %             with the Keithley.
    function K = Keithley_2700(ComPort)
      % Keep track of the ComPort in the 
      K.ComPort = ComPort;
      
      % Open the RS232 connection.
      K.sc = serial(K.ComPort, 'BaudRate', 19200, 'DataBits', 8, ...
                    'Parity', 'none', 'StopBits', 1, 'Terminator', 'LF', 'InputBufferSize', 4096);
                  
      % Open the connection to the Keithley.
      fopen(K.sc);
      
      % We reset the Keithley to always start from the same state.
      K.reset();
      
      % We stop the beep sound in order to limit potential disturbence to
      % other people in the laboratory.
      fprintf(K.sc, 'SYST:BEEP:STAT 0');
    end
    
    
    
    % Overload delete function in order to reset the Keithley and close the
    % ComPort associated before destroying the object or leaving the matlab.
    function delete(K)
      % Send the reset message.
      K.reset();
      
      % Close the ComPort.
      fclose(K.sc);
    end
    
    
    
    % Overloading disp function in order to show setup data and also query
    % and show information directly from the Keithley (compare things to
    % check all is coherent).
    function disp(K)
      % Display the used serial port to communicate with the Keithley.
      disp(['Serial port used: ', K.ComPort]);
      % Display the current measurement mode (according to the last
      % related command send to the Keithley).
      disp(['Measurement mode: ', K.meas_mode]);
      % Range of the source.
      disp(['Measurement range: ', K.meas_range]);
      % Number of acquisitions.
      disp(['Number of samples per acquisition: ', num2str(K.n_samples)]);
      % Display Acquisition period.
      disp(['Period of acquisition: ', num2str(K.period)]);
      % Acquired voltage.
      disp(['Size of last acquired voltage: [', num2str(size(K.meas_Voltage)), ']']);
      % Acquired current.
      disp(['Size of last acquired current: [', num2str(size(K.meas_Current)), ']']);
      % Acquired resistance.
      disp(['Size of last acquired current: [', num2str(size(K.meas_Res)), ']']);
    end
    
    
    
    % Reset the Keithley before leaving matlab or destroying the object.
    function reset(K)
      % Send the reset signal.
      fprintf(K.sc, '*RST');
    end
    
    
    
    % Setup the type of source.
    %
    % Parameter:
    %  - st: source type (choice between VOLT, VOLTage, CURR or CURRent).
    function set_sensor_function(K, st)
      % Verify that the format is correct (in fact a switch case mainly used for the otherwise).
      switch(st)
        case 'VOLT'
          K.meas_mode = 'VOLT';
        case 'VOLTage'
          K.meas_mode = 'VOLT';
        case 'CURR'
          K.meas_mode = 'CURR';
        case 'CURRent'
          K.meas_mode = 'CURR';
        case 'RES'
          K.meas_mode = 'RES';
        case 'RESistance'
          K.meas_mode = 'RES';
        case 'TEMP'
          K.meas_mode = 'TEMP';
        case 'TEMPerature'
          K.meas_mode = 'TEMP';
        otherwise
          warndlg('Wrong type of source specified. Setup to VOLT by default', 'Keithley 2700 - Wrong source.');
      end
      
      % Send the signal to the Keithley.
      temp = sprintf(':SENS:FUNC "%s"', K.meas_mode);
      fprintf(K.sc, temp);
    end
    
    
    
    % Configure the Keithley to access to the right number of acquisition
    % in one shot.
    %
    % Parameter:
    %  - na: Number of acquisition.
    function set_number_acquisition(K, na)
      % Keep track of the new number of acquisition.
      K.n_samples = na;
      
      %-- Send a set of command in order to make the Keithley ready.
    
      % Set the buffer size to na points (decomposition due to
      % incompatibility between Keithley and parsing string in fprintf).
      temp = sprintf('TRAC:POIN %d', na);
      fprintf(K.sc, temp);
      
      % Program the Keithley to make na acquisition per trigger (idem for
      % decomposition).
      temp = sprintf('SAMP:COUN %d', na);
      fprintf(K.sc, temp);
      
      % Set the number of trigger to 1.
      fprintf(K.sc, 'TRIG:COUN 1');
    end
    
    
    
    % Set the period of acquisition.
    %
    % Parameters:
    %  - period: perdio of acquisition.
    function set_acqui_period(K, period)
      % Keep track of the period.
      K.period = period;
      
      % Send the command for the Keithley to take the period into account.
      switch(K.meas_mode)
        case 'VOLT'
          % Range to auto.
          fprintf(K.sc, 'SENS:VOLT:RANG:AUTO ON');
          
          % Set up the period (decomposition due to incompatibility between
          % Keithley and parsing string in fprintf).
          temp = sprintf('SENS:VOLT:NPLC %.2f', period);
          fprintf(K.sc, temp);
          
        case 'CURR'
          % Range to auto.
          fprintf(K.sc, 'SENS:CURR:RANGE:AUTO ON');
          
          % Set up the period (idem).
          temp = sprintf('SENS:CURR:NPLC %.2f', period);
          fprintf(K.sc, temp);
          
        case 'RES'
          % Range to auto.
          fprintf(K.sc, 'SENS:RES:RANGE:AUTO ON');
          
          % Set up the period (idem).
          temp = sprintf('SENS:RES:NPLC %.2f', period);
          fprintf(K.sc, temp);
          
        case 'TEMP'
          % Range to auto.
          fprintf(K.sc, 'SENS:TEMP:RANGE:AUTO ON');
          
          % Set up the period (idem).
          temp = sprintf('SENS:CURR:NPLC %.2f', period);
          fprintf(K.sc, temp);
      end
    end
    
    
    
    % Function to clear the buffer in order to start a new acquisition.
    function clear_buffer(K)
      % Flush both input and output.
      flushinput(K.sc);
      flushoutput(K.sc);
      
      % Send the command to clear the buffer.
      fprintf(K.sc, ':TRAC:CLEAR');
    end
    
    
    
    % The function to initiate/manually trigger the acquisition.
    function start_acquisition(K)
      % Send a command to start.
      fprintf(K.sc, ':INIT');
    end
    
    
    
    % Wait for end of measurement: we send a command that will reply 1 at
    % the end of the acquisition.
    function res = wait_for_measurement(K)
      % Flush all past exchange on serial port.
      flushinput(K.sc);
      flushoutput(K.sc);
      
      % Send the command to wait for the end of the task.
      fprintf(K.sc, '*OPC?');
      
      % Look at serial port until it reply 1.
      res = '';
      while(strcmp(res, '1') == 0)
        res = fscanf(K.sc, '%c', 1);
        pause(0.1);
      end
    end
    
    
    
    % Function reading a set of results.
    function res = read_set(K)
      % Clear the input buffer.
      flushinput(K.sc);
      flushoutput(K.sc);
      
      % Make sure the format of element is limited to datas.
      fprintf(K.sc, ':FORM:ELEM READ');
      
      % Ask for last measurement.
      fprintf(K.sc, ':TRAC:DATA?');
      
      % Read a first set of data.
      tmp = fscanf(K.sc, '%c', 16 * K.n_samples);
      
      % Parse them into float.
      temp = textscan(tmp(2 : end), '%f', 'Delimiter', ',');
      
      switch(K.meas_mode)
        case 'VOLT'
          % Keep track of the acquisition.
          K.meas_Voltage = temp{1};
          
          % We only return voltage values.
          res = K.meas_Voltage;
          
        case 'CURR'
          % Keep track of the acquisition.
          K.meas_Current = temp{1};
          
          % We only return voltage values.
          res = K.meas_Current;
          
        case 'RES'
          % Keep track of the acquisition.
          K.meas_Res = temp{1};
          
          % We only return voltage values.
          res = K.meas_Res;
      end
    end
    
    
    
    % Access to the last readed datas.
    function res = get_data(K)
      switch(K.meas_mode)
        case 'VOLT'
          res = K.meas_Voltage;
        case 'CURR'
          res = K.meas_Current;
        case 'RES'
          res = K.meas_Res;
%         case 'TEMP'
%           res = K.meas_Temperature;
      end
    end
  end
end


