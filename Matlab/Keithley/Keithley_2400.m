%-%
% Class that give access to the Keithley2400 hardware. The goal is to
% isolate all information linked to the communication with the Keithley
% from every ohters parts of the implementation.
%
% 2013.11.04 Solvay - G. Lebrun.
%-%
classdef Keithley_2400 < handle
  % Not accessible properties of the class
  properties(SetAccess = public)
    %-- Keithley setup parameters.
    
    % Used com port.
    ComPort = 'COM1';
    % Handle to the serial connection to the Keithley.
    sc = [];
    % Type of measurement.
    meas_mode = 'CURR';
    % Type of source.
    source_mode = 'VOLT';
    % Range of the source.
    source_range = 'AUTO';
    %CONC function meas
    meas_functionConc = 'OFF' 
    % A voir
    source_type = 'CURR';
    % Compliances
    meas_compl_curr = 1; %mA
    meas_compl_volt = 0; %Volts
        Vstop
    Vstep
    Vdelay
    Vstart
    % Number of acquisitions.
    n_acqui = 1;
    % Start value of a sweep (measurement linked to a source going from
    % start to end value).
    start_value = 0;

    % End value of a sweep (...).
    end_value = 0;
    
    %Step value of a sweep
    step_value = 0;
    
    %-- Results of Keithley acquisition (one object for each type of measurement).
    
    % Acquired voltage.
    meas_Voltage;
    % Acquired current.
    meas_Current;
    % Acquired resistance.
    meas_Res;
  end
  
  
  % Accessible methods.
  methods
    % Basic constructor implementing a default setup of the Keithley and
    % asking the Com Port which will be used to communicate with the
    % Keithley.
    %
    % Parameter:
    %  - ComPort: string defining the choosen Com Port used to communicate
    %             with the Keithley.
    function K = Keithley_2400(ComPort)
      % Keep track of the ComPort in the 
      K.ComPort = ComPort;
      
      % Open the RS232 connection.
      K.sc = serial(K.ComPort, 'BaudRate', 19200, 'DataBits', 8, ...
                    'Parity', 'none', 'StopBits', 1, 'Terminator', 'LF/CR');
                  
      % Open the connection to the Keithley.
      fopen(K.sc);
      
      % Use the Keithley to emit a sound in order to confirm the
      % connection.
      %fprintf(K.sc, ':SYS:BEEP:IMM 440, 1');
      pause(1);
      %fprintf(K.sc, ':SYS:BEEP:IMM 880, 1');
    end
    
    
    
    % Overload delete function in order to reset the Keithley and close the
    % ComPort associated before destroying the object or leaving the matlab.
    function delete(K)
      % Send the reset message.
      K.reset();
       
      % Close the ComPort.
      fclose(K.sc);
    end
    
    
    
    % Overloading disp function in order to show setup data and also query
    % and show information directly from the Keithley (compare things to
    % check all is coherent).
    function disp(K)
      % .
      
    end
    
    
    
    % Reset the Keithley before leaving matlab or destroying the object.
    function reset(K)
      % Send the reset signal.
      fprintf(K.sc, '*RST');
    end
    
    
    
    % Function reading a set of results.
    function read_set(K)
      
    end
    
    function flush(K)
        while (K.sc.BytesAvailable)
            fscanf(K.sc);
        end
    end
    
    
    
    % Setup the type of source.
    %
    % Parameter:
    %  - st: source type (choice between VOLT, VOLTage, CURR or CURRent).

    function set_source_function(K, st)
      % Verify that the format is correct.
      switch(st)
        case 'VOLT'
          % Keep track in the instance.
          K.source_type = 'VOLT';
        case 'VOLTage'
          % Keep track in the instance.
          K.source_type = 'VOLT';
        case 'CURR'
          % Keep track in the instance.
          K.source_type = 'CURR';
        case 'CURRent'
          % Keep track in the instance.
          K.source_type = 'CURR';
        otherwise
          warndlg('Wrong type of source specified. Setup to VOLT by default.', 'Wrong source type error.');
      end
          
      
      % Send the signal to the Keithley.
      fprintf(K.sc, ':SOUR:FUNC %s', K.source_type);
    end
    
    
    
    % Setting the list of measurement function to activate.
    %
    % Parameters:
    %  - list: list of all measure function that will be used during the
    %          acquisition.
    function set_measure_functions(K, list)
      % Set all measurement function to off.
      fprintf(K.sc, ':SENS:FUNC:OFF:ALL');
      
      % Add each measurement at a time 
      for i = 1 : numel(list)
        % Set on each element of the list.
        fprintf(K.sc,':SENS:FUNC "%s"', list{i});
        
        % Setting range of the measurement function to automatic.
       % fprintf(K.sc, ':SENS:%s:RANG:AUTO', list{i});
      end
    end
    function set_measure_function_concurrent(K,st)
    switch(st)
        case 'OFF'
            K.meas_functionConc = 'OFF';
        case 'ON'
            K.meas_functionConc = 'ON';
    end
    fprintf(K.sc, ':SOUR:FUNC:CONC %s', K.meas_functionConc);
    end
    
    function K=set_compliance(K,who,val)
        switch(who)
            case 'VOLT'
                K.meas_compl_volt = val;
               
                fprintf(K.sc, ':SENS:VOLT:PROT %s', K.meas_compl_volt);
                
            case 'CURR'
                K.meas_compl_curr = val;
                fprintf(K.sc, ':SENS:CURR:PROT %s', K.meas_compl_curr);
            otherwise
               % warndlg('Wrong type of source specified. Setup to VOLT by default.', 'Wrong source type error.');
        end    
    end
    
    % Setup the initial source value for a sweep (measure from a set of
    % source states ranging form start value to end value).
    %
    % Parameter:
    %  - sv: start value of the sweep.
    function set_source_init_value(K, sv)
      % Keep track of the setup from PC point of view.
      K.start_value = sv;
      
      % Actual setup of the Keithley.
      switch(K.source_type)
        case 'CURR'
          % Check the value respect the maximum of the range (independently
          % of the source range choosen).
          if(sv > 1.05)
            % A warning to inform something doesn't goes as wished.
            warndlg('The current start value is over the maximum: setting to 0 instead.', 'Value out of range.');
           
            % Setting the value to 0.
            sv = 0;
            K.start_value = 0;
          end
          
          % Check if the value respect the minimum of the range (...).
          if(sv < -1.05)
            % A warning to inform something doesn't goes as wished.
            warndlg('The current start value is under the minimmum: setting to 0 instead.', 'Value out of range.');
           
            % Setting the value to 0.
            sv = 0;
            K.start_value = 0;
          end
          
          % Set the value on the Keithley.
          fprintf(K.sc, 'SOUR:CURR:START %.2f', sv);
          
        case 'VOL'
          % Check the value respect the maximum of the range (independently
          % of the source range choosen).
          if(sv > 210)
            % A warning to inform something doesn't goes as wished.
            warndlg('The voltage start value is over the maximum: setting to 0 instead.', 'Out of range value error.');
           
            % Setting the value to 0.
            sv = 0;
            K.start_value = 0;
          end
          
          % Check if the value respect the minimum of the range (...).
          if(sv < -210)
            % A warning to inform something doesn't goes as wished.
            warndlg('The voltage start value is under the minimmum: setting to 0 instead.', 'Out of value range.');
           
            % Setting the value to 0.
            sv = 0;
            K.start_value = 0;
          end
          
          % Set the value on the Keithley.
          fprintf(K.sc, 'SOUR:VOL:START %.3f', sv);
          
        otherwise
          % Nothing is done apart from a warning to indicate that a strange
          % behavior occurs.
          warndlg(['A bad source type has been proposed where you should not', ...
                   'have been able to do so: ask for help on this one.'], ...
                   'Bad behavior');
      end
    end
    
    
    
    % Setup the end source value for a sweep (measure from a set of
    % source states ranging form start value to end value).
    %
    % Parameter:
    %  - ev: end value of the sweep.
    function set_source_step_value(K, sv)
        K.step_value = sv;
        switch(K.source_type)
            case 'CURR'
                fprintf(K.sc, 'SOUR:CURR:START %.3f', sv);
            case 'VOLT'
                fprintf(K.sc, 'SOUR:VOL:START %.3f', sv);
        end
    end
    function raw(K,cmd)
       %fprintf(K.sc,'*RST');
       %fprintf(K.sc,'*RSsT');
       %fprintf('%s\n', cmd);
       fprintf(K.sc,'%s\n', cmd);
    end
    function set_SWEEP_IV(K,Vstart,Vstop,Vstep,Vdelay,type)
        K.raw('*RST'); pause(0.2);
        K.raw(':SENS:FUNC:CONC OFF'); pause(0.2);
        K.raw(':SOUR:FUNC VOLT'); pause(0.2);
        K.raw(':SENS:FUNC ''CURR'''); pause(0.2);
        K.raw(':SENS:VOLT:PROT 7'); pause(0.2);
        K.raw(':SENS:CURR:PROT 1'); pause(0.2);

        %Lancement Sweep
        K.Vstart = Vstart;
        K.Vstop = Vstop;
        K.Vstep = Vstep;
        K.Vdelay = Vdelay;
       % K.raw(fprintf('%s %d',':SOUR:VOLT:STEP',Vstep)); pause(0.2);
        K.raw([':SOUR:VOLT:START ' num2str(Vstart)]); pause(0.2);
        K.raw([':SOUR:VOLT:STOP ' num2str(Vstop)]); pause(0.2);
        if (strcmp('LOG',type))
            VnbPoint = Vstep;
            K.raw(':SOUR:SWE:SPAC LOG'); pause(0.2);
            K.raw([':SOUR:SWE:POINTs ' num2str(VnbPoint)]); pause(0.2);
        else
            VnbPoint = ceil((Vstop-Vstart)/Vstep);
            K.raw(':SOUR:SWE:SPAC LIN'); pause(0.2);
            K.raw([':SOUR:VOLT:STEP ' num2str(Vstep)]); pause(0.2);
        end
        K.raw(':SOUR:VOLT:MODE SWE'); pause(0.2);

        K.raw([':TRIG:COUN ' num2str(VnbPoint)]); pause(0.2);
        K.raw([':SOUR:DEL ',num2str(K.Vdelay)]); pause(0.2);
        K.raw(':OUTP ON'); pause(0.2);
        K.raw(':FORMat:ELEMents VOLT, CURR'); pause(0.2);
        K.raw(':READ?'); 
        K.raw(':OUTP OFF'); 
        h = waitbar(0,'Please wait...');
        for step = 1:VnbPoint
            pause(K.Vdelay*1.5);
            waitbar(step / VnbPoint);
        end
        close(h);
    end
    function [V,I,ReadT] = readDataFromSweep(K)
        i1 = 1;
        ReadT = 0;
        while (K.sc.BytesAvailable)
           ReadT = [ReadT; scanstr(K.sc,',')];
           pause(0.2);
           i1 = i1+1;
        end
        %% 
        r = [];
        E = 0;
        for i1=1:length(ReadT)
           r = cell2mat(ReadT(i1));
           if (isnumeric(r))
               E(i1) = r;
           end
        end
        E = E';
        E = round(E,8);
        pattern = [K.Vstart:K.Vstep:K.Vstop]';
        t = [];
        i2 = 1;
        for i1=1:1:length(pattern)
            temp = find(E == round(pattern(i1),8));
            if (~isempty(temp))
                t(i2) = temp;
                i2 = i2+1;
            end
        end
        V = E(t);
        I = E(t(1:end)+1);
    end
    function set_source_end_value(K, ev)
      % Keep track of the setup from PC point of view.
      K.end_value = ev;
      
      % Actual setup of the Keithley.
      switch(K.source_type)
        case 'CURR'
          % Check the value respect the maximum of the range (independently
          % of the source range choosen).
          if(ev > 1.05)
            % A warning to inform something doesn't goes as wished.
            warndlg('The current stop value is over the maximum: setting to 0 instead.', 'Out of range value error.');
           
            % Setting the value to 0.
            ev = 0;
            K.value_end = 0;
          end
          
          % Check if the value respect the minimum of the range (...).
          if(ev < -1.05)
            % A warning to inform something doesn't goes as wished.
            warndlg('The current stop value is under the minimmum: setting to 0 instead.', 'Out of range value error.');
           
            % Setting the value to 0.
            ev = 0;
            K.value_initial = 0;
          end
          
          % Set the value on the Keithley.
          fprintf(K.sc, 'SOUR:CURR:STOP %.3f', ev);
          
        case 'VOL'
          % Check the value respect the maximum of the range (independently
          % of the source range choosen).
          if(ev > 210)
            % A warning to inform something doesn't goes as wished.
            warndlg('The voltage start value is over the maximum: setting to 0 instead.', 'Out of range value error.');
           
            % Setting the value to 0.
            ev = 0;
            K.end_value = 0;
          end
          
          % Check if the value respect the minimum of the range (...).
          if(ev < -210)
            % A warning to inform something doesn't goes as wished.
            warndlg('The voltage start value is under the minimmum: setting to 0 instead.', 'Out of range value error.');
           
            % Setting the value to 0.
            ev = 0;
            K.end_value = 0;
          end
          
          % Set the value on the Keithley.
          fprintf(K.sc, 'SOUR:VOL:STOP %.2f', ev);
          
        otherwise
          % Nothing is done apart from a warning to indicate that a strange
          % behavior occurs.
          warndlg(['A bad source type has been proposed where you should not', ...
                   ' have been able to do so: ask for help on this one.'], ...
                   'Bad behavior');
      end
    end
    
    
    
    % Initialize the Keithley for a Voltage source and Intensity
    % measurement. In fact the result correspond to the intensity obtain
    % for each voltage programmed.
    function I = apply_IV_analysis(K, vi, ve, N)
      % Reset Keithley.
      K.reset();
      
      % Set the type of source.
      K.set_source_function('VOL');
            
      % Set the type of measurement.
      K.set_measure_functions({'CURR', 'VOLT'});
      
      % Setup V initial.
      K.set_source_init_value(vi);
      
      % Setup V end.
      K.set_source_end_value(ve);
      
      % Set the number of points.
      K.set_number_acquisition(N);
      
      % Start the acquisition.
      fprintf(ks, ':MEAS?');
      
      % Retreive data.
      I = fscanf('%s');
    end
  end
end


