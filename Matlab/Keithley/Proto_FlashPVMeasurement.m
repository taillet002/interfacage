classdef class_PVMeas < handle
  % Not accessible properties of the class
  properties(SetAccess = public)
    Vstop
    Vstep
    Vdelay
    Vstart
    VLightOn
    VLightOff
    ILightOn
    ILightOff
    Power
    K = [];
    L = [];
  end
  
  methods
      function obj = class_PVMeas()
          obj.L = serial('COM1','BaudRate',9600,'DataBits',8,'Parity','none');
          obj.L.Terminator = 'CR/LF';
          %% Connexion
          fopen(obj.L);
          disp('Connexion � la lampe OK');
          obj.K = Keithley_2400('COM4');
          disp('Connexion au Keithley_2400 OK');
      end
      function obj=gen_IV_LightOn(obj,Vstart,Vstop,Vstep,Vdelay)
          fprintf(obj.L,'S1')
          obj.Vstart = Vstart;
          obj.Vstop = Vstop;
          obj.Vstep = Vstep;
          obj.Vdelay = Vdelay;
          obj.K.set_SWEEP_IV(Vstart,Vstop,Vstep,Vdelay,'LIN');
          pause(1)
          fprintf(obj.L,'S0');
          [V,I,RAW] = obj.K.readDataFromSweep();
          obj.VLightOn = V;
          obj.ILightOn = I;
      end
      function obj=calculate_properties(obj)
          obj.Power = obj.VLightOn.*obj.ILightOn;
      end
      function t=set_Meas(obj,Vm,Im)
          obj.K.raw('*RST'); pause(0.05);        
          obj.K.raw(':SENS:FUNC:CONC OFF'); pause(0.05);
          obj.K.raw(':SOUR:FUNC VOLT'); pause(0.05);
          obj.K.raw(':SOUR:VOLT:MODE FIXED'); pause(0.05);
          obj.K.raw(':SENS:FUNC ''CURR'''); pause(0.05);
          obj.K.raw(':SENS:VOLT:PROT 4'); pause(0.05);
          obj.K.raw(':SENS:CURR:PROT 1'); pause(0.05);
          obj.K.raw([':SOUR:VOLT:LEV ' num2str(Vm)]); pause(0.05);
          obj.K.raw(':OUTP ON'); pause(0.05);
          obj.K.raw(':FORMat:ELEMents VOLT, CURR'); pause(0.05); 
      end
      function t=Meas(obj)
          t = [0;0];
          %fprintf(obj.L,'T1.5');
          %fprintf(obj.L,'TS'); pause(0.1);
          obj.K.raw(':READ?'); pause(0.1);
          if (obj.K.sc.BytesAvailable)
            t = cell2mat(scanstr(obj.K.sc,','));
            %fscanf(obj.K.sc,'%s\n');
          end
         % obj.K.raw(':OUTP OFF');          
      end
      function t=flashMeas(obj,varargin)
         % 
          t = [];
          fprintf(obj.L,'T1.2');
          fprintf(obj.L,'TS'); pause(0.3);
          obj.K.raw(':READ?'); pause(0.2);
          if (obj.K.sc.BytesAvailable)
            t = cell2mat(scanstr(obj.K.sc,','));
            %fscanf(obj.K.sc,'%s\n');
          end

      end
      function obj=gen_IV_LightOff(obj,Vstart,Vstop,Vstep,Vdelay)
          fprintf(obj.L,'S0')
          obj.Vstart = Vstart;
          obj.Vstop = Vstop;
          obj.Vstep = Vstep;
          obj.Vdelay = Vdelay;
          obj.K.set_SWEEP_IV(Vstart,Vstop,Vstep,Vdelay,'LIN');
          pause(1)
          [V,I,RAW] = obj.K.readDataFromSweep();
          obj.VLightOff = V;
          obj.ILightOff = I;
      end
      function delete(obj)
          % Send the reset message.
          obj.K.reset();

          % Close the ComPort.
          fclose(obj.K.sc);
          fclose(obj.L);
      end
    end
  end