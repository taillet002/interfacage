classdef Spectrum < handle
    % Object representing a spectrum. It means we keep all the data, the
    % wavelengths corresponding to all datas and and details about how the
    % spectrum has been produced (mainly specific parameters of an avantes
    % spectrometer).
    %
    % 2015.04.08 Solvay - G. Lebrun
    
    properties(SetAccess = private, GetAccess = private)
        % Handler on the spectrometer communication interface.
        h_av = [];
        
        % Index of the spectrometer in the interface.
        spec_ind = 0;
        
        % We keep track of the serial number of the spectrometer.
        sn = [];
        
        % The acquired spectrogram.
        data = [];
        
        % The set of wavelengths analyzed.
        lambda = [];
        
        % The set of saturated pixels.
        sat = [];
        
        % The number of acquisition used to get the final signal.
        nb_acqu = 1;
        
        % The integration time.
        int_time = 100;
        
        % Integration delay.
        int_delay = 0;
        
        % Number of averages.
        n_averages = 1;
        
        % Start, step and stop pixels.
        sssp = [0, 0, 0];
        
        % Activation or not of the saturation detection.
        sat_detect = 1;
        
        % Use of the dynamic dark correction.
        cdd = 0;
        
        % Synchronization or not to other spectrometers.
        use_sync = 0;
        
        % Use of smoothing on the signal.
        smoothing = 0;
        
        % Trigger parameters (trigger mode, trigger source,
        % trigger source type).
        triggers = [0, 0, 0];
        
        % Use high resolution or not.
        use_high_res = 0;
        
        % Do we change any parameter of the spectrogram acquisition.
        modified = 0;
        
        % What is the number of the last modification of the spectrometer.
        last_modify = 0;
        
        % Configuration in the structure since last modification.
        CS = struct();
    end
    
    
    
    methods
        function S = Spectrum(A, spec_id)
            % We keep track of the handle on the Avantes structure, the indice of
            % the spectrometer to use and we setup a basic configuration.
            %
            % Parameter:
            %  - A: the handler on the interface which handle all the spectrometers
            %       plugged to the computer.
            %  - spec_id: spectrometer index
            
            % Keep track of the handle on the avantes interface.
            S.h_av = A;
            
            % Keep track of the spectrometer .
            S.spec_ind = spec_id;
            
            % Activate the spectrometer - no problem with multiple call of
            % activation.
            A.activate(spec_id);
            
            % We retreive the information from the spectrometer.
            if(A.is_activated(spec_id) > 0)
                % List of informations linked to the spectrometer itself.
                info = A.get_list(spec_id);
                
                % We copy the serial number of the spectrometer.
                S.sn = info.SerialNumber;
                
                % We initialize the different data structure according to the
                % number of pixel in the sensor.
                S.data  = zeros(1, info.pix_number);
                S.lambda = info.lambda';
                S.sat   = zeros(1, info.pix_number);
                
                % Define start, step and stop pixels.
                S.sssp = [0, 1, info.pix_number - 1];
            end
            
            % Construction of the structure according to the initial values of
            % the data members.
            S.modified = 1;
            
            % Apply the initiale setup to the spectrometer.
            S.h_av.set_high_res(S.spec_ind, S.use_high_res);
            S.h_av.set_synchro(S.spec_ind, S.use_sync);
            S.last_modify = S.h_av.set_meas_params(S.spec_ind, S.gen_conf());
        end
        
        
        
        function c = copy(S)
            % Alternative way of copying the instance. Not just keep the same
            % handle: it make a new instance and copy all the data.
            %
            % Result:
            %  - c: the instance copied.
            
            % Creation of the new instance.
            c = Spectrum(S.h_av, S.spec_ind);
            
            % Copy all internal information so that c and current instance states
            % are exactly the same.
            c.data = S.data;
            c.lambda = S.lambda;
            c.sat = S.sat;
            c.nb_acqu = S.nb_acqu;
            c.int_time = S.int_time;
            c.int_delay = S.int_delay;
            c.n_averages = S.n_averages;
            c.sssp = S.sssp;
            c.sat_detect = S.sat_detect;
            c.use_sync = S.use_sync;
            c.cdd = S.cdd;
            c.smoothing = S.smoothing;
            c.triggers = S.triggers;
            c.use_high_res = S.use_high_res;
            c.modified = S.modified;
            c.last_modify = S.last_modify;
            c.CS = S.CS;
        end
        
        
        
        function disp(S)
            % Disp function overload. Here is define how informations of the
            % instance are presented.
            
            % String that will be displayed.
            s = sprintf('State of the spectrum:\n');
            s = sprintf('%s Integration time:           %5d   |  ', s, S.int_time);
            s = sprintf('%s Integration delay:                   %5d\n', s, S.int_delay);
            s = sprintf('%s Number of averages:         %5d   |  ', s, S.n_averages);
            s = sprintf('%s Number of acquisitions:              %5d\n', s, S.nb_acqu);
            s = sprintf('%s Saturation detection ?      %5d   |  ', s, S.sat_detect);
            s = sprintf('%s Dynamic correction of dark ?             %d\n', s, S.cdd);
            s = sprintf('%s Smoothing applied to the data ? %d   |  ', s, S.smoothing);
            s = sprintf('%s Start, step and stop pixel: %4d %4d %4d\n', s, S.sssp);
            s = sprintf('%s Is the spectrometer ready to synchronize to others ?  %28d\n', s, S.use_sync);
            s = sprintf('%s Trigger mode, trigger source and trigger source type: %22d %2d %2d\n', s, S.triggers);
            s = sprintf('%s Is the spectrometer in a high resolution mode ? %34d\n', s, S.use_high_res);
            s = sprintf('%s Spectrogram options modified since last acquisition ? %28d\n', s, S.modified);
            s = sprintf('%s Last Avantes interface modification checksum: %36d\n', s, S.last_modify);
            s = sprintf('%s Backup structure of setup parameters empty ? %37d', s, isempty(S.CS));
            
            % Actual display of all the text generated.
            disp(s);
        end
        
        
        
        function delete(S)
            % When the instance is deleted, the ressource on the spectrometer is
            % released so the Avantes instance knows how much spectrum to process
            % for each spectrometer.
            
            % Ask for spectrometer desactivation.
            S.h_av.deactivate(S.spec_ind);
        end
        
        
        
        function CS = gen_conf(S)
            % Generate a configuration structure from the set of all information
            % gathered about how the spectrometer will make its acquisition.
            %
            % Result:
            %  - CS: a configuration structure generated according to the last
            %        modifications made to spectrometer.
            
            % If no modification where applied since last
            if(S.modified == 1)
                % Integration time during acquisition.
                S.CS.IntegrationTime = S.int_time;
                
                % Integration delay.
                S.CS.IntegrationDelay = S.int_delay;
                
                % Define from how many average signal, the acquisition will be
                % obtained.
                S.CS.NrAverages = S.n_averages;
                
                % Define if we use covered pixel to estimate sensor noise.
                S.CS.CorDynDark = S.cdd;
                S.CS.SaturationDetection = S.sat_detect;
                
                % Define the start, step and stop pixels.
                S.CS.StartPixel = S.sssp(1);
                S.CS.StepPixel  = S.sssp(2);
                S.CS.StopPixel  = S.sssp(3);
                
                % Do we use smoothing on data.
                S.CS.Smoothing = S.smoothing;
                
                % Define trigger mode, source and source type.
                S.CS.TriggerMode       = S.triggers(1);
                S.CS.TriggerSource     = S.triggers(2);
                S.CS.TriggerSourceType = S.triggers(3);
                
                % We return the newly updated structure.
                CS = S.CS;
                
                % As we renew the structure we are back to a configuration where we
                % can consider that informations where not modified.
                S.modified = 0;
            else
                % As nothing as been modified we only send the structure.
                CS = S.CS;
            end
        end
        
        
        
        function plot(S)
            % Redefining the plotting operator.
            
            % Creation of the figure.
            h = figure('Position', [50 0 1280 800], 'Color', 'White');
            
            % Display the sets of spectrometer values.
            subplot('Position', [0.05 0.25 0.9 0.7]); plot(S.lambda, S.data);
            ylabel('Intensity');
            subplot('Position', [0.05 0.1  0.9 0.1]); plot(S.lambda, S.sat);
            ylabel('Saturation');
            xlabel('Wavelenght (nm)');
        end
        
        
        
        function mbdm_plot(S, a)
            % Main embedded plot (i.e. in a dialog box) were 'a' is the handler on
            % the axe. Plot the current spectrum in the axe.
            %
            % Parameters:
            %  - a: handler on the axe in which to plot.
            
            plot(a, S.lambda, S.data);
            ylabel(a, 'Intensity');
        end
        
        
        
        function mbds_plot(S, a)
            % Secondary embedded plot (i.e. in a dialog box) were 'a' is the handler on
            % the axe. Plot the saturation mask of the current spectrum.
            %
            % Parameters:
            %  - a: handler on the axe in which to plot.
            
            plot(a, S.lambda, S.sat);
            ylabel(a, 'Sat.');
            xlabel(a, 'Wavelength (nm.s^{-1})');
        end
        
        
        
        function setup_dia(S)
            % Open a dialog box that enable the user to setup all the
            % characteristics of the Spectrum acquisition.
            
            % The dialog window.
            d = dialog('Position', [300 200 250 120], 'Name', 'Spectrum parameters', 'Color', 'White');
            
            % Add a the handler on the instance so that we can interact with it
            % by mean of the user interface.
            %       set(d, 'UserData', struct());
            %       d.UserData.ih = S;
            %       tmp = get(d, 'UserData');
            %       disp(tmp);
            %       set(tmp, 'ih', S);
            
            % Text and edit control for integration time.
            t_it = uicontrol('Parent', d, 'Style', 'text', 'BackgroundColor', 'White', ...
                'Position', [20 35 100 20], 'HorizontalAlignment', 'left', ...
                'String', 'Integration time'); %#ok<NASGU>
            
            e_it = uicontrol('Parent', d, 'Style', 'edit', 'HorizontalAlignment', 'right', ...
                'Position', [20 20 70 20], 'String', num2str(S.int_time), ...
                'Callback', @(a, b) e_it_cbf(a, b, S)); %#ok<NASGU>
            
            % A set of callback to take into account modified values.
            function e_it_cbf(eit_h, ~, S)
                % eit_h.Parent.UserData.ih.;
                S.set_integration_time(str2double(get(eit_h, 'String')));
            end
            
            
            % Text and edit control for number of averages.
            t_na = uicontrol('Parent', d, 'Style', 'text', 'BackgroundColor', 'White', ...
                'Position', [20 85 100 20], 'HorizontalAlignment', 'left', ...
                'String', 'N� of averages'); %#ok<NASGU>
            
            e_na = uicontrol('Parent', d, 'Style', 'edit', 'HorizontalAlignment', 'right', ...
                'Position', [20 70 70 20], 'String', num2str(S.n_averages), ...
                'Callback', @(a, b) e_na_cbf(a, b, S)); %#ok<NASGU>
            
            
            % A set of callback to take into account modified values.
            function e_na_cbf(ena_h, ~, S)
                S.set_number_averages(str2double(get(ena_h, 'String')));
            end
            
            
            % Text and edit control for number of averages.
            t_id = uicontrol('Parent', d, 'Style', 'text', 'BackgroundColor', 'White', ...
                'Position', [120 85 100 20], 'HorizontalAlignment', 'left', ...
                'String', 'Integration delay'); %#ok<NASGU>
            
            e_id = uicontrol('Parent', d, 'Style', 'edit', 'HorizontalAlignment', 'right', ...
                'Position', [120 70 70 20], 'String', num2str(S.int_delay), ...
                'Callback', @(a, b) e_id_cbf(a, b, S)); %#ok<NASGU>
            
            
            % A set of callback to take into account modified values.
            function e_id_cbf(eid_h, ~, S)
                S.set_integration_delay(str2double(get(eid_h, 'String')));
            end
            
            % Wait for d to close before running to completion.
            uiwait(d);
        end
        
        
        
        function acquire(S)
            % Method to acquire the spectrum. There is no parameter or result
            % because all is define before and the result (signal and saturation)
            % will gathered when the spectrometer is ready.
            
            % If the spectrometer setup or the instance setup has been modified,
            % we renew the setup.
            if(S.last_modify ~= S.h_av.get_modif_count(S.spec_ind) || S.modified == 1)
                S.h_av.set_high_res(S.spec_ind, S.use_high_res);
                S.h_av.set_synchro(S.spec_ind, S.use_sync);
                S.last_modify = S.h_av.set_meas_params(S.spec_ind, S.gen_conf());
            end
            
            % Make the acquisition.
            S.h_av.measure(S.spec_ind, S.nb_acqu);
            
            % Wait until the spectrometer finish its measurements.
            S.h_av.wait_spectro(S.spec_ind);
            
            % Get data once the acquisition is finished.
            data_sat = S.h_av.get_data(S.spec_ind, S.nb_acqu);
            
            % If data has the good number of samples.
            if(size(data_sat, 2) == numel(S.lambda))
                % Update data.
                temp = zeros(size(data_sat, 2), size(data_sat, 3));
                temp(:, :) = data_sat(1, :, :);
                S.data = temp';
                
                % Update sat.
                temp(:, :) = data_sat(2, :, :);
                S.sat = temp';
            else
                disp('A problem occur: mismatch between acquired and instance sizes.');
            end
        end
        
        
        
        function launch_acquisition(S)
            % Method to acquire the spectrum. There is no parameter or result
            % because all is define before and the result (signal and saturation)
            % will gathered when the spectrometer is ready.
            
            % If the spectrometer setup or the instance setup has been modified,
            % we renew the setup.
            if(S.last_modify ~= S.h_av.get_modif_count(S.spec_ind) || S.modified == 1)
                S.h_av.set_high_res(S.spec_ind, S.use_high_res);
                S.h_av.set_synchro(S.spec_ind, S.use_sync);
                S.last_modify = S.h_av.set_meas_params(S.spec_ind, S.gen_conf());
            end
            
            % Launch the acquisition.
            S.h_av.measure(S.spec_ind, S.nb_acqu);
        end
        
        
        
        function check = acquire_data(S)
            % Get data once the acquisition is finished. Must be launched
            % after a S.launch_acquisition();
            
            check = false;

            data_sat = S.h_av.get_data(S.spec_ind, S.nb_acqu);

            
            if(S.h_av.is_ready(S.spec_ind))
                
                % If data has the good number of samples.
                if(size(data_sat, 2) == numel(S.lambda))
                    % Update data.
                    temp = zeros(size(data_sat, 2), size(data_sat, 3));
                    temp(:, :) = data_sat(1, :, :);
                    S.data = temp';
                    
                    % Update sat.
                    temp(:, :) = data_sat(2, :, :);
                    S.sat = temp';
                    
                    check = true;
                else
                    disp('A problem occur: mismatch between acquired and instance sizes.');
                end
            else
                disp('Spectrometer not ready');
            end
        end
        
        
        
        
        function synchro(S)
            % This method retreive all data from the spectrometer when the
            % acquisition has been started by another spectrometer to which it is
            % synchronized.
            
            % Wait until the spectrometer finish its measurements.
            S.h_av.wait_spectro(S.spec_ind);
            
            % Get data once the acquisition is finished.
            data_sat = S.h_av.get_data(S.spec_ind, S.nb_acqu);
            
            % If data has the good number of samples.
            if(size(data_sat, 2) == numel(S.lambda))
                % Update data.
                temp = zeros(size(data_sat, 2), size(data_sat, 3));
                temp(:, :) = data_sat(1, :, :);
                S.data = temp';
                
                % Update sat.
                temp(:, :) = data_sat(2, :, :);
                S.sat = temp';
            else
                disp('A problem occur: mismatch between acquired and instance sizes.');
            end
        end
        
        
        
        function check = set_integration_time(S, int_time)
            % Define the integration time corresponding to the spectrogram.
            %
            % Parameters:
            %  - int_time: integration time.
            %
            % Result:
            %  - check: A string constructed to feed back if the setting goes well.
            
            % Verify the integration time is positive.
            if(int_time > 0)
                % Change the value for the instance.
                S.int_time = int_time;
                
                % We tell the instance a spectrometre parameter has been modified.
                S.modified = 1;
                
                % Return a string telling all works well.
                check = 'Ok';
            else
                % Return a string telling something goes wrong.
                check = 'Integration time (in ms) must be a positive number.';
            end
        end
        
        
        
        function check = set_number_averages(S, n_av)
            % Define the number of averages to get a the spectrogram.
            %
            % Parameters:
            %  - n_av: number of averages to apply.
            %
            % Result:
            %  - check: A string constructed to feed back if the setting goes well.
            
            % If n_av is in the good format.
            if(n_av > 0 && isnumeric(n_av))
                % Change the value for the instance.
                S.n_averages = n_av;
                
                % We tell the instance a spectrometre parameter has been modified.
                S.modified = 1;
                
                % Return a string telling all works well.
                check = 'Ok';
            else
                % Return a string telling something goes wrong.
                check = 'Number of averages must be a positive integer.';
            end
        end
        
        
        
        function check = set_integration_delay(S, int_d)
            % Define the integration delay.
            %
            % Parameters:
            %  - int_d: integration delay of the acquisition.
            %
            % Result:
            %  - check: A string constructed to feed back if the setting goes well.
            
            % If n_av is in the good format.
            if(int_d >= 0 && isnumeric(int_d))
                % Change the value for the instance.
                S.int_delay = int_d;
                
                % We tell the instance a spectrometre parameter has been modified.
                S.modified = 1;
                
                % Return a string telling all works well.
                check = 'Ok';
            else
                % Return a string telling something goes wrong.
                check = 'Integration delay must be a positive.';
            end
        end
        
        
        
        function check = set_number_acquisitions(S, n_ac)
            % Define the number of acquisition the will occur once we ask for
            % measure (define the number of time we need to retreive data before we
            % get the new acquisitions).
            %
            % Parameters:
            %  - n_ac: number of acquisition programmed.
            %
            % Result:
            %  - check: A string constructed to feed back if the setting goes well.
            
            % If n_ac is in the good format.
            if(n_ac > 0 && isnumeric(n_ac))
                % Change the value for the instance.
                S.nb_acqu = n_ac;
                
                % We tell the instance a spectrometre parameter has been modified.
                S.modified = 1;
                
                % Return a string telling all works well.
                check = 'Ok';
            else
                % Return a string telling something goes wrong.
                check = 'Number of acquisitions must be a positive integer.';
            end
        end
        
        
        
        function check = set_high_res(S, val)
            % Define if we want too use or not the high resolution with a
            % spectrometer (for some of them it is not available).
            %
            % Parameters:
            %  - val: 1 for available and 0 if not.
            %
            % Result:
            %  - check: A string constructed to feed back if the setting goes well.
            %           Warning: no feedback from the spectrometer itself, more a
            %           verification that val is in range).
            
            % If the value change or not.
            if(val == S.use_high_res)
                % We do nothing.
                check = 'Ok';
            elseif((val == 1) || (val == 0))
                % We update value.
                S.use_high_res = val;
                
                % We tell the instance something has been modified.
                S.modified = 1;
                
                check = 'Ok';
            else
                % Warn about the range in which val should be.
                check = 'Value should be 0 or 1.';
            end
        end
        
        
        
        function check = set_synchro(S, val)
            % define the value of the synchronization or not of the spectrometer to
            % others.
            %
            % Parameters:
            %  - val: potentially new value of the use or not of synchronization to
            %         another spectrometer.
            %
            % Return:
            %  - check: sentence determining if all was ok (warning: no feedback
            %           from the spectrometer itself, more a verification that val
            %           is in range).
            
            % If the value change or not.
            if(val == S.use_sync)
                % We do nothing.
                check = 'Ok';
            elseif((val == 1) || (val == 0))
                % We update value.
                S.use_sync = val;
                
                % We tell the instance something has been modified.
                S.modified = 1;
                
                check = 'Ok';
            else
                % Warn about the range in which val should be.
                check = 'Value should be 0 or 1.';
            end
        end
        
        
        
        function res = get_data(S)
            % Get the set of data as a copy of the instance datas. The result is a
            % structure to separate context information from data, from saturations
            % and from lambdas.
            %
            % Result:
            %  - res: result with data, saturations profiles and lambdas.
            
            % The complete matrix.
            res = struct();
            
            % The serial number of the spectrometer (to be able to verify the
            % characteristics of the spectrometer if necessary.
            res.sn = S.sn;
            
            % The set of parameters use to make the acquisitions.
            res.infos = S.gen_conf();
            
            % Update the set of wavelengths used for the acquisition.
            res.lambda = S.lambda;
            
            % Update to the last set of result.
            res.data = S.data;
            
            % Update to the last set of saturations profiles.
            res.saturation = S.sat;
        end
        
        
        
        function res = export_xcl(S, path)
            % Export the data in a spreadsheet. First sheet (named Info.) list all
            % the information linked to the acquisition. Second sheet is the
            % spectrum or the set of spectrums.
            %
            % Parameter:
            %  - path: path of the Excel file in which the Spectrum will be
            %          exported.
            
            % Information to add in the spreadsheet.
            tmp = S.get_data();
            
            % Construct the first sheet of the file.
            infos = cat(2, fieldnames(tmp.infos), struct2cell(tmp.infos));
            infos{size(infos, 1) + 1, 1} = 'Serial Number';
            infos{end, 2} = tmp.sn;
            
            % We write the first sheet of the Excel file.
            xlswrite(path, infos, 'Info.');
            
            % We construct the second sheet of file.
            tmp_data = [tmp.lambda', tmp.saturation'];
            tmp_data = cat(2, tmp_data, tmp.data');
            disp(tmp_data);
            
            % We write the second sheet of the Excel file.
            xlswrite(path, tmp_data, 'Data');
        end
    end
end


