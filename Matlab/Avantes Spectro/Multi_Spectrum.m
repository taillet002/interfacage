classdef Multi_Spectrum < handle
% Proposed a transparent way (when possible resuse the same methods names
% to setup spectrometers) to handle a set of spectrometers. Do not work for
% spectrum used in mutiple acquisition mode (possibility to use avering of
% several acquisition but the spectrum instance must not contain several of
% them).
%
% 2015.05.20 Solvay - G. Lebrun

  properties(SetAccess = private, GetAccess = private)
    % List of spectrum to handle.
    list_s = {};
  end
  
  
  
  methods
    function MS = Multi_Spectrum(list_spec)
    % Initialization of the instance with a set of spectrum with which we
    % want to work together.
    %
    % Parameter:
    %  - list_spec: list of spectrum to use together.
    
      % Make a copy of all instance in list_spect.
      MS.list_s = cell(1, numel(list_spec));
      for i = 1 : numel(list_spec)
        MS.list_s{i} = list_spec{i}.copy();
      end
    end
    
  
    
    function delete(MS)
    % Nothing special for the time being.
    end
    
    
    
    function disp(MS)
    % Display each spectrometer in the order they appear in the list (the
    % same of the list used to initialize the instance).
    
      % Disp each spectrum.
      for i = 1 : numel(MS.list_s)
        disp(MS.list_s{i});
      end
    end
    
    
    
    function set_all_synchro(MS)
    % Ask for all the spectrometer to be defined as synchronous.
    
      % For all the spectrometers.
      for i = 1 : numel(MS.list_s)
        MS.list_s{i}.set_synchro(1);
      end
    end
    
    
    
    function acquire(MS)
    % Start the acquisition for each spectrometer (one after the other).
    % No synchronization for the time being.
    
      for i = 1 : numel(MS.list_s)
        MS.list_s{i}.acquire();
      end
    end

    
    
    function check = set_integration_time(MS, int_time)
    % Transposition of the Spectrum methods in order to define the
    % integration time for all the spectrometers. For the time being all
    % the integration time are defined equal (difficulty to synchronize
    % them if we make several following acquisitions).
    %
    % Parameters:
    %  - int_time: integration time.
    %
    % Result:
    %  - check: .
    
      if(isscalar(int_time))
        % For all the spectrometers.
        for i = 1 : numel(MS.list_s)
          % Define the same integration time int_time for spectrometer i.
          check = MS.list_s{i}.set_integration_time(int_time);
          
          if(~strcmp(check, 'Ok'))
            disp('There is a problem in the definition of integration time for the spectrometer:');
            disp(MS.list_s{i});
          end
        end
      else
        % For all the spectrometers.
        for i = 1 : numel(MS.list_s)
          % Define the specific integration time of spectrometer i.
          check = MS.list_s{i}.set_integration_time(int_time(i));
          
          if(~strcmp(check, 'Ok'))
            disp('There is a problem in the definition of integration time for the spectrometer:');
            disp(MS.list_s{i});
          end
        end
      end
    end
    
    
    
    function res = get_data(MS, index)
    % Retreive the merged data of all spectrum. If an index is specified,
    % the method will send the data of the specific spectrum.
    %
    % Parameters:
    %  - index: index of the spectrometer from which to extract data.
    %
    % Return:
    %  - res: the data of the asked spectrum or the merge of all of them.
    
      % If no index given, we produce a merge of all spectrum.
      if(nargin == 1)
        % Initialize result.
        res = struct();
        
        % Merge of all data.
        infos = struct();
        res.sn = cell(1, numel(MS.list_s));
        res.lambda = [];
        res.saturation = [];
        res.data = [];
        
        for i = 1 : numel(MS.list_s)
          % Data of spectrum i.
          data_i = MS.list_s{i}.get_data();
          
          % Merge informations.
          res.infos(i) = data_i.infos;
          res.sn{i} = data_i.sn;
        
          % Merge of data.
          res.lambda = cat(2, res.lambda, data_i.lambda);
          res.saturation = cat(2, res.saturation, data_i.saturation);
          res.data = cat(2, res.data, data_i.data);
        end
        
        % Sort the samples according to their wavelength.
        [slambda, idx] = sort(res.lambda);
        res.lambda = slambda;
        res.saturation = res.saturation(idx);
        res.data = res.data(idx);
        
      elseif(nargin == 2)
        res = MS.list_s{index}.get_data();
      else
        disp('There must be 0 or a scalar argument.');
      end
    end
  end
end




