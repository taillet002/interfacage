%% Création Avantes
avantes = Avantes();

% Création spectros
spectro1 = Spectrum(avantes, 1);
spectro2 = Spectrum(avantes, 1);

%% Paramétrisation spectros
spectro1.set_integration_time(100); %ms
spectro1.set_number_averages(1);

spectro2.set_integration_time(1000); %ms
spectro2.set_number_averages(10);

%% Acquisition
spectro1.acquire();
data = spectro1.get_data();

%% Affichage
plot(data.lambda, data.data);