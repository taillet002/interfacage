classdef Avantes < handle
% This class is a direct interface to what as5215 library give access to.
% It is the communication link to possibily several spectrometers.
%
% 2015.04.09 Solvay - G. Lebrun

  properties(SetAccess = private)
    % Is the Avantes communication canal ressources reserved or not.
    reserved = 0;
    
    % List of spectrometers. Contains there specific informations.
    list = [];
    
    % List of last asked measure (integration time, range, ...) for each
    % spectrometer.
    measure_setup = {};
    
    % Measure setup modification counter - counter on the number of
    % modification on the measure set for each spectrometer.
    msmc = [];
    
    % Spectrometer handler list (empty if never asked for).
    handlers = [];
    
    % Spectrometer handler count (more than one if the same handler is
    % asked by different calls).
    h_count = [];
  end
  
  
  
  methods
    function A = Avantes()
    % During initialization, the key idea is to gather all informations and
    % organize them according to the different spectrometers.
    
      % Initialization of the libraries.
      A.reserved = spectrometer('init', 0);
      
      % If reserved is different form 1 there is a problem.
      if(A.reserved ~= 1)
        disp('There is a problem with the initialization. Is everything plugged ?');
      end
    
      % Get the list of all available spectrometers.
      A.list = spectrometer('getlist');
      
      % Create the list of handlers, handle counters, last setups and the
      % count of the changes in the setup at the size of the list of
      % spectrometers.
      A.handlers = zeros(1, numel(A.list));
      A.h_count = zeros(1, numel(A.list));
      A.measure_setup = cell(1, numel(A.list));
      A.msmc = zeros(1, numel(A.list));
    end
 
    
    
    function delete(A)
    % Deletion method. Mainly handle the freeing of every resources
    % reserved.
      
      % For every spectrometer activated, we desactivate it. For each of
      % them we display a message because they should have been deactivated
      % before Avantes instance deletion.
      disp('Avantes deletion:');
      for n_h = 1 : numel(A.h_count)
        if(A.h_count(n_h) ~= 0)
          spectrometer('deactivate', A.list(n_h));
          disp(['Warning: spectrometer ', num2str(n_h), ' was not deactivated.']);
        end
      end
      
      % Free the spectrometers resources.
      spectrometer('done');
    end
    
    
    
    function disp(A)
    % Display all the status of the Avantes instance.
    
      % Status from initialization.
      if(A.reserved ~= 0)
        disp('Initialized without problem.');
      else
        disp('Problem at initialization.');
      end
      
      % Print the list of all spectrometers available.
      for l = 1 : numel(A.list)
        disp(['Spectrometer : ', num2str(l)]);
        disp(A.list(l));
      end
    end
    
    
    
    function h = activate(A, n)
    % Activate one of the spectrometers giving the handler and keeping at
    % the same time a trace of the newly created handler. If another call
    % ask for the same ressource, a counter is increased to take it into
    % account.
    %
    % Parameter:
    %  - n: index of the spectrometer we want to activate.
    %
    % Result:
    %  - h: the handle on the asked spectrometer.
    
      % If the choosen spectrometer exist (index inferior to max number of
      % spectrometers) we activate it or at least increase the handler
      % counter.
      if((n <= numel(A.handlers)) && (n > 0))
        % If the spectrometer has not been activated.
        if(A.h_count(n) == 0)
          % We activate the spectrometer and keep track of the handler.
          A.handlers(n) = spectrometer('activate', A.list(n));
          
          % We start the handler counter.
          A.h_count(n) = 1;
          
          % We return the handler corresponding to the spectrometer.
          h = A.handlers(n);
                    
          % We gather all the information specific to the spectrometer.
          A.update_info(n);
        else
          % We increase the handler counter.
          A.h_count(n) = A.h_count(n) + 1;
          
          % We return the handler corresponding to the spectrometer.
          h = A.handlers(n);
        end
      else
        % We tell the user that the index correspond to no spectrometers.
        disp(['There is no spectrometer corresponding to index : ', ...
              num2str(n)]);
            
        % As we do not have any handler to send, we send an empty table.
        h = [];
      end
    end
    
    
    
    function deactivate(A, n)
    % Deactivate one spectrometer. If the counter is above 1 it will
    % actually only decrease the counter.
    %
    % Parameter:
    %  - n: index of the spectrometer.
    
      % If the index correspond to a spectrometer.
      if((n <= numel(A.handlers)) && (n > 0))
        % If the counter is at one
        if(A.h_count(n) == 1)
          % We decrease the counter.
          A.h_count(n) = 0;
          
          % We deactivate the spectrometer.
          spectrometer('deactivate', A.handlers(n));
          
          % We empty the handler.
          A.handlers(n) = 0;
        elseif(A.h_count(n) > 1)
          % If the counter is above 1 we only decrease the counter.
          A.h_count = A.h_count - 1;
        else
          % We warn the user that the spectrometer is already deactivated.
          disp('Warning: this spectrometer is already deactivated.');
        end
      else
        % We tell the user that the index correspond to no spectrometers.
        disp(['There is no spectrometer corresponding to index : ', ...
              num2str(n)]);
      end
    end
    
    
    
    function state = is_activated(A, n)
    % Give access according to h_count to the state of each spectrometer
    % (if it is or not activated actually).
    %
    % Parameter:
    %  - n: index of the spectrometer from which we want to get the state.
    %
    % Result:
    %  - state: state of the spectrometer.
    
      % If the index correspond to an actual spectrometer.
      if((n <= numel(A.handlers)) && (n > 0))
        state = A.h_count(n);
      else
        % We tell the user that the index correspond to no spectrometers.
        disp(['There is no spectrometer corresponding to index : ', ...
             num2str(n)]);
        
        % The more compatible state for the case wher ethe spectrometer do
        % not exist is 0.
        state = 0;
      end
    end
    
    
    
    function set_high_res(A, n, val)
    % In order for the user to setup the spectrometer for a high
    % resolution use. Todo: find what it really does ?
    %
    % Parameter:
    %  - n: index of the spectrometer to setup.
    %  - val: boolean value defining if we use or not the high resolution
    %         on the spectrometer.
    
      % If the spectrometer is activated, we setup the high resolution mode.
      if(A.h_count(n) ~= 0)
        % Retreive the spectrometer handler
        h = A.handlers(n);
        
        % Set the spectrometer high res to 'val'.
        spectrometer('usehighres', h, val);
        
        % We keep track of the choice in the information of the
        % spectrometer.
        A.list(n).high_res  = val;
      end
    end
    
    
    
    function info = get_list(A, n)
    % Get a copy of one element of the list of spectrometers.
    %
    % Parameter:
    %  - no parameters: will send the complete list of spectrometer
    %                   characteristics.
    %  - n: index of the spectrometer from which we want the informations.
    %
    % Return:
    %  - info: a copy of the informations.
    
      % .
      if(nargin == 1)
        info = A.list;
      else
        % If there is information.
        if(A.reserved)
          info = A.list(n);
        else
          % Else info will be empty.
          info = [];
        end
      end
    end
    
    
    
    function m_count = set_meas_params(A, n, setup)
    % Define the set of parameters to make a spectrometer acquisition.
    %
    % Parameter:
    %  - n: the number of the spectrometer we want to setup.
    %  - setup: the setup of the spectrometer n for the next acquisition.
    %
    % Result:
    %  - m_count: we return the count on the number of setup modification
    %             so that we know if or not we need to update it.
   
      % Check if the spectrometer is activated.
      if(A.h_count(n) > 0)
        % Modification of the last measure setup for the spectrometer n.
        A.measure_setup{n} = setup;
        
        % Send the new setup to the .
        spectrometer('measconfig', A.handlers(n), A.measure_setup{n});
        
        % Increase the counter on the number of modification of the setup :
        % it is mainly to check at each acquisition that the setup as not
        % been modified by another call of the function.
        A.msmc(n) = A.msmc(n) + 1;
        
        % We return the current count on the number of modifications on the
        % setup.
        m_count = A.msmc(n);
      else
        disp('The spectrometer is not activated.');
        disp('You need to activate it before any acquisition');
        disp('and by doing so, you are invited to make sure ');
        disp('you choose the correct spectrometer.');
      end
    end
    
    
    
    function set_synchro(A, n, sync)
    % Define if the spectrometer n need to be synchronized with another
    % spectrometer.
    %
    % Parameter:
    %  - n: number of the spectrometer.
    %  - sync: value of the synchronization mode.
      
      % Check if the spectrometer is activated.
      if(A.h_count(n) > 0)
        % Change the synchronization mode of the spectrometer.
        spectrometer('setsyncmode', A.handlers(n), sync);
      else
        disp('The spectrometer is not activated.');
        disp('You need to activate it before synchronization.');
      end
    end
    
    
    
    function ready = is_ready(A, n)
    % Define if the choosen spectrometer is ready or not to respond.
    %
    % Parameter:
    %  - n: index of the spectrometer from which we want the state.
    %
    % Result:
    %  - ready: 1 if ready, 0 if not.
    
      % If the spectrometer is activated, ask for its state.
      if(A.h_count(n) > 0)
        % Ask for the state of the spectrometer.
        ready = spectrometer('ready', A.handlers(n));
      else
        disp('The spectrometer is not activated.');
      end
    end
    
    
    
    function measure(A, n, nb)
    % Ask an acquisition for the spectrometer.
    %
    % Parameter:
    %  - n: index of the spectrometer.
    
      % If the spectrometer is activated ask for an acquisition.
      if(A.h_count(n) > 0)
        spectrometer('measure', A.handlers(n), nb);
      else
        disp('The spectrometer is not activated.');
      end
    end
    
    
    
    function wait_spectro(A, n)
    % Wait until the spectrometer is ready to reply to other commands.
    %
    % Parameter:
    %  - n: indice of the spectrometer to wait.
   
      % A first delay according to integration time setup on the
      % spectrometer.
      s = A.measure_setup{n};
      pause(s.IntegrationTime / 1000);
    
      % Wait until the spectrometer is ready to reply.
      ready = false;
      while(~ready)
        % Check the availability of the spectrometer.
        ready = spectrometer('ready', A.handlers(n));
        
        % We display which spectrometer we will wait.
        % disp(['Waiting spectrometer ', num2str(n), '.']);
        
        % A delay to not send to much queries to the spectrometer.
        pause(0.1);
      end
    end
    
    
    
    function data_sat = get_data(A, n, nb)
    % Get all the datas from the spectrometer (acquisition and
    % saturations). They are formated in a simple 2 by number of wavelength
    % with in the first row the acquisition data and in the second row the
    % saturations.
    %
    % Parameter:
    %  - n: number of the spectrometer in the all interface.
    %  - nb: number of acquisition supposed to be taken in the
    %        spectrometer. If underestimated the user will miss samples, if
    %        overestimated, the user will see copies of the last
    %        acquisition at the end of the matrix.
    %
    % Result:
    %  - data_sat: matrix with data and saturation.
    
      % If the spectrometer is activated ask for an acquisition.
      if(A.h_count(n) > 0)
        % Retreive a first acquisition to know the .
        temp = A.acqu_unique(n);
        
        if(nb > 1)
          % Prepare the data for others acquisitions.
          data_sat = zeros(size(temp, 1), size(temp, 2), nb);
          
          % Copy the first element in the global data.
          data_sat(:, :, 1) = temp;
          
          % Retreive the lasts acquisitions.
          for i = 2 : nb
            % Waiting for the spectrometer to be ready to send the new
            % acquisition.
            A.wait_spectro(n);
            
            % Some display to know where we are.
            disp(['Lecture de l''acquisition ', num2str(i)]);
            
            % New acquisition.
            tmp = A.acqu_unique(n);
            
            % Copy to the global data.
            data_sat(:, :, i) = tmp;
          end
        else 
          data_sat = temp;
        end
      else
        disp('The spectrometer is not activated.');
      end
    end
    
    
    
    function m_count = get_modif_count(A, n)
    % A counter to get the number of modification applied to a spectrometer
    % to know if modification were applied to it since last acquisition (in
    % case of multiple access to a same spectrometer).
    %
    % Parameter:
    %  - n: number of the spectrometer.
    %
    % Result:
    %  - m_count: number of setup modification applied to the spectrometer.
    
      % We simply return msmc value as it will be 0 if the spectrometer has
      % not been activated yet.
      m_count = A.msmc(n);
    end
  end
  
  
  
  methods(Access = private)
    function update_info(A, n)
    % The goal of this method is to update all information the SDK give us
    % access to in order to have a profile of the spectrometer.
    %
    % Parameter:
    %  - n: index of the profilometer from which we need informations.
    
      % If the spectrometer has been activated, we update all the
      % informations specific to the spectrometer and not the data of the
      % acquisition itself.
      if(A.h_count(n) ~= 0)
        % Handle on the spectrometer.
        h = A.handlers(n);
        
        % Retreive the version of the spectrometer.
        v =  spectrometer('getversion', h);
        A.list(n).fpga_ver     = v.fpga;
        A.list(n).firmware_ver = v.firmware;
        A.list(n).dll_ver      = v.dll;
        
        % Retreive the parameters of the spectrometer.
        p = spectrometer('getparameter', h); 
        A.list(n).friendly_name = p.FriendlyName;
        A.list(n).sensor_type   = p.SensorType;
        
        % Update the number of pixel of the spectrometer.
        A.list(n).pix_number = spectrometer('getnumpixels', h);
        
        % Get the list of all lamda values.
        A.list(n).lambda = spectrometer('getlambda', h);
        
        % Update the all list of analog ins.
        A.list(n).analog = arrayfun(@(i) spectrometer('getanalogin', h, i), 0 : 8);
        
        % Update the list of digital in.
        A.list(n).digital = arrayfun(@(i) spectrometer('getdigin', h, i), 0 : 2);
      end
    end
    
    
    
    function data_sat = acqu_unique(A, n)
      % Internal method without verification that suppose the spectrometer is
      % already activated.
      %
      % Parameter:
      %  - n: indice of the spectrometer.
      %
      % Result:
      %  - data_sat: 2xnumel matrix with all sample and saturation.
      
      % When ready we get the data saturation.
      data = spectrometer('getdata', A.handlers(n));
      sat  = spectrometer('getsaturated', A.handlers(n));
      
      % The combination of data and sat is returned.
      data_sat = [data'; double(sat)];
    end
  end
end


