%-%
% Class to control Alicat_PCD_series
% Digital Pressure and Vacuum Gauges and controllers
% March 2018 Solvay - J. Bonnet
%-%
classdef Alicat_PCD_series < handle
    properties(SetAccess = private, GetAccess = private)
        
        % is simulation mode on ?
        simulation_mode = false;
        
        % [set poiint target, measure]
        val = [];
        
        % Keep track of the serial connection used.
        ComPort = 'COM1';
        
        % Handle to the serial connection
        sc = [];
    end
    
    methods(Access = public)
        function A = Alicat_PCD_series(com_port)
            % Initialization of the interfaceby setting the com
            % port with which we are supposed to discuss.
            %
            % Parameter:
            %  - com_port: string defining the com port of the XY table.
            
            % To keep track of the com port used.
            A.ComPort = com_port;
            
            % Creation of the serial interface.
            try A.sc = serial(A.ComPort, 'BaudRate', 19200,...
                    'Parity', 'none',...
                    'DataBits', 8, ...
                    'StopBits',1,...
                    'FlowControl','none',...
                    'Terminator','CR',...    
                    'Timeout', 2,...
                    'Tag', 'Alicat_PCD_series');
                
                % Open the connection.
                fopen(A.sc);
%                 fscanf(A.sc,'%f',[1 3])
                
                disp('Alicat_PCD_series port com open')
%                 A.disp()
                
            catch % if it's not possible to launch serial -> simulation_mode ON
                % Simulation mode will send random values
                
                A.simulation_mode = true;
                disp('Pb with serial com. simulation mode : ON');
                
            end
            
        end
                
        function disp(A)
            % Disp function.
            
            % Display information of the serial port used..
            disp('--- Alicat_PCD_series ---');
            disp(['Com port used (B 19200, P none, DB 8, Trm CR): ', A.ComPort]);
        end
                
        function delete(A)
            % Every action to apply before quiting.
            
            % Close the serial handler.
            fclose(A.sc);
            delete(A.sc);
        end
                
        function val = get_Measure(A)
            % Gets measure
            %
            % Result:
            %  - val: [set point (%), pressure (%)]
            if ~A.simulation_mode
                
                % Read pressure
                fprintf(A.sc,'A');
                rep = fscanf(A.sc);
                k = strfind(rep,' ');
                val = str2double(rep(k(1):k(2)));
                
            else
                
                val = randi([0 100],1,2);
                
            end

        end
        
        function set_setpoint_floating(A, setpoint)
            % floating point ex. : AS4.54<CR>
            command = ['AS', num2str(setpoint)];
%             disp(command);
            fprintf(A.sc, command);
            
           r = fscanf(A.sc);
        end
        
        function set_setpoint_portOfFullScale(A, setpoint)
            % portion of full scale
            command = ['AS', num2str(setpoint)];
            
            fprintf(A.sc, command);
        end
    end
    
    
    
    methods(Access = private)
        function reply = com(a, command)
            % Send a command to the Lauda and save the reply.
            %
            % Parameter:
            %  - command: The command to send to the Agilent.
            %  - reply: reply from the Agilent table.
            
            if a.simulation_mode
                
                reply = 'No Agilent connected';
                
            else
                
                % Clear the output buffer.
                flushinput(a.sc);
                flushoutput(a.sc);
                
                % Send the command.
                fprintf(a.sc, command);
                reply = fgetl(a.sc);
                
            end
        end
        
        
        function command = chanConstr(~, prefix, chans)
            % Build command using channels
            %
            % Parameter:
            %  - prefix:    begenning of the command.
            %  - chans:     list of channels [104 105 ...].
            
            % Build command with channels list.
            com_temp = prefix;
            for i = 1 : length(chans)
                com_temp = [com_temp num2str(chans(i)) ','];
            end
            command = [com_temp(1:end-1) ')'];
            
        end
        
    end
end


