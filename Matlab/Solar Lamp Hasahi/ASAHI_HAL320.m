%% Communication avec ASAHI_HAL320
s1 = serial('COM1','BaudRate',9600,'DataBits',8,'Parity','none');
s1.Terminator = 'CR/LF';
%% Connexion
fopen(s1)
%% Paramètrage

%% Liste des commandes
fprintf(s1,'S1') % Ouvre le shutter

fprintf(s1,'S0') % Ferme le shutter 

%% Fermer la connexion
fclose(s1);

%% Test de la cellule
I = [169 0]; %mA
U = [0 0.60134];