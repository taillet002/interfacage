classdef ViewNeMesys < Observer
    %VIEWNEMESYS This class is the view of the NeMesys MVC interface
    %   This class deals with the view part of the interface : it deals
    %   with the figures and plottings of the data. It's an heir of the
    %   Observer class, and it observe the Model. Thus, each time the model
    %   change, the plotting updates.
    %
    % Solvay 2017.07.07 - A. Gay
    % Based on classes by G.Lebrun
    
    properties
        nbSyr;              % The number of syringes
        
        buttons = struct;   % The structure containing the buttons
        tables = struct;    % The structure containing the tables
        fig;                % The figure used
        Controller;         % The Controller of the MVC pattern
        sub_Ax; 			% Axe du Subplot
		bar_Ax;				% Axe de bar
		text_obj;			% Obj des texts
        syrNames;           % The names of the syringes
    end
    
    methods
        
        function V = ViewNeMesys(Controller, figureNb)
            V = V@Observer(Controller.NeMesys);

            % We stock the properties
            V.Controller = Controller;
            V.nbSyr = numel(V.Controller.NeMesys.qPumps);
            V.syrNames = V.Controller.NeMesys.names;
            V.fig = figure();
            V.sub_Ax = subplot(2, 1, 1);  
            set(V.fig,'CurrentAxes',V.sub_Ax);
			V.bar_Ax =  bar(V.sub_Ax,[1 1]);
            V.CreateView();
            %set(V.fig, 'ResizeFcn', @V.updatePosition);
        end
        
        
        
        function delete(V)
            % The function that deletes the view, the model and the
            % controller
                        
            V.Controller.delete();
            
            figure(V.fig);
            closereq;           
            
            delete@Observer(V);                        
        end
        
        
        
        function CreateView(V)
            % This function initialise all the variables and interfaces
            
            figure(V.fig);
            set(V.fig, 'Position', [0 0 624 959])
            
            V.CreateButtons();
            V.CreateTables();
            V.CreateText();
            %set(V.fig, 'ResizeFcn', @V.updatePosition);
            set(V.fig, 'CloseRequestFcn', @V.ClosefigureFcn);
        end
        
        
        
        function updatePosition(varargin)
            % Update the position of all the elements when the size of the
            % figure change
            try
                V = varargin{1};
                
                V.OptimizeSizeTable(V.tables.data);
                V.OptimizeSizeTable(V.tables.edit);
                
                V.setPositionButtons();
            catch exc
                %disp(exc.message)
                %disp(exc.stack(1))
            end
        end
        
        
        
        function update(V, N)
            % This function is called when the model has been updated
            % N is the NeMesys model
            
            try
                sec = round((N.timeLeft / 60 - fix(N.timeLeft / 60))*60);
                
                timeLeft = fix(N.timeLeft / 60) + sec/100;

                
                data = [N.isPumping; N.estFlow; timeLeft; N.volLeft; ...
                    N.volTot; N.enabled; N.inFault];
                set(V.tables.data, 'Data', data);
                set(V.tables.data, 'ColumnName', N.names);
                
                editData = [N.flowList; N.maxFlow; N.volWanted];
                currentData = get(V.tables.edit, 'Data');
                
                if(~isequal(editData, currentData))
                    set(V.tables.edit, 'Data', editData);
                    set(V.tables.edit, 'ColumnName', N.names);
                end
                
                V.PlotBar(N);
            catch exc
                %disp(exc.message)
                %disp(exc.stack(1))
                %V.CreateView()
            end
        end
        
    end
    
    methods (Access = private)
        
        function CreateButtons(V)
            for i=1:V.nbSyr
                V.buttons.fill(i) = uicontrol('Parent',V.fig, ...
                    'Style','pushbutton', ...
                    'String','Remplir', ...
                    'Units', 'Normalized', ...
                    'Visible','on', ...
                    'Callback', {@V.FillFcn, i});

                V.buttons.empty(i) = uicontrol('Parent',V.fig, ...
                    'Style','pushbutton', ...
                    'String','Vider', ...
                    'Units', 'Normalized', ...
                    'Visible','on', ...
                    'Callback', {@V.EmptyFcn, i});
                                
                V.buttons.feed(i) = uicontrol('Parent',V.fig, ...
                    'Style','pushbutton', ...
                    'String','Feed', ...
                    'Units', 'Normalized', ...
                    'Visible','on', ...
                    'Callback', {@V.FeedFcn, i});
                
                V.buttons.process(i) = uicontrol('Parent',V.fig, ...
                    'Style','pushbutton', ...
                    'String','Process', ...
                    'Units', 'Normalized', ...
                    'Visible','on', ...
                    'Callback', {@V.ProcessFcn, i});
                
                V.buttons.stop(i) = uicontrol('Parent',V.fig, ...
                    'Style','pushbutton', ...
                    'String','Arr�t', ...
                    'Units', 'Normalized', ...
                    'Visible','on', ...
                    'Callback', {@V.StopFcn, i});

                V.buttons.debug(i) = uicontrol('Parent',V.fig, ...
                    'Style','pushbutton', ...
                    'String','Debug', ...
                    'Units', 'Normalized', ...
                    'Visible','on', ...
                    'Callback', {@V.DebugFcn, i});

            end
        end
        
        
        
        function setPositionButtons(V)
            wTot = get(V.fig, 'Position');
            wTot = wTot(3);
            
            wCell = get(V.tables.edit, 'ColumnWidth');
            wCell = wCell{1};
            pCell = wCell / wTot;
            
            wEdge = get(V.tables.edit, 'Position');
            wEdge = wTot * (1-wEdge(3)) / 2;
            
            wHead = wTot - 1.5* wEdge - V.nbSyr * wCell;
            pHead = wHead / wTot;   % Percentage occupied by headers & left edge
            
            for i=1:V.nbSyr
                set(V.buttons.fill(i), 'Position', ...
                    [pHead+((i-1)+0.05)*pCell 0.45 0.45*pCell 0.045]);
                
                set(V.buttons.empty(i), 'Position', ...
                    [pHead+((i-1)+0.05)*pCell 0.405 0.45*pCell 0.045]);
                
                set(V.buttons.feed(i), 'Position', ...
                    [pHead+((i-1)+0.5)*pCell 0.45 0.45*pCell 0.045]);
                
                set(V.buttons.process(i), 'Position', ...
                    [pHead+((i-1)+0.5)*pCell 0.405 0.45*pCell 0.045]);
                
                set(V.buttons.stop(i), 'Position', ...
                    [pHead+((i-1)+0.25)*pCell 0.435 0.5*pCell 0.03]);

                set(V.buttons.debug(i), 'Position', ...
                    [pHead+((i-1)+0.05)*pCell 0.495 0.90*pCell 0.03]);
            end
        end
        
        
        
        function CreateTables(V)
            V.tables.data = uitable(V.fig, ...
                'Data', zeros(6, V.nbSyr),...
                'Units', 'Normalized', ...
                'Position', [0.02 0.02 0.96 0.25], ...
                'ColumnWidth', {100 100 100 100 100},...
                'RowName', {'Pompe ?'; 'Flux Calcul�'; 'Tps restt (mn.s)'; 'Vol actuel (mL)'; 'Vol Total (mL)'; '''Enable'''; 'Erreur ?'});
            
            V.tables.edit = uitable(V.fig, ...
                'Data', zeros(3, V.nbSyr),...
                'Units', 'Normalized', ...
                'Position', [0.02 0.27 0.96 0.13], ...
                'ColumnWidth', 'auto',...
                'RowName', {'D�bit (mL/min)*'; 'Db max (mL/min)*'; 'Vol voulu (mL)'},...
                'CellEditCallback', @V.CellEditFcn, ...
                'ColumnEditable', true);
            
            V.updatePosition();
        end
        
        
        
        function OptimizeSizeTable(~, table)
            nbElem = get(table, 'Data');
            nbElem = size(nbElem, 2);
            
            b1 = 100;
            set(table, 'ColumnWidth', num2cell(b1 * ones(1, nbElem)));
            pos = get(table, 'Extent');
            x1 = pos(3);
            
            b2 = 50;
            set(table, 'ColumnWidth', num2cell(b2 * ones(1, nbElem)));
            pos = get(table, 'Extent');
            x2 = pos(3);
            
            pos = get(table, 'Position');
            widthTot = nbElem * (b1-b2) / (x1-x2);
            widthHead = widthTot * x1 - nbElem * b1;
            
            b_opt = fix((widthTot * pos(3) - widthHead) / nbElem)-1;
            
            set(table, 'ColumnWidth', num2cell(b_opt * ones(1, nbElem)));
        end
        
        function V=CreateText(V)
			for i=1:V.nbSyr
                
								% We plot the name
				V.text_obj(i,1) = text(i, 0.9, 'Name',...
                    'HorizontalAlignment','center',...
                    'FontWeight', 'bold', ...
                    'EdgeColor', 'k', ...
                    'BackgroundColor', [0.9 0.9 0.9], ...
                    'Margin', 4);
				                % We plot the percentage
                V.text_obj(i,2) = text(i, 0.5, sprintf('%4.0f%%', 100),...
                    'HorizontalAlignment','center',...
                    'FontWeight', 'bold', ...
                    'EdgeColor', 'k', ...
                    'BackgroundColor', 'w', ...
                    'Margin', 4);
					            % We plot the state : emptying, filling or stopped  
				V.text_obj(i,3) = text(i, 0.8, 'A l''arr�t', ...
                    'HorizontalAlignment','center',...
                    'FontWeight', 'bold', ...
                    'EdgeColor', 'k', ...
                    'BackgroundColor', 'w', ...
                    'Margin', 3);
                
				                % We plot the valve state : Feed or Process
                V.text_obj(i,4) = text(i, 0.65, 'A definir', ...
                    'HorizontalAlignment','center',...
                    'FontWeight', 'bold', ...
                    'EdgeColor', 'k', ...
                    'BackgroundColor', 'w', ...
                    'Margin', 3);
                set(V.text_obj(i,1),'Parent',get(V.fig,'CurrentAxes'));
                set(V.text_obj(i,2),'Parent',get(V.fig,'CurrentAxes'));
                set(V.text_obj(i,3),'Parent',get(V.fig,'CurrentAxes'));
                set(V.text_obj(i,4),'Parent',get(V.fig,'CurrentAxes'));
            end
            
            
		end
        
        function PlotBar(V, N)
            % We plot the level bar
%             set(0,'CurrentFigure',V.fig);
%             subplot(2, 1, 1);
%             plot(NaN)
%             bar(N.level);
%             ylim([0 1])
%             xlim()
            set(V.bar_Ax,'YData',N.level);
            set(V.sub_Ax,'YLim',[0 1]);
            set(V.sub_Ax,'XLim',[0 V.nbSyr+1]);
            for i=1:V.nbSyr
                % We plot the name
				set(V.text_obj(i,1),'String',N.names(i,:));
				             
                % We plot the percentage
				set(V.text_obj(i,2),'String',sprintf('%4.0f%%', abs(100*N.level(i))));
				              
                % We plot the state : emptying, filling or stopped
                sFlow = N.flowList(i);
                if(sFlow < 0)
                    str = 'Remplissage';
                elseif(sFlow > 0)
                    str = 'Marche';
					set(V.text_obj(i,3),'Color',[0 1 0]);
                else
					set(V.text_obj(i,3),'Color',[1 0 0]);
                    str = 'Arr�t';
					
                end
				set(V.text_obj(i,3),'String',str);
				
                
                % We plot the valve state : Feed or Process
                if(N.isFeedPos(i) == 1)
                    str = 'Feed';
                else
                    str = 'Process';
                end
				set(V.text_obj(i,4),'String',str);
            end
%             for i=1:V.nbSyr
%                 % We plot the name
%                 text(i, 0.9, N.names(i,:),...
%                     'HorizontalAlignment','center',...
%                     'FontWeight', 'bold', ...
%                     'EdgeColor', 'k', ...
%                     'BackgroundColor', [0.9 0.9 0.9], ...
%                     'Margin', 4);
%                 
%                 % We plot the percentage
%                 text(i, 0.5, sprintf('%4.0f%%', abs(100*N.level(i))),...
%                     'HorizontalAlignment','center',...
%                     'FontWeight', 'bold', ...
%                     'EdgeColor', 'k', ...
%                     'BackgroundColor', 'w', ...
%                     'Margin', 4);
%                 
%                 
%                 % We plot the state : emptying, filling or stopped
%                 sFlow = N.flowList(i);
%                 if(sFlow < 0)
%                     str = 'Aspire';
%                 elseif(sFlow > 0)
%                     str = 'Se vide';
%                 else
%                     str = 'A l''arr�t';
%                 end
%                 text(i, 0.8, str, ...
%                     'HorizontalAlignment','center',...
%                     'FontWeight', 'bold', ...
%                     'EdgeColor', 'k', ...
%                     'BackgroundColor', 'w', ...
%                     'Margin', 3);
%                 
%                 % We plot the valve state : Feed or Process
%                 if(N.isFeedPos(i) == 1)
%                     str = 'Feed';
%                 else
%                     str = 'Process';
%                 end
%                 text(i, 0.65, str, ...
%                     'HorizontalAlignment','center',...
%                     'FontWeight', 'bold', ...
%                     'EdgeColor', 'k', ...
%                     'BackgroundColor', 'w', ...
%                     'Margin', 3);
%             end
        end
        
        
        
        function FillFcn(V, ~, ~, index)
            % This function fill the corresponding syringe
            
            V.Controller.fillSyringe(index);
        end
        
        function EmptyFcn(V, ~, ~, index)
            % This function empty the corresponding syringe
            
            V.Controller.emptySyringe(index);
        end
        
        function StopFcn(V, ~, ~, index)
            % This function stop the corresponding syringe
            
            V.Controller.stopSyringe(index);
        end
        
        function FeedFcn(V, ~, ~, index)
            % This function change the position of the corresponding valve
            
            V.Controller.feedPosition(index);
        end
        
        function ProcessFcn(V, ~, ~, index)
            % This function change the position of the corresponding valve
            
            V.Controller.processPosition(index);
        end
        
        function DebugFcn(V, ~, ~, index)
            % This function change the position of the corresponding valve
            
            V.Controller.debugPump(index);
        end        
        
        function CellEditFcn(V, ~, callbackdata)
            % Function called when the flow or the max flow is modified
            
            row = callbackdata.Indices(1);
            index = callbackdata.Indices(2);
            
            switch row
                case 1  % Flow change
                    V.Controller.setFlowIndex(index, callbackdata.NewData);
                    
                case 2  % Max flow change
                    V.Controller.setMaxFlowIndex(index, callbackdata.NewData);
                    
                case 3  % Wanted volume change
                    %% Non g�r�
                    disp('Non g�r� pour l''instant')
            end
        end
        
        function ClosefigureFcn(varargin)            
            V = varargin{1};
            
            V.delete();
        end
        
    end
end