classdef Qmix_Pump < handle
    % Class to interface Qmix pumps. Essentially a gathering of all Qmix mex
    % functions in a unique class.
    %
    % Solvay 2016.03.08 - G. Lebrun
    
    properties(SetAccess = private, GetAccess = public)
        % Handle on the Qmix_Bus instance to increment and decrement.
        qb = [];
        
        % Number of the axis in the bus.
        no = 0;
        
        % Handle on the pump.
        ph = 0;
        
        % Driver position counter.
        dpc = 0;
        
        % Maximum driver position counter
        dpcMax = 0;
        
        % Maximum Flow Rate
        maxFlowRate = 0;
        
        % Volume unit (2 integers).
        vu = [0 0];
        
        % Flow unit (3 integers).
        fu = [0 0 0];
        
        % Characteristics of the syringe.
        sc = [0 0];
        
        % In case of wrong instanciation.
        wi = false;
    end
    
    
    
    methods
        function Q = Qmix_Pump(qb, pump_id)
            % Initialization of a pump.
            %
            % Parameters:
            %  - qb: Qmix_Bus instance to increment and decrement
            %  - pump_id: Identification number of the pump in the chosen bus.
            
            % Open The bus (and keep track of the instance involved).
            Q.qb = qb;
            Q.qb.add_axis();
            
            % Keep track of the number of the axis in the bus.
            Q.no = pump_id;
            
            % Retreive the handler on the axis.
            [res, Q.ph] = qp_get_pump_handle(pump_id);
            
            % In case there is a problem to obtain the handle on the pump.
            if(res ~= 0)
                warning('Enable to get the handler on neMESYS axis %d because of error code %d.', pump_id, res);
                Q.wi = true;
            else
                Q.wi = false;
            end
        end
        
        
        
        function delete(Q)
            % When deleting the axis, we also tell the 'bus' to take it into
            % account so that in case all axis where deleted, it will close the
            % bus.
            
            % Delete one axis in the bus (no need to identified which one).
            Q.qb.del_axis();
        end
        
        
        
        function disp(Q)
            % Overload the disp function in order to show the details of the
            % syringe.
            
            fprintf('Handle the pump: %d\n', Q.ph);
            fprintf('Syringe on pump %d\n', Q.no);
            fprintf('Volume unit : %s%s\n', ...
                Q.to_unit_pre(Q.vu(1)), Q.to_unit_suf(Q.vu(2)));
            fprintf('Flow rate unit : %s%s%s\n', ...
                Q.to_unit_pre(Q.fu(1)), Q.to_unit_suf(Q.fu(2)), Q.to_unit_time(Q.fu(3)));
            fprintf('Syringe inner diameter in mm : %d\n', Q.sc(1));
            fprintf('Syringe maximum stroke in mm : %d\n', Q.sc(2));
        end
        
        
        
        function import_syringe_params(Q, p)
            % Import parameters for the syringe. We do not consider the driver
            % position counter in this method because in some case it is necessary
            % to not take it into account. See
            %
            % Parameters:
            %  - p: structure with all information needed to define the syringe
            %       installed on the pump. It also contains the unit as it is
            %       needed to define correctly the syringe.
            %
            %  Waited structure for params:
            %  - vol: vector of two real defining the volume unit.
            %  - flow: vector of three real defining the flow unit.
            %  - syr: vector of two real defining the syringe itself.
            
            % Apply the parameters.
            Q.set_flow_unit(p.flow(1), p.flow(2), p.flow(3));
            Q.set_volume_unit(p.vol(1), p.vol(2));
            Q.set_syringe(p.syr(1), p.syr(2));
            Q.set_max_driver_pos_cnt(p.dpcMax(1));
            Q.set_max_flow_rate(p.flowMax(1));
        end
        
        
        
        function p = export_syringe_params(Q)
            % Export parameters for the syringe.
            %
            % Return:
            %  - p: structure with all needed parameters to define the syringe.
            %
            % Waited structure for params:
            %  - vol: vector of two real defining the volume unit.
            %  - flow: vector of three real defining the flow unit.
            %  - syr: vector of two real defining the syringe itself.
            %  - dpc: driver position counter (the user is free to use it or not).
            
            % Initialize the structure.
            p = struct();
            
            % Set the values
            p.flow = Q.fu;
            p.vol = Q.vu;
            p.syr = Q.sc;
            
            % Update drive position counter in order to have the very last value.
            p.dpc = Q.get_driver_pos_cnt();
        end
        
        
        
        function set_flow_unit(Q, pre, u, t)
            % Define the flow unit used for interaction with the axis.
            %
            % Parameters:
            %  - pre: prefix number (-6 for �, -3 for milli and so on).
            %  - u: unit (only 68 for L in my knowledge).
            %  - t: time factor (1 for second, 60 for minute and 3600 for hour).
            
            % Set the unit directly in the neMESYS.
            qp_set_flow_unit(Q.ph, pre, u, t);
            
            % Keep track of the unit in the instance.
            Q.fu = [pre, u, t];
        end
        

        
        function fu = get_flow_unit(Q)
            % Return the flow unit used for interaction with the axis.
            %
            % Parameters:
            %   fu = flow Unit
            
            % Keep track of the unit in the instance.
            fu = Q.fu;
        end

        
        
        function set_volume_unit(Q, pre, u)
            % Define the volume unit used for interaction with the axis.
            %
            % Parameters:
            %  - pre: prefix number (-6 for �, -3 for milli and so on).
            %  - u: unit (only 68 for L in my knowledge).
            
            % Set the unit directly in the neMESYS.
            qp_set_volume_unit(Q.ph, pre, u);
            
            % Keep track of the unit in the instance.
            Q.vu = [pre, u];
        end
        
        
        
        function vu = get_volume_unit(Q)
            % Return the volume unit used for interaction with the axis.
            %
            % Parameters:
            %  - vu: volume unit.
                        
            % Keep track of the unit in the instance.
            vu = Q.vu;
        end
        
        
        function set_syringe(Q, dia, len)
            % Define the characteristics of the syringe.
            %
            % Parameters:
            %  - dia: diameter of the syringe.
            %  - len: length of the syringe.
            
            % Set hte characteristic of the syringe in the neMESYS.
            qp_set_syr_params(Q.ph, dia, len);
            
            % Keep track of the characteristics.
            Q.sc = [dia, len];
        end
        
        
        
        function set_driver_pos_cnt(Q, dpc)
            % Set the driver position counter of the pump. Warning: this is a
            % dangerous operation as it may lead to false position of the seringe.
            %
            % Parameters:
            %  - dpc: driver position counter we want to setup.
            
            % Restore the driver position counter.
            disp(qp_restore_dpc(Q.ph, dpc));
        end
        
        
        
        function set_max_driver_pos_cnt(Q, dpcMax)
            % Set the driver maximum position counter of the pump.
            %
            % Parameters:
            %  - dpcMax: driver maximum position counter we want to setup.
            
            Q.dpcMax = dpcMax;
        end
        
        
        function set_max_flow_rate(Q, flowMax)
            % Set the driver maximum position counter of the pump.
            %
            % Parameters:
            %  - flowMax: maximum flow rate we want to setup.
            
            Q.maxFlowRate = flowMax;
        end
        
        
        
        function dpc = get_driver_pos_cnt(Q)
            % Get the driver position counter.
            %
            % Return:
            %  - dpc: the driver position counter.
            
            % Ask for the driver position counter.
            [~, Q.dpc] = qp_get_dpc(Q.ph);
            dpc = Q.dpc;
        end
        
        
        
        function volLeft = get_left_volume(Q)
            % Get the driver position counter.
            %
            % Return:
            %  - volLeft: the left volume in the syringe.
            
            % We get the volume in mm^3=�L, and we transform it into the
            % good unit.
            
           
            hmax = min(63, Q.sc(2));
            volLeft = ((Q.sc(1) / 2) ^ 2) * pi * ... % pi * r^2
                hmax * (Q.get_driver_pos_cnt / Q.dpcMax) ... % * h = 50mm * driv/drivMax
                * 10^(-6 - Q.vu(1)); % * unit
        end
        
        
        
        function volTot = get_total_volume(Q)
            % Get the driver position counter.
            %
            % Return:
            %  - volTot: the total volume of the syringe.
            
            hmax = min(63, Q.sc(2)); % Course : 63mm
            volTot = ((Q.sc(1) / 2) ^ 2) * pi * hmax * 10^(-6 - Q.vu(1)); %pi*r^2*h
        end
        
        
        
        function aspirate(Q, vol, flow)
            % Ask for the pump to aspirate. Both numbers are used on the assumption
            % of the unit define during initialization of the pump.
            %
            % Parameters:
            %  - vol: volume of the aspirate.
            %  - flow: flow used for the aspirate.
            
            % Ask for the aspirate.
            qp_aspirate(Q.ph, vol, flow);
        end
        
        
        
        function dispense(Q, vol, flow)
            % Ask for the pump to dispense. Both numbers are used on the assumption
            % of the unit define during initialization of the pump.
            %
            % Parameters:
            %  - vol: volume of the aspirate.
            %  - flow: flow used for the aspirate.
            
            % Ask for the aspirate.
            qp_dispense(Q.ph, vol, flow);
        end
        
        
        
        function generate_flow(Q, flow)
            % Change the flow rate of the pump.
            %
            % Parameter:
            %  - flow: new flow rate.
            
            qp_generate_flow(Q.ph, flow);
        end
        
        
        function duration = fill(Q)
            %FILL Fill the syringe to its max capacity and return the
            %   filling time, in seconds.
            % The aspiration is done at the maximum flow rate of the pump
            
            volTot = Q.get_total_volume(); % Maximum volume of the syringe
            volLeft = Q.get_left_volume(); % Left volume of the syringe
            flow = Q.get_max_flow();
            
            % We aspirate to fill the syringe
            Q.aspirate(volTot - volLeft, flow);
            
            % Then we calculate the needed time to aspirate
            % In order to know if the flow rate is in x/s or x/min :
            %   fl_u(3) = 1     if x/s
            %   fl_u(3) = 60    if x/min
            flow_unit = Q.get_flow_unit;
            vol_unit = Q.get_volume_unit;
            
            % Calculation of the needed time in s
            duration = flow_unit(3) * ((volTot-volLeft) * 10^vol_unit(1)) / ...
                (flow * 10^flow_unit(1));
        end
        
        
        function duration = empty(Q)
            %EMPTY Empty the syringe and return the emptying time, in 
            % seconds.
            % The dispensing is done at the maximum flow rate of the pump
            
            volLeft = Q.get_left_volume(); % Left volume of the syringe
            flow = Q.get_max_flow();
            
            % We empty all that is left
            Q.dispense(volLeft, flow);
            
            % Then we calculate the needed time to dispense
            % In order to know if the flow rate is in x/s or x/min :
            %   fl_u(3) = 1     if x/s
            %   fl_u(3) = 60    if x/min
            flow_unit = Q.get_flow_unit;
            vol_unit = Q.get_volume_unit;
            
            % Calculation of the needed time in s
            duration = flow_unit(3) * (volLeft * 10^vol_unit(1)) / ...
                (flow * 10^flow_unit(1));         
        end

        
        function set_fill_level(Q, fl, flow)
            % Ask for the syringe to go to a position corresponding to a fill level
            % of fl. This may lead to dispense or aspirate of fluid.
            %
            % Parameters:
            %  - fl: fill level we want for the syringe.
            %  - flow: flow used to go to the fill level.
            
            % Send the command to change the fill level.
            qp_set_fill_level(Q.ph, fl, flow);
        end
        
        
        
        function stop_all_pumps(~)
            % Stop all the pumps.
            
            qp_stop_all_pumps();
        end
        
        
        
        function stop_pumping(Q)
            % Stop the current pump.
            
            qp_stop_pumping(Q.ph);
        end
        
        
        
        function mf = get_max_flow(Q)
            % Retreive the maximum flow allowed in the syringe.
            %
            % Return:
            %  - mf: real of the maximum flow rate. Warning: Depend on the unit
            %        defined for the syringe.
            
            % Retreive the max flow accessible with the current syringe.
            
            mf = Q.maxFlowRate;
            %[~, mf] = qp_get_max_flow_rate(Q.ph);
        end
        
        
        
        function calibrate_move(Q)
            % Apply a calibration movement in order to go to the reference position
            % of the pump.
            
            % Apply the calibration movement.
            qp_syringe_calibrate(Q.ph);
        end
        
        
        
        function res = is_enable(Q)
            % Tell if the pump is enable.
            %
            % Return:
            %  - res = 0 if not enabled or 1 if enabled.
            
            res = qp_is_enable(Q.ph);
        end
        
        
        
        function enable(Q)
            % Enable the pump.
            
            qp_enable_pump(Q.ph);
        end
        
        
        
        function disable(Q)
            % Disable the pump.
            
            qp_disable_pump(Q.ph);
        end
        
        
        
        function res = is_in_fault(Q)
            % Tell if the pump is in fault.
            %
            % Return:
            %  - res = 0 if not enabled or 1 if enabled.
            
            res = qp_is_in_fault_state(Q.ph);
        end
        
        
        
        function clear_fault(Q)
            % Clear all faults linked to the pump.
            
            qp_clear_fault(Q.ph);
        end
        
        
        
        function res = is_pumping(Q)
            % Tell if the pump is ... pumping.
            %
            % Parameters:
            %  - res = 0 if not enabled or 1 if enabled.
            
            res = qp_is_pumping(Q.ph);
        end
        
        
        
        function wait_finish(Q)
            % Wait until the pump has finished its movement.
            
            % Until is_pumping is false, we wmake pauses.
            while(Q.is_pumping())
                disp(Q.is_pumping());
                pause(0.5);
            end
        end
        
        
        
        function res = has_valve(Q)
            % Check if the pump has a valve or not.
            %
            % Return:
            %  - res: 1 if there is a valve, 0 if not.
            
            res = qp_has_valve(Q.ph);
        end
        
        
        
        function res = get_valve(Q)
            % Construct a valve object with the handle of the valve connect to the
            % current pump. If no valve is connect to the pump [] is returned.
            %
            % Return:
            %  - res: the valve object if valve exist or [].
            
            [~, tmp] = qp_get_valve_handle(Q.ph);
            
            res = Qmix_Valve(-1, tmp);
        end
        
        
        
        function clear_all_batch(Q)
            % Clear the all list of batch entries.
            
            % Clear all batch entries.
            qp_clear_batch_list(Q.ph);
        end
        
        
        
        function err = read_last_dev_err(Q)
            % Read and return the last dev. error code.
            %
            % Return:
            %  - err: error code.
            
            [~, err] = qb_read_last_dev_err(Q.ph);
        end
        
        
        
        function wi = wrong_init(Q)
            % Tell if something goes wrong during instanciation.
            %
            % Return:
            %  - wi: true if something goes wrong during instanciation.
            
            wi = Q.wi;
        end
        
        
        
        function m = to_unit_pre(Q, v)
            % Listing of the meaning of each value used to define the prefix of a
            % unit used for a syringe (volume or flow).
            %
            % Parameter:
            %  - v: value to translate.
            %
            % Result:
            %  - m: meaning of the value.
            
            switch(v)
                case -2
                    m = 'centi';
                    
                case -1
                    m = 'deci';
                    
                case -6
                    m = '�';
                    
                case -3
                    m = 'm';
                    
                case 0
                    m = 'base unit';
                    
                otherwise
                    m = 'unknown';
            end
        end
        
        
        
        function m = to_unit_suf(Q, v)
            % Actually only one value is listed in the documentation which is 68
            % for liter so... We keep thing this way in case it may evolve.
            %
            % Parameters:
            %  - v: value to translate.
            %
            % Result:
            %  - m: meaning of the value.
            
            if(v == 68)
                m = 'L';
            else
                m = 'unknown';
            end
        end
        
        
        
        function m = to_unit_time(Q, v)
            % Translate the value used in definition of the flow rate in their
            % meaning (a string of the name of the unit).
            %
            % Parameters:
            %  - v: value of translate.
            %
            % Result:
            %  - m: meaning of the value.
            
            switch(v)
                case 3600
                    m = '/hour';
                case 60
                    m = '/mn';
                case 1
                    m = '/s';
                otherwise
                    m = 'unknown';
            end
        end
    end
end


