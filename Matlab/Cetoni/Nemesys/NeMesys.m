classdef NeMesys < Observable
    %NEMESYS this class is the model of the NeMesys MVC interface.
    %   This class got all the parameters of the NeMesys pumps connected to
    %   it, and their properties. It also has all the function to control
    %   them. Nevertheless, it can be used without implementing the MVC
    %   Pattern, as a control class of NeMesys pumps.
    %
    % Solvay 2017.07.07 - A. Gay
    % Based on classes by G.Lebrun
    
    properties
        pathconfig; % The path to the .xml folder
        qBus;       % The connected Qmix_Bus
        qPumps = [];% The list of connected Qmix_Pump
        qValve = [];% The list of connected Qmix_Valve
        flowList;   % The list of the current flows of the syringes
        
        names;      % The names of the syringes
        level;      % The filling level [0,1] of the syringes
        isPumping;  % The boolean if syringes are pumping
        estFlow;    % The estimated flows from measurements
        maxFlow;    % The maximum flows
        volWanted;  % The wanted volume in each syringe
        volLeft;    % The left volumes of the syringes
        volTot;     % The total volumes
        enabled;    % The booleans if the pumps are enabled
        inFault;    % The booleans if the pumps are in fault (bugged ?)
        timeLeft;   % The left time of pumping (function of flows and volumes) in s
        isInFeedPos;% The booleans if the valves are in feed or process position
        prevToc = 0;% The previous time (toc) of update
        
        isFeedPos;  % The booleans if the pumps is on 'feed' position
    end
    
    methods
        
        function N = NeMesys(pathConfig,qBus)
            % NEMESYS(nbSYR, pathConf, SParam, numFig)
            % This function initialise the pumps, from the configuration
            % XML files given.
            
            % We initialise the observable superclass
            N@Observable();
            tic;
            N.pathconfig = pathConfig;
            
            filePath = strcat(pathConfig, '\SyrParameters.mat');
            try
                load(filePath);
                if(exist('syrNames', 'var'))
                    N.names = syrNames;
                else
                    N.names = char({'Syr.1','Syr.2','Syr.3', 'Syr.4', 'Syr.5'});
                end
            catch exc
                h = errordlg(strcat('Failed to load parameters', ...
                    ', make sure the folder is the good one and tha', ...
                    't the initialisation wizard has already be launched.'), ...
                    'No parameters file');
                uiwait(h);
            end
            
            % We initialise the syringes
            N.initSyringes(nb_syr, pathConfig, sParam, pumpsID,qBus);
            N.loadSyringesPositions(pathConfig);
        end
        
        
        
        function delete(N)
           % This function delete the model but not the controller neither 
           % the observers(views)
           
           N.setFlows(zeros(size(N.qPumps)));

           N.saveSyringesPositions();
           
           % We put all the syringes in feed position
           for i=1:numel(N.qPumps)
               N.feedPosition(i);
           end
                      
           %% We delete the valves
           disp('Deleting Valves');
           for i=1:numel(N.qValve)
               N.qValve(i).delete;
           end
           
           %% We delete the pumps
           
           for i=1:numel(N.qPumps)
               fprintf('Deleting Pump n#%d\n', i);
               N.qPumps(i).delete;
           end
           
           
           %% We delete the bus
           try
               disp('Deleting Bus. May take a few seconds');
               N.qBus.delete;
           catch exc
               disp(exc.message)
               disp(exc.stack(1))
           end
           
           delete@Observable(N);
        end
 
        function updateDataSyringes(N)
            % N.UPDATEDATASYRINGES()
            % This function get all the data from the pumps and updates all
            % the observers of our object.
            
            try
                
                % We save the 'previous' volume left : if it's the first
                % launch of the function, we put them at 0
                if(numel(N.volLeft) == numel(N.qPumps))
                    volPrec = N.volLeft;
                else
                    volPrec = zeros(size(N.qPumps));
                end
                
                for i=1:numel(N.qPumps)
                    % For each pump, we get the data
                    qPump = N.qPumps(i);
                    
                    N.isPumping(i) = qPump.is_pumping;
                    N.enabled(i) = qPump.is_enable;
                    N.inFault(i) = qPump.is_in_fault;
                    
                    N.volLeft(i) = qPump.get_left_volume;
                    N.volTot(i) = qPump.get_total_volume;
                    N.maxFlow(i) = qPump.get_max_flow;
                    
                    %%%%%%%% WARNING : This could be source of errors
                    if(~N.isPumping(i))
                        N.flowList(i) = 0;
                    end
                    %%%%%%%%%%%%%%%%%
                    
                end
                
                % We calculate the estimated flows from the mesures
                N.estFlow = (volPrec - N.volLeft) / ... % mL
                    ((toc - N.prevToc)/60);          % min
                N.level = N.volLeft ./ N.volTot;
                
                N.prevToc = toc;

                % We calculate the time left
%                 N.timeLeft = 60 * (...
%                     (N.volLeft ./ N.flowList) .* (N.flowList > 0) + ... % Dispense
%                     -1 * ((N.volTot-N.volLeft) ./ N.flowList) .* (N.flowList < 0)); % Aspirate

                N.timeLeft = 60 * abs((N.volLeft - N.volWanted) ./ N.flowList);

                %                 sec = round((N.timeLeft - fix(N.timeLeft)) *60);
%                 
%                 N.timeLeft = fix(N.timeLeft) + sec/100;

            catch exc
                disp('A problem occured while updating Pumps data');
                disp(exc.message);
                disp(exc.stack(1));
            end

            N.updateObserver();            
        end
        
        
        
        function setFlows(N, flowList)
            % We apply the list of flows to the pumps
            
            % We apply the flow or the max
            N.flowList = sign(flowList) .* ...
                min(abs(flowList), N.maxFlow);
            
            for i=1:numel(N.qPumps)
                if(flowList(i) ~= N.flowList(i))
                    N.setFlowPumpIndex(i, flowList(i));
                end
                pause(0.1); %To be changed ?
            end
            
            % We change the flowList and update the observers
            N.flowList = flowList;
            N.updateObserver();
        end
        
        
        
        function setFlowIndex(N, index, flow)
            % We apply the new flow
            N.setFlowPumpIndex(index, flow);
            
            % We change the flowList
            N.flowList(index) = flow;
            
            % We update the observers
            N.updateObserver();
        end
        
        
        
        function setMaxFlowPump(N, index, maxFlow)
            % Set the max Flow of a pump
            
            N.qPumps(index).set_max_flow_rate(maxFlow);
            N.maxFlow(index) = maxFlow;
            
            N.updateObserver();
        end
        
        
        
        function clearFault(N, index)
            % This method clear the faults on the given pumps
            
            N.qPumps(index).clear_fault();
        end

        
        
        function enablePump(N, index)
            % This method clear the faults on the given pumps
            
            N.qPumps(index).enable();
        end

        
        
        function emptySyringe(N, index)
            % Empty a syringe using it's max flow rate
            
            N.setFlowIndex(index, N.maxFlow(index));
        end
        
        
        
        function fillSyringe(N, index)
            % Empty a syringe using it's max flow rate
            
            N.setFlowIndex(index, -1 * N.maxFlow(index));
        end
        
        
        
        function stopSyringe(N, index)
            % Stop a syringe
            
            N.setFlowIndex(index, 0);
        end
        
        
        
        function feedPosition(N, index)
            % This method put the corresponding valve to the 'feed'
            % position. Nevertheless, it should be edited to correspond to
            % your gestion of valves.
            
            qValveFeedPos = 1;

            switch index
                case 1
                    % We open the second electrovalve
                    N.qValve(1).switch_position(qValveFeedPos);
                case 2
%                     % We open the first electrovalve
%                     N.qValve(4).switch_position(qValveFeedPos);

                case 3
                    % We open the second electrovalve
                    N.qValve(3).switch_position(qValveFeedPos);
                case 4
                    % We open the first electrovalve
                    N.qValve(2).switch_position(qValveFeedPos);
                    
                case 5
                    % We open the second electrovalve
                    N.qValve(5).switch_position(qValveFeedPos);
            end
            
            N.isFeedPos(index) = 1;
            try
                N.saveFlowInFile(index, N.flowList(index))
            catch exc %#ok<NASGU>
            end
            N.updateObserver();
        end
        
        
        
        function processPosition(N, index)
            % This method put the corresponding valve to the 'process'
            % position. Nevertheless, it should be edited to correspond to
            % your gestion of valves.
            
            qValveProcessPos = 0;

            switch index
                case 1
                    % We open the second electrovalve
                    N.qValve(1).switch_position(qValveProcessPos);
                case 2
%                     % We open the first electrovalve
%                     N.qValve(4).switch_position(qValveProcessPos);

                case 3
                    % We open the second electrovalve
                    N.qValve(3).switch_position(qValveProcessPos);
                case 4
                    % We open the first electrovalve
                    N.qValve(2).switch_position(qValveProcessPos);
                    
                case 5
                    % We open the second electrovalve
                    N.qValve(5).switch_position(qValveProcessPos);
            end
            
            N.isFeedPos(index) = 0;
            N.saveFlowInFile(index, N.flowList(index))
            N.updateObserver();
        end
        
        
        
        function calibrateMove(N, index)
            % This function does a calibration movement : it sets the pump
            % at the initial position and re-init the driver
            
            % We say to the pump that it's full and empty it
            N.qPumps(index).set_driver_pos_cnt(N.qPumps(index).dpcMax);
            N.emptySyringe(index);
            
            % We wait for it to be empty
            while(N.qPumps(index).is_enable && ...
                    N.qPumps(index).is_enable)
                pause(5);
            end
            
            if(N.qPumps(index).is_enable)
                % If it's still enabled, we try again
                N.qPumps(index).set_driver_pos_cnt(N.qPumps(index).dpcMax);
                N.emptySyringe(index);
                
                while(N.qPumps(index).is_enable && ...
                        N.qPumps(index).is_enable)
                    pause(5);
                end
            end
            
            % Then we clear the fault, re-init the driver and empty 0.5% of
            % its content before re-setting the driver to 0
            N.clearFault(index);
            N.enablePump(index);
            N.setDriverPos(index, 0);
            N.qPumps(index).aspirate(...
                0.005 * N.qPumps(index).get_total_volume(),...
                N.maxFlow(index));
            while(N.isPumping(index))
                pause(1)
            end
            N.setDriverPos(index, 0);
        end
        
        
        
        function dispVolumeIndex(N, index, volume, flow)
            % SETFLOWPUMP(qPump, flow) This method safely generate a flow
            % on a pump, to aspirate or dispense the given volume
            % To do that, it apply to the corresponding pump an aspirate or
            % a dispense of a given volume at the needed flow. In this way,
            % the pump won't bug by going over it's limits
            % Parameters
            %   index : the index of the Qmix_Pump
            %   volume : the wanted volume to aspirate or dispense
            %   flow : the wanted flow (>=0 or <0)

            qPump = N.qPumps(index);
            
            if(abs(flow) > N.maxFlow(index))
                flow = N.maxFlow(index) * sign(flow);
            end
            
            if(flow == 0) % Stopping
                qPump.dispense(N.volLeft(index), flow);
                N.volWanted(index) = N.volLeft;
                
            elseif(flow > 0) % Dipensing
                if(N.volLeft(index) == 0) % Empty syringe
                    fprintf('Warning : The syringe n# %d is empty while a positive flow is applied on it.\n',...
                        index);
                else % We dispense
                    qPump.dispense(min(N.volLeft(index), volume), flow);
                    N.volWanted(index) = max(0, N.volLeft(index)-volume);
                end
                
            else % Aspirating
                if (N.volLeft(index) == N.volTot(index)) % Full syringe
                    fprintf('Warning : The syringe n# %d is full while a negative flow is applied on it.\n',...
                        index);
                else % We aspirate
                    qPump.aspirate(...
                        min(N.volTot(index) - N.volLeft(index), volume),...
                        -flow);
                    N.volWanted(index) = min(N.volTot(index),...
                        N.volTot(index) + volume);
                end
            end
            
            N.updateDataSyringes();
        end
        
        
        
        function setDriverPos(N, index, driverPos)
            % This method change the value of the driver position count of
            % a pump. It should only be used if you know what you're doing
            
            N.qPumps(index).set_driver_pos_cnt(driverPos);
        end

        function loadSyringesPositions(N, pathconfig)
            % This method load the previous positions of the syringes.
            
            try
                filePath = strcat(pathconfig, '\syrPosition.mat');
                if exist(filePath, 'file')
                    load(filePath, 'syrPosition');

                    % If the drivers are all '0'
                    isZeroPositionDriver = 1;
                    for i=1:numel(N.qPumps)
                        isZeroPositionDriver = isZeroPositionDriver & ...
                            (N.qPumps(i).get_driver_pos_cnt == 0);
                    end
                    
                    % If the saved positions of drivers are all '0'
                    isZeroPosSave = 1;
                    for i=1:numel(N.qPumps)
                        isZeroPosSave = isZeroPosSave & ...
                            (syrPosition(i) == 0);
                    end

                    if(isZeroPositionDriver && ~isZeroPosSave)
                        
                        choiceNoBug = 'Yes, my syringes are empty : no bug';
                        choiceBug = 'No, they''re not empty : correct the bug';
                        questionText = strcat('The internal memory of the p', ...
                            'umps indicates that all of your synringes ar', ...
                            'e totally empty. However, this could be a bu', ...
                            'g, which can be corrected. Are all your synr', ...
                            'inges empty ?');
                        answer = questdlg(questionText ,...
                            'Possible bug', choiceNoBug, choiceBug, ' ');
                        
                        if(strcmp(answer, choiceBug))
                            for i=1:numel(N.qPumps)
                                N.setDriverPos(i, syrPosition(i));
                            end
                        end
                    end
                end
            catch exc
                disp('Failed to load previous syringes positions')
                disp(exc.message)
                disp(exc.stack(1))
            end
        end
        
        function saveSyringesPositions(N)
            % This method saves the positions of the syringes
            
            syrPosition = 1:numel(N.qPumps);
            for i=1:numel(N.qPumps)
                syrPosition(i) = N.qPumps(i).get_driver_pos_cnt;
            end
            
            filePath = strcat(N.pathconfig, '\syrPosition.mat');
            save(filePath, 'syrPosition');
        end

        
    end

    
    methods (Access = private)
        
        function initSyringes(N, nbSyringes, pathConfig, sParam, pumpsID,qBus)
            disp('D�but Initialisation Seringues');
            
            %% Initialisation of the Qmix_Bus
            %N.qBus = Qmix_Bus(pathConfig);
            N.qBus = 
            %% Initialisation of the Qmix_Pumps.
            
            % The order of the ports is meant to get a logical 'physic'
            % order
            N.qPumps = [];
            for i=1:nbSyringes
                N.qPumps = [N.qPumps Qmix_Pump(N.qBus, pumpsID(i))];
            end
%             if(nbSyringes >= 1), N.qPumps = [N.qPumps Qmix_Pump(N.qBus, 3)]; end
%             if(nbSyringes >= 2), N.qPumps = [N.qPumps Qmix_Pump(N.qBus, 2)]; end
%             if(nbSyringes >= 3), N.qPumps = [N.qPumps Qmix_Pump(N.qBus, 1)]; end
%             if(nbSyringes >= 4), N.qPumps = [N.qPumps Qmix_Pump(N.qBus, 0)]; end
%             if(nbSyringes >= 5), N.qPumps = [N.qPumps Qmix_Pump(N.qBus, 4)]; end
            
            %% Initialisation of the Qmix_Valve
            for i=1:numel(N.qPumps)
                N.qValve = [N.qValve N.qPumps(i).get_valve()];
            end
            
            %% Initialisation of the parameters of the pumps and syringes
            
            for i=1:numel(N.qPumps)
                N.qPumps(i).import_syringe_params(sParam(i));
                N.qPumps(i).enable();
                N.feedPosition(i);
            end
                        
            N.isInFeedPos = ones(1, nbSyringes);
            
            %% Then we update the data for the first time
            
            N.updateDataSyringes();
            N.volWanted = N.volLeft;
            % NB : The list of observers will be empty on this first update
            
            disp('Fin Initialisation Seringues');
        end
        
        
        
        
        function setFlowPumpIndex(N, index, flow)
            % SETFLOWPUMP(qPump, flow) This method safely generate a flow
            % on a pump.
            % To do that, it apply to the corresponding pump an aspirate or
            % a dispense of a given volume at the needed flow. In this way,
            % the pump won't bug by going over it's limits
            % Parameters
            %   index : the index of the Qmix_Pump
            %   flow : the wanted flow (>=0 or <0)
                                    
            qPump = N.qPumps(index);
            
            if(abs(flow) > N.maxFlow(index))
                flow = N.maxFlow(index) * sign(flow);
            end
            
            if(flow == 0) % Stopping
                qPump.dispense(N.volLeft(index)/2, flow);
                N.volWanted(index) = N.volLeft(index);
            elseif(flow > 0) % Dipensing
                if(N.volLeft(index) == 0) % Empty syringe
                    fprintf('Warning : The syringe n# %d is empty while a positive flow is applied on it.\n',...
                        index);
                else % We dispense
                    qPump.dispense(N.volLeft(index), flow);
                    N.volWanted(index) = 0;
                end
            else % Aspirating
                if (N.volLeft(index) == N.volTot(index)) % Full syringe
                    fprintf('Warning : The syringe n# %d is full while a negative flow is applied on it.\n',...
                        index);
                else % We aspirate
                    qPump.aspirate(N.volTot(index) - N.volLeft(index), -flow);
                    N.volWanted(index) = N.volTot(index);
                end
            end
            
            % We save the flow
            N.saveFlowInFile(index, flow)
            N.updateDataSyringes();
        end
        
        
        
        function saveFlowInFile(N, index, flow)
            % This method writes the change of flows
            
            try
                filePath = strcat(N.pathconfig, '\flowHistoric.csv');
                
                if(~exist(filePath, 'file'))
                    file = fopen(filePath, 'a');
                    fwrite(file, 'year, month, day, seconds, index, flow, isFee');
                    fclose(file);
                    dlmwrite(filePath,'d','delimiter',',','-append');
                end
                
                C = clock;
                year = C(1);
                month = C(2);
                day = C(3);
                seconds = C(4) * 3600 + C(5)*60 + C(6);
                List = [year month day seconds index flow N.isFeedPos(index)];
                dlmwrite(filePath,List,'delimiter',',','-append');
            catch exc
                disp('Failed to save Flows')
                disp(exc.message)
                disp(exc.stack(1))
            end
        end
    end
    
end

