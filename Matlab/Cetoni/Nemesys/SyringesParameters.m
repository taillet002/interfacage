function sParam = SyringesParameters(unites, debitMax, diametres, hauteurs, driverMax)
%SYRINGESPARAMETERS Fournit la liste des param�tres des seringues
%   s_param : liste des structures de param�tres des seringues, dans le bon
%       ordre. A modifier en fonction des seringues utilis�es


length = min([numel(unites) numel(debitMax) numel(diametres) numel(hauteurs)]);

for i=length:-1:1
    
    % Pour les unit�s : [-6, 68, 1] => [10^-6 (L), 68 = L, 1 = s]

    if (unites(i) == 1) % mL/min
        flow = [-3, 68, 60];
        vol = [-3, 68];
    elseif(unites(i) == 2) % �L/s
        flow = [-6, 68, 1];
        vol = [-6, 68];
    end
    
    sParam(i) = struct('flow', flow, 'vol', vol,...
        'syr', [diametres(i), hauteurs(i)], ... % Diametre et hauteur de seringue en mm
        'dpcMax', driverMax(i), 'flowMax', debitMax(i));
    
end

end

