% This script will guide you to the creation of the parameters for the
% NeMesys Interface

%% We get the path
h = msgbox(strcat('Welcome in this initialisation wizard for the NeMesy', ...
    's interface. First step, in the next window, please choose the pat', ...
    'h to the configuration folder of your NeMesys installation', ...
    '. For more information, see the guide'), ...
    'Initialisation Wizard');
uiwait(h);

h = warndlg(strcat('Be sure that the folder (and subfolders,', ...
    ') ''QmixSDK-64bit'' are in your path before to continue'), ...
    'Initialisation Wizard');
uiwait(h);


folderPath = uigetdir('Path to your config');

%% We get the number of syringes
waiting = 1;
while(waiting)
    answer = inputdlg('How many syringes do you have on your NeMesys ?', ...
        'Initialisation Wizard');
    
    try
        nb_syr = str2double(answer);
        if(numel(nb_syr) ~= 0 && fix(nb_syr) == nb_syr)
            waiting = 0;
        else
            h = msgbox(strcat('Your previous input could not be transfo', ...
                'rmed to a number, please try again.'), ...
                'Initialisation Wizard');
            uiwait(h);
        end
    catch exc
    end
end

clear waiting
%% And finally, we get the parameters of the syringes

% We set the defaults data
data = [nb_syr-2:-1:0 nb_syr-1 ; ...
    zeros(1,nb_syr); ...
    zeros(1,nb_syr); ...
    zeros(1,nb_syr)];

try
    filePath = strcat(folderPath, '\SyrParameters.mat');
    nb_syr_new = nb_syr;
    load(filePath);
    nb_syr = nb_syr_new;
    
    sizeTab = min(nb_syr, size(pumpsID,2));
    
    data(1,1:sizeTab) = pumpsID(1:sizeTab);
    data(2,1:sizeTab) = maxFlows(1:sizeTab);
    data(3,1:sizeTab) = diameters(1:sizeTab);
    data(4,1:sizeTab) = height(1:sizeTab);
catch exc
    disp(exc.message)
    disp(exc.stack(1))
end

% If syrnames already exists, it's used to set the names. Otherwise,
% they're named '1' to '5'
if(~exist('syrNames', 'var') || size(syrNames, 1) < nb_syr)
    syrNames = [];
    for i=1:nb_syr
        syrNames = [syrNames; num2str(i)];
    end
    syrNames = char(syrNames);
elseif(size(syrNames, 1) > nb_syr)
    syrNames = syrNames(1:nb_syr, :);
end

% We show the figure and table
f = figure();

table = uitable(f, ...
    'Data', data,...
    'Units', 'Normalized', ...
    'Position', [0.02 0.10 0.96 0.75], ...
    'ColumnWidth', {100 100 100 100 100},...
    'ColumnEditable', true, ...
    'RowName', {'Pump id'; 'max flow (mL/min)'; 'diametres (mm)'; 'height (mm)'},...
    'ColumnName', syrNames);

button = uicontrol('Parent',f, ...
    'Style','togglebutton', ...
    'String','OK', ...
    'Units', 'Normalized', ...
    'Position', [0.52 0.02 0.46 0.08], ...
    'Visible','on');

% We change the width of the table column
nbElem = get(table, 'Data');
nbElem = size(nbElem, 2);

b1 = 100;
set(table, 'ColumnWidth', num2cell(b1 * ones(1, nbElem)));
pos = get(table, 'Extent');
x1 = pos(3);

b2 = 50;
set(table, 'ColumnWidth', num2cell(b2 * ones(1, nbElem)));
pos = get(table, 'Extent');
x2 = pos(3);

pos = get(table, 'Position');
widthTot = nbElem * (b1-b2) / (x1-x2);
widthHead = widthTot * x1 - nbElem * b1;

b_opt = fix((widthTot * pos(3) - widthHead) / nbElem)-1;

set(table, 'ColumnWidth', num2cell(b_opt * ones(1, nbElem)));

clear b_opt b1 b2 nbElem pos widthTot widthHead x1 x2

% We wait for the button to be clicked

while(get(button, 'Value') == 0)
    pause(0.5)
end

data = get(table, 'Data');
close(f);

pumpsID = data(1,:);
maxFlows = data(2,:);
diameters = data(3,:);
height = data(4,:);
driverMax = -1815000 * ones(1,nb_syr);

sParam = SyringesParameters(ones(1,nb_syr), maxFlows, diameters, height, driverMax); %#ok<NASGU>

%% We save a first version of the parameters

filePath = strcat(folderPath, '\SyrParameters.mat');

save(filePath, 'nb_syr', 'syrNames', 'sParam', 'pumpsID', 'maxFlows', 'diameters', 'height');

%% We then try to control the syringes, in order to check the paramters

h = msgbox(strcat('The wizard will now try to create the interface to se', ...
    't some other parameters. IF THIS BUG, see the guide for precis', ...
    'e explainations and possible soltions.'), ...
    'Initialisation Wizard');
uiwait(h);

h = warndlg(strcat('BE CAREFUL : In the next operation, the syringes wil', ...
    'l move, theorically just a few, but will move anyway at the previousl', ...
    'y given maximum flows. Make sure that nothing will block them or un', ...
    'plug them for a while.'), ...
    'Initialisation Wizard');
uiwait(h);

% We create the interface
N = NeMesys(folderPath);

C = ControllerNeMesys(N);

V = ViewNeMesys(C, 1);

pause(5);

% For each syringe, we impose a flow. Then, if the estimated flow is not
% the good one, then our value of max driver is not the good one, so we
% modify it.
for index=1:numel(N.qPumps)
    volTot = N.volTot(index);
    volLeft = N.volLeft(index);
    maxFlow = N.maxFlow(index);
    if(volLeft < 0.5 * volTot)
        C.setFlowIndex(index, -maxFlow);
    else
        C.setFlowIndex(index, maxFlow);
    end
    pause(1)
    time = 0.1 * N.timeLeft(index);
    
    meanFlow = 0;
    for j=1:10
        pause(time/10)
        meanFlow = meanFlow + N.estFlow(index);
    end
    C.stopSyringe(index)
    pause(1)
    
    meanFlow = meanFlow / 10;
    
    ratio = abs(meanFlow / maxFlow);
    
    driverMax(index) = ratio * driverMax(index) - 1000;
end

% We recalculate the param and re-save them
sParam = SyringesParameters(ones(1,nb_syr), maxFlows, diameters, height, driverMax);

filePath = strcat(folderPath, '\SyrParameters.mat');

save(filePath, 'nb_syr', 'syrNames', 'sParam', 'pumpsID');

V.delete;

%% End of the script

h = msgbox(strcat('The wizard has now finished the configuration of you', ...
    'r NeMesys configuration. You can now use the interface as describe', ...
    'd in the guide.'), ...
    'Initialisation Wizard');
uiwait(h);
