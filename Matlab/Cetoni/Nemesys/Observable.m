classdef Observable < handle
    %OBSERVABLE SuperClass of an observable object in view of MVC pattern
    %   This class can be observed by many observers, who'll be updated
    %   when this class change.
    
    properties
        listObserver = [];  % The list of the observers of the object
    end
    
    methods
        
        function O = Observable()
            % Creation of the object
        end
        
        
        
        function delete(O)
           % Deleting the object
           
           O.resetObserver()
        end
        
        
        
        function addObserver(O, observer)
            % Add an observer to the list
            O.listObserver = [O.listObserver observer];
        end
        
        
        
        function deleteObserver(O, observer)
            % Delete an observer of the list
            O.listObserver(O.listObserver == observer) = [];
        end
        
        
        
        function resetObserver(O)
            % Remove all observers from the list
            O.listObserver = [];
        end
        
        
        
        function updateObserver(O)
            % Call the update function of all the  observers
            for i=1:numel(O.listObserver)
                O.listObserver(i).update(O);
            end
        end
    end
end

