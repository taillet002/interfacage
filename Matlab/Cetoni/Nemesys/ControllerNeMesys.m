classdef ControllerNeMesys < handle
    %CONTROLLER This class is the controler of the neMesys MVC Interface
    %   This class got the Nemesys model and all the function to control it
    %   (with all the needed verifications). It also got the timer of the
    %   update of NeMesys data for the View
    %
    % Solvay 2017.07.07 - A. Gay
    % Based on classes by G.Lebrun
    
    properties
        NeMesys;
        timerUpdate;
    end
    
    methods

        function C = ControllerNeMesys(NeMesys)
            C.NeMesys = NeMesys;
            
            C.timerUpdate = timer('TimerFcn',{@C.updateData},...
                'Period', 1,  ...
                'ExecutionMode', 'fixedDelay');
            
            start(C.timerUpdate);
        end
        
        
        
        function delete(C)
            % This method delete the controler but not the model
            
            stop(C.timerUpdate);
            
            C.NeMesys.delete();
        end
        
        
        
        function updateData(varargin)
            % This method launch the update of the pumps data of the
            % NeMesys model class
            
            try
                % I get C and I launch the update.
                C = varargin{1};
                C.NeMesys.updateDataSyringes();
                
                if (nargin >= 2)
                    timer = varargin{2};
                    cpt = get(timer, 'TasksExecuted');
                    
                    if(mod(cpt, 10) == 0)
                        C.NeMesys.saveSyringesPositions();
                    end
                end
            catch exc
                disp(exc.message);
                disp(exc.stack(1));
            end
        end
        
        
        
        function setFlows(C, flowList)
            % this method apply the list of flows on the pumps
            
            try
                if(numel(C.NeMesys.qPumps) ~= 0 && ...
                        numel(C.NeMesys.qPumps) == numel(flowList))
                    C.NeMesys.setFlows(flowList);
                end
            catch exc
                disp(exc.message);
                disp(exc.stack(1));
            end
        end
        
        
        
        function setFlowIndex(C, indexList, flowList)
            % this method apply the list of flows on the pumps

            try
                for i=1:numel(indexList)
                    index = indexList(i);
                    flow = flowList(i);
                    if(numel(C.NeMesys.qPumps) >= index)
                        if(C.NeMesys.flowList(index) ~= 0)
                            C.stopSyringe(index);
                            pause(0.1)
                        end
                        C.NeMesys.setFlowIndex(index, flow);
                        pause(0.1)
                    end
                end
            catch exc
                disp(exc.message);
                disp(exc.stack(1));
            end
        end
        
        
        
        function setMaxFlowIndex(C, indexList, maxFlowList)
            % This function update the maximum flow of a syringe
            
            for i=1:numel(indexList)
                index = indexList(i);
                maxFlow = maxFlowList(i);
                if(isnumeric(maxFlow) && isnumeric(index) && maxFlow >= 0)
                    C.NeMesys.setMaxFlowPump(index, maxFlow);
                end
            end
        end
        
        
        
        function emptySyringe(C, indexList)
            % Empty a syringe, but verify first that it's available and not
            % already empty
                        
            for index=indexList
                if(numel(C.NeMesys.qPumps) >= index && ...
                        C.NeMesys.volLeft(index) ~= 0)
                    C.NeMesys.emptySyringe(index);
                    pause(0.1)
                end
            end
        end
        
        
        
        function fillSyringe(C, indexList)
            % Fill a syringe, but verify first that it's available and not
            % already full
                        
            for index=indexList
                if(numel(C.NeMesys.qPumps) >= index && ...
                        C.NeMesys.volLeft(index) ~= C.NeMesys.volTot(index))
                    C.NeMesys.fillSyringe(index);
                    pause(0.1)
                end
            end
        end
        
        
        
        function debugPump(C, indexList)
            % This method debugs the pumps of the list
            
            % First, we clear faults
            for index=indexList
                if(numel(C.NeMesys.qPumps) >= index && ...
                        C.NeMesys.volLeft(index) ~= C.NeMesys.volTot(index))
                    C.NeMesys.clearFault(index);
                end
            end

            pause(0.1)
            % Then, we enable
            for index=indexList
                if(numel(C.NeMesys.qPumps) >= index && ...
                        C.NeMesys.volLeft(index) ~= C.NeMesys.volTot(index))
                    C.NeMesys.enablePump(index);
                end
            end
        end
        
        
        function dispense(C, index, volume, flow)
            % Dispense a volume, but verify first that the syringe's 
            % available and not already empty
                        
            if(numel(C.NeMesys.qPumps) >= index && ...
                    C.NeMesys.volLeft(index) ~= 0)
                C.NeMesys.dispVolumeIndex(index, volume, abs(flow));
            end
        end
        
        
        
        function aspirate(C, index, volume, flow)
            % Aspirate a volume, but verify first that the syringe's 
            % available and not already full
                        
            if(numel(C.NeMesys.qPumps) >= index && ...
                    C.NeMesys.volLeft(index) ~= C.NeMesys.volTot(index))
                C.NeMesys.dispVolumeIndex(index, volume, -1 * abs(flow));
            end
        end
        
        
        
        function stopSyringe(C, indexList)
            % Stop the flow of a syringe. As it is a security function,
            % there will be no verification if the syringe is already
            % stopped.
            
            for index=indexList
                if(numel(C.NeMesys.qPumps) >= index)
                    C.NeMesys.stopSyringe(index);
                    pause(0.1)
                end
            end
        end
        
        
        
        function feedPosition(C, indexList)
            % Change the position of the valve of a pump
            for index=indexList
                if(numel(C.NeMesys.qPumps) >= index)
                    C.NeMesys.feedPosition(index);
                    pause(0.1)
                end
            end
        end
        
        
        
        function processPosition(C, indexList)
            % Change the position of the valve of a pump
            
            for index=indexList
                if(numel(C.NeMesys.qPumps) >= index)
                    C.NeMesys.processPosition(index);
                    pause(0.1)
                end
            end
        end
        
        
        function setVolume(C, index, volume)
            
            
            if(volume >= 0 && volume <= C.NeMesys.volTot(index))
                qP = C.NeMesys.qPumps(index);
                hmax = 63; % Course : 63mm
                
                dpc = qP.dpcMax * qP.get_left_volume / ...
                    (((qP.sc(1) / 2) ^ 2) * pi * hmax * ...
                    10^(-6 - qP.vu(1)));
                
                C.NeMesys.setDriverPos(index, dpc);
            end
        end
    end
    
end

