classdef Qmix_Bus < handle
% Interface to Qmix bus whose unique goal is to keep track of the fact that
% the bus has been opened or not (for example when we connect to the second
% pump we want to know that we already initialize the bus for the first
% pump). Actually each neMESYS pump or neMAXYS axis is initialize
% individually.
%
% Solvay 2016.06.01 - G. Lebrun

  properties(SetAccess = private, GetAccess = private)
    % Configuration file path.
    cf_path = [];
    
    % Counter on neMESYS or neMAXYS opened axis.
    nem_cnt = 0;
  end
  
  
  
  methods
    function Q = Qmix_Bus(cf_path)
    % We do nothing but keep track of the configuration file.
    
      % Set the configuration file.
      Q.cf_path = cf_path;
    end
    
    
    
    function delete(Q)
    % To make sure that the bus is free once we suppress the Qmix_Bus
    % instance, we close the bus at deletion.
    
      % .
      qb_close_bus();
    end
    
    
    
    function add_axis(Q)
    % Tell the instance that a new axis is added.
    
      % Increment the counter.
      Q.nem_cnt = Q.nem_cnt + 1;
      
      % If is at 1 we open the bus.
      if(Q.nem_cnt == 1)
        qb_init_bus(Q.cf_path);
      end
    end
    
    
    
    function del_axis(Q)
    % Tell the instance that an axis has been deleted.
    
      % Decrement the counter.
      Q.nem_cnt = Q.nem_cnt - 1;
      
      % If nem_cnt is at 0 we stop the bus.
      if(Q.nem_cnt == 0)
        qb_stop_bus();
      end
    end
  end
end

