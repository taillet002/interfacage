classdef Qmix_Valve < handle
% Interface to Qmix valves. It handle their detection, setup and switch.
% Their detection is also possible through Qmix_Pump interface.
%
% Solvay 2016.06.06 - G. Lebrun

  properties(SetAccess = private, GetAccess = private)
    % Valve Qmix handle.
    vh = [];
    
    % Actual position.
    pos = -1;
    
    % Number of positions.
    np = 0;
  end
  
  
  
  methods
    function Q = Qmix_Valve(id, vh)
    % Initialize the valve object with either the index of the valve of the
    % handle (in case of instanciation from Qmix_Pump).
    %
    % Parameters:
    %  - id: index of the valve in the bus.
    %  - vh: valve handle.
    
      if(nargin == 1)
        % Ask of the valve of position 'id'.
        [~, Q.vh] = qv_get_valve_handle(id);  
      elseif(nargin == 2)
        % Set valve handle from parameters.
        Q.vh = vh;
      end
      
      % Ask number of position and actual position just to update them in
      % the instance.
      Q.get_actual_position();
      Q.get_num_position();
    end
    
    
    
    function disp(Q)
    % Overload disp to display some of the important characteristics.
    
      fprintf('Valve handle: %d\n', Q.vh);
      fprintf('Current position: %d\n', Q.pos);
      fprintf('Number of positions: %d\n', Q.np);
    end
    
    
    
    function res = get_actual_position(Q)
    % Get the actual position of the valve.
    %
    % Return:
    %  - res: actual position of the valve.
    
      res = qv_get_actual_position(Q.vh);
      
      % Update the instance.
      Q.pos = res;
    end
  
    
    
    function switch_position(Q, pos)
    % Change the position of the valve to pos.
    %
    % Parameter:
    %  - pos: index of the new position of the valve.
      
      tmp = qv_switch_position(Q.vh, pos);
      
      % Update the instance.
      if(tmp == 0)
        Q.pos = pos;
      end
    end
    
    
    
    function np = get_num_position(Q)
    % To check the number of position of the valve.
    
      np = qv_get_num_pos(Q.vh);
      
      % Update the instance.
      Q.np = np;
    end
  end
end

