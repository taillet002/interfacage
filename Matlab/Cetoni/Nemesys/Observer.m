classdef Observer < handle
    %OBSERVER SuperClass of an observer object, in view of the MVC Pattern
    %   This class can observe an observable object, which means it'll be
    %   updated when the observable's values change.
    
    properties
    end
    
    methods
        
        function O = Observer(observable)
            % Create the object by adding it to the observable list
            observable.addObserver(O);
        end
        
        
        
        function delete(O) %#ok<INUSD>
%             delete@handle();
        end
        
        
        
        function update(O, observable) %#ok<INUSD>
            % This function is meant to update all the elements of the 
            % objects which need to be when the observable is updated
        end
    end
    
end

