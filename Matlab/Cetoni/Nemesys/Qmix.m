classdef Qmix < handle
% Class interface to Qmix which control neMESYS and neMAXYS tools.
%
% Solvay 2016.03.08 - G. Lebrun

  properties(SetAccess = private, GetAccess = private)
    % Constant (TODO: make a configuration file support).
    LCMC = 'labbCAN_MotionControl_API';
    LCB = 'labbCAN_Bus_API';
    LCP = 'labbCAN_Pump_API';
    path_conf = 'C:\Users\Public\Documents\QmixElements\';
    path_qmix = 'D:\QmixSDK-64bit\';
    
    % Counter on the number of loading of each library (to know when to
    % actually unload them if several Qmix element are used).
    LCMC_cpt = 0;
    LCB_cpt = 0;
    LCP_cpt = 0;
  end
  
  
  
  methods
    function Q = QMix()
    % Nothing special here. Most of the code will be in daughter class like
    % neMESYS or neMAXYS ones.
    
    end
  end

    
    
  methods(Access = protected)
    function load_Bus(Q)
    % Simple internal methods to load labbCAN_Bus_API library if not already done.  
      
      if ~libisloaded(Q.LCB)
        disp('Load labbCAN_Bus_API');
        
        % Loading of the library controlling the bus.
        log.res_load_bus_api = loadlibrary(Q.LCB, ...
          [Q.path_qmix, 'include\', Q.LCB, '.h']);
      else
        Q.LCB = Q.LCB + 1;
      end
    end
    
    
    
    function load_MotionControl(Q)
    % Simple internal methods to load labbCAN_MotionControl_API library if not already done.
    
      if ~libisloaded(I.LCMC)
        disp('Load labbCAN_MotionControl_API');
        
        % Load of the library controlling axis.
        log.res_load_MC_API = loadlibrary(I.LCMC, ...
          [I.path_qmix, 'include\', I.LCMC, '.h']);
      else
        Q.LCMC_cpt = Q.LCMC + 1;
      end
    end
    
    
    
    function load_Pump_API(Q)
    % Simple internal methods to load labbCAN_Pump_API library if not already done.
      
      if ~libisloaded(I.LCP)
        disp('Load labbCAN_Pump_API');
        
        % Load of the library controlling axis.
        log.res_load_MC_API = loadlibrary(I.LCP, ...
          [I.path_qmix, 'include\', I.LCP, '.h']);
      else
        Q.LCP_cpt = Q.LCP_cpt + 1;
      end
    end
    
    
    
    function unload_Bus(Q)
    % Simple internal methods to unload labbCAN_MotionControl_API library
    % if no device point to it anymore.
      
      if(Q.LCB_cpt > 0)
        Q.LCB_cpt = Q.LCB_cpt - 1;
      elseif libisloaded(Q.LCB)
        disp('Shutting down communications.');
        
        % Stop all LabCanBus communications before closing.
        errCode = calllib(N.LCB, 'LCB_Stop');
        if(errCode ~= 0)
          warndlg('Can''t stop the communication with the LabCanBus.', 'Zyxfu error');
        end
        
        % Close the LabCanBus modules.
        errCode = calllib(N.LCB, 'LCB_Close');
        if(errCode ~= 0)
          warndlg('Can''t stop the LabCanBus.', 'Zyxfu error');
        end
        
        disp('labbCAN_Bus_API unloading');
        % Unload Bus library.
        unloadlibrary(Q.LCB);
      end
    end
    
    
    
    function unload_MotionControl(Q)
    % Simple internal methods to unload labbCAN_MotionControl_API library
    % if no device point to it anymore.

      if(Q.LCMC_cpt > 0)
        Q.LCMC = Q.LCMC - 1;
      elseif libisloaded(Q.LCMC)
        disp('Shutting down Axes modules');
        
        % Close LabCanAxisSystem modules.
        errCode = calllib(Q.LCMC, 'LCA_CloseAxesModule', N.hAxisMod);
        if(errCode ~= 0)
          warndlg('Can''t close the LabCanAxisSystem.', 'Zyxfu error');
        end
        
        disp('labbCAN_MotionControl_API unloading');
      
        % Unload MotionControl library.
        unloadlibrary(Q.LCMC);
      end
    end
    
    
    
    function unload_Pump_API(Q)
    % Simple internal methods to unload labbCAN_MotionControl_API library
    % if no device point to it anymore.
      
      if(Q.LCP_cpt > 0)
        Q.LCP = Q.LCP - 1;
      elseif libisloaded(Q.LCP)
        disp('labbCAN_Pump_API unloading');
        
        % Unload MotionControl library.
        unloadlibrary(Q.LCP);
      end
    end
  end
end

