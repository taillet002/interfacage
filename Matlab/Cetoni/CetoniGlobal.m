classdef CetoniGlobal < handle
    properties(SetAccess = public, GetAccess = public)
        path_conf = ''
        ne
        na
        qBus
    end
    methods
        function C = CetoniGlobal(path_conf)
            C.path_conf = path_conf;
            
            % Connexion au Bus
            C.qBus = qb_init_bus(path_conf);
            if(C.qBus ~= 0)
                warndlg('Can''t start the communication with the LabCanBus.', 'Zyxfu error');
                return;
            end
           
            C.na = neMAXYS(C.qBus);
            C.ne = neMesys(path_conf,C.qBus);

        end
    end
end
        % Initializatio