classdef Cetoni < handle
    properties(SetAccess = public, GetAccess = public)
        path_conf = ''
        
        logs = struct(); % All logs of calls in a structure.
        axsys_h = []; % Handle on the LabCanAxisSystem axis system.
        
        % Handler on the 3 axes.
        ax0_h = [];
        ax1_h = [];
        ax2_h = [];
        hLog = []; % Handle on LabCanBus log file.
        
        %-- Set of data members link to geometry of the plateform.
        max_pos = [0 0 0]; % Vector of axis maximum positions (for each 0 being min).
        min_pos = [0 0 0]; % Vector of axis maximum positions (for each 0 being min).
        max_vel = [0 0 0]; % Vector of axis minimum velocity (for each 0 being min).
        check_period = 1; % Period of verification of the position of positions.
        
        %-- Set of user defined characteristic of the table.
        vel = [1 1 1]; % vector of chosen velocities.
        
        
    end
    methods
        % Initialization
        function C = Cetoni(path_conf)
            C.path_conf = path_conf;
            
            % We connect to the bus
            C.logs.load_conf = qb_init_bus(path_conf);
            init_Nemaxys()
            
            % 
            
            
        end
        function init_Nemaxys(C)
            % Get number of axis system.
            C.logs.no_axis_system = qmc_get_nb_axis_systems();
            if(C.logs.load_conf ~= 0)
                warndlg('Can''t start the communication with the LabCanBus.', 'Zyxfu error');
                return;
            end
            
            % Get number of axis system.
            C.logs.no_axis_system = qmc_get_nb_axis_systems();
            
            % Get the axis system handler.
            [C.logs.get_axissystem_h, C.axsys_h] = qmc_get_system_handle(0);
            
            % Verify that we have 3 axes available.
            C.logs.get_n_axes = qmc_get_nb_axis(C.axsys_h);
            
            % If we have the three axes.
            if(C.logs.get_n_axes == 3)
                % Retreive handles of all axes of the SystemAxis.
                [C.logs.res_get_ax0, C.ax0_h] = qmc_get_axis_handle(C.axsys_h, 0);
                [C.logs.res_get_ax1, C.ax1_h] = qmc_get_axis_handle(C.axsys_h, 1);
                [C.logs.res_get_ax2, C.ax2_h] = qmc_get_axis_handle(C.axsys_h, 2);
            else
                warndlg('Can''t start the communication with the LabCanBus.', 'Zyxfu error');
                return ;
            end
            
            % Enable all axes.
            if((C.logs.res_get_ax0 == 0) && (C.logs.res_get_ax1 == 0) && (C.logs.res_get_ax0 == 0))
                disp('Enable all axis');
                C.logs.res_enable_ax0 = qmc_enable_axis(C.ax0_h);
                C.logs.res_enable_ax1 = qmc_enable_axis(C.ax1_h);
                C.logs.res_enable_ax2 = qmc_enable_axis(C.ax2_h);
            end
            
            
            % Saving max position for x axis.
            [C.logs.get_max_pos_ax0, C.max_pos(1)] = qmc_get_max_pos(C.ax0_h);
            if(C.logs.get_max_pos_ax0 ~= 0)
                warndlg('Problem getting max x position.', 'Zyxfu warning');
                return ;
            end
            
            % Saving min position for x axis.
            [C.logs.get_min_pos_ax0, C.min_pos(1)] = qmc_get_min_pos(C.ax0_h);
            if(C.logs.get_min_pos_ax0 ~= 0)
                warndlg('Problem getting min x position.', 'Zyxfu warning');
                return ;
            end
            
            % Saving max velocity for x axis.
            [C.logs.get_max_vel_ax0, C.max_vel(1)] = qmc_get_max_vel(C.ax0_h);
            if(C.logs.get_max_vel_ax0 ~= 0)
                warndlg('Problem getting max x velocity.', 'Zyxfu warning');
                return ;
            end
            
            % Saving max position for y axis.
            [C.logs.get_max_pos_ax1, C.max_pos(2)] = qmc_get_max_pos(C.ax1_h);
            if(C.logs.get_max_pos_ax1 ~= 0)
                warndlg('Problem getting max x position.', 'Zyxfu warning');
                return ;
            end
            
            % Saving min position for y axis.
            [C.logs.get_min_pos_ax1, C.min_pos(2)] = qmc_get_min_pos(C.ax1_h);
            if(C.logs.get_min_pos_ax1 ~= 0)
                warndlg('Problem getting min x position.', 'Zyxfu warning');
                return ;
            end
            
            % Saving max velocity for y axis.
            [C.logs.get_max_vel_ax1, C.max_vel(2)] = qmc_get_max_vel(C.ax1_h);
            if(C.logs.get_max_vel_ax1 ~= 0)
                warndlg('Problem getting max x velocity.', 'Zyxfu warning');
                return ;
            end
            
            % Saving max position for z axis.
            [C.logs.get_max_pos_ax2, C.max_pos(3)] = qmc_get_max_pos(C.ax2_h);
            if(C.logs.get_max_pos_ax2 ~= 0)
                warndlg('Problem getting max x position.', 'Zyxfu warning');
                return ;
            end
            
            % Saving min position for z axis.
            [C.logs.get_min_pos_ax2, C.min_pos(3)] = qmc_get_min_pos(C.ax2_h);
            if(C.logs.get_min_pos_ax2 ~= 0)
                warndlg('Problem getting min x position.', 'Zyxfu warning');
                return ;
            end
            
            % Saving max velocity for z axis.
            [C.logs.get_max_vel_ax2, C.max_vel(3)] = qmc_get_max_vel(C.ax2_h);
            if(C.logs.get_max_vel_ax2 ~= 0)
                warndlg('Problem getting max x velocity.', 'Zyxfu warning');
                return ;
            end
            
            % Define the default velocities of the different axis.
            C.vel = C.max_vel / 2;
            
            %         % Set default position unit system for all axes to millimeter.
            %         errCode = calllib('labbCAN_MotionControl_API, 'LCA_SetDefaultPosUnit', N.axsys_h, N.ALL_AXIS, N.MM_UNIT);
            %         if(errCode ~= 0)
            %           warndlg('Problem setting default position unit.', 'Zyxfu warning');
            %         end
            %
            %         % Set default velocity unit system for all axes to millimeter per second.
            %         errCode = calllib('labbCAN_MotionControl_API, 'LCA_SetDefaultVelUnit', N.axsys_h, N.ALL_AXIS, N.VEL_MMS);
            %         if(errCode ~= 0)
            %           warndlg('Problem setting default velocity unit.', 'Zyxfu warning');
            %         end
        end
    end    
end

