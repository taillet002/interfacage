%--
% Class that give access to the neMAXYS hardware.
%
% 2015.09.29 - G. Lebrun.
%--
classdef neMAXYS < handle
  % Not accessible properties of the class.
  properties(SetAccess = public, GetAccess = public)
    % Path to the configuration.
    path_conf = '';
    
    %-- Set of handle for controling the different part of the zyxfu.
    
    % Handle on the LabCanAxisSystem axis system.
    axsys_h = [];
    
    % Handler on the 3 axes.
    ax0_h = [];
    ax1_h = [];
    ax2_h = [];
    
    % Handle on LabCanBus log file.
    hLog = [];
    
    % All logs of calls in a structure.
    logs = struct();
    
    %-- Set of data members link to geometry of the plateform.
	
    % Vector of axis maximum positions (for each 0 being min).
    max_pos = [0 0 0];
	
    % Vector of axis maximum positions (for each 0 being min).
    min_pos = [0 0 0];
    
    % Vector of axis minimum velocity (for each 0 being min).
    max_vel = [0 0 0];
    
    % Period of verification of the position of positions.
    check_period = 1;
    
    %-- Set of user defined characteristic of the table.
    
    % vector of chosen velocities.
    vel = [1 1 1];
  end
  
  
  
  % Accessible methods.
  methods
    function N = neMAXYS()
    % Constructor containing a long succession of command in order to state
    % the Zyxfu in a good state to start with.
    %
    % Parameters:
    %  - path_conf: path to the directory containing all the xml
    %               configuration files.
      
      % Get number of axis system.
      N.logs.no_axis_system = qmc_get_nb_axis_systems();
      
      % Get the axis system handler.
      [N.logs.get_axissystem_h, N.axsys_h] = qmc_get_system_handle(0);
      
      % Verify that we have 3 axes available.
      N.logs.get_n_axes = qmc_get_nb_axis(N.axsys_h);
      
      % If we have the three axes.
      if(N.logs.get_n_axes == 3)
        % Retreive handles of all axes of the SystemAxis.
        [N.logs.res_get_ax0, N.ax0_h] = qmc_get_axis_handle(N.axsys_h, 0);
        [N.logs.res_get_ax1, N.ax1_h] = qmc_get_axis_handle(N.axsys_h, 1);
        [N.logs.res_get_ax2, N.ax2_h] = qmc_get_axis_handle(N.axsys_h, 2);
      else
        warndlg('Can''t start the communication with the LabCanBus.', 'Zyxfu error');
        return ;
      end
      
      % Enable all axes.
      if((N.logs.res_get_ax0 == 0) && (N.logs.res_get_ax1 == 0) && (N.logs.res_get_ax0 == 0))
        disp('Enable all axis');
        N.logs.res_enable_ax0 = qmc_enable_axis(N.ax0_h);
        N.logs.res_enable_ax1 = qmc_enable_axis(N.ax1_h);
        N.logs.res_enable_ax2 = qmc_enable_axis(N.ax2_h);
      end
      
      
      % Saving max position for x axis.
      [N.logs.get_max_pos_ax0, N.max_pos(1)] = qmc_get_max_pos(N.ax0_h);
      if(N.logs.get_max_pos_ax0 ~= 0)
        warndlg('Problem getting max x position.', 'Zyxfu warning');
        return ;
      end
      
      % Saving min position for x axis.
      [N.logs.get_min_pos_ax0, N.min_pos(1)] = qmc_get_min_pos(N.ax0_h);
      if(N.logs.get_min_pos_ax0 ~= 0)
        warndlg('Problem getting min x position.', 'Zyxfu warning');
        return ;
      end
      
      % Saving max velocity for x axis.
      [N.logs.get_max_vel_ax0, N.max_vel(1)] = qmc_get_max_vel(N.ax0_h);
      if(N.logs.get_max_vel_ax0 ~= 0)
        warndlg('Problem getting max x velocity.', 'Zyxfu warning');
        return ;
      end
      
      % Saving max position for y axis.
      [N.logs.get_max_pos_ax1, N.max_pos(2)] = qmc_get_max_pos(N.ax1_h);
      if(N.logs.get_max_pos_ax1 ~= 0)
        warndlg('Problem getting max x position.', 'Zyxfu warning');
        return ;
      end
      
      % Saving min position for y axis.
      [N.logs.get_min_pos_ax1, N.min_pos(2)] = qmc_get_min_pos(N.ax1_h);
      if(N.logs.get_min_pos_ax1 ~= 0)
        warndlg('Problem getting min x position.', 'Zyxfu warning');
        return ;
      end
      
      % Saving max velocity for y axis.
      [N.logs.get_max_vel_ax1, N.max_vel(2)] = qmc_get_max_vel(N.ax1_h);
      if(N.logs.get_max_vel_ax1 ~= 0)
        warndlg('Problem getting max x velocity.', 'Zyxfu warning');
        return ;
      end
      
      % Saving max position for z axis.
      [N.logs.get_max_pos_ax2, N.max_pos(3)] = qmc_get_max_pos(N.ax2_h);
      if(N.logs.get_max_pos_ax2 ~= 0)
        warndlg('Problem getting max x position.', 'Zyxfu warning');
        return ;
      end
      
      % Saving min position for z axis.
      [N.logs.get_min_pos_ax2, N.min_pos(3)] = qmc_get_min_pos(N.ax2_h);
      if(N.logs.get_min_pos_ax2 ~= 0)
        warndlg('Problem getting min x position.', 'Zyxfu warning');
        return ;
      end
      
      % Saving max velocity for z axis.
      [N.logs.get_max_vel_ax2, N.max_vel(3)] = qmc_get_max_vel(N.ax2_h);
      if(N.logs.get_max_vel_ax2 ~= 0)
        warndlg('Problem getting max x velocity.', 'Zyxfu warning');
        return ;
      end
      
      % Define the default velocities of the different axis.
      N.vel = N.max_vel / 2;
        
%         % Set default position unit system for all axes to millimeter.
%         errCode = calllib('labbCAN_MotionControl_API, 'LCA_SetDefaultPosUnit', N.axsys_h, N.ALL_AXIS, N.MM_UNIT);
%         if(errCode ~= 0)
%           warndlg('Problem setting default position unit.', 'Zyxfu warning');
%         end
%         
%         % Set default velocity unit system for all axes to millimeter per second.
%         errCode = calllib('labbCAN_MotionControl_API, 'LCA_SetDefaultVelUnit', N.axsys_h, N.ALL_AXIS, N.VEL_MMS);
%         if(errCode ~= 0)
%           warndlg('Problem setting default velocity unit.', 'Zyxfu warning');
%         end
    end
    
    
    
    function go_home(N)
    % Move plate to home position.
      
      % dll call to ask the Zyxfu to go at home position.
      N.logs.allax_homing = qmc_find_home(N.axsys_h);
      if(N.logs.allax_homing ~= 0)
        warndlg('Fail to initiate the movement to home position.', 'Zyxfu warning');
      end
    end
    
    
    
    function x_go_home(N)
    % Move plate to home position for x axis.
      
      % dll call to ask the Zyxfu to go at home position.
      N.logs.ax0_homing = qmc_home_axis(N.ax0_h);
      if(N.logs.ax0_homing ~= 0)
        warndlg('Problem while x axis is trying to go home.', 'Zyxfu warning');
      end
    end
    
    
    
    function y_go_home(N)
    % Move plate to home position for y axis.
      
      % dll call to ask the Zyxfu to go at home position.
      N.logs.ax1_homing = qmc_home_axis(N.ax1_h);
      if(N.logs.ax1_homing ~= 0)
        warndlg('Problem while y axis is trying to go home.', 'Zyxfu warning');
      end
    end
    
    
    
    function z_go_home(N)
    % Move plate to home position for z axis.
      
      % dll call to ask the Zyxfu to go at home position.
      N.logs.ax2_homing = qmc_home_axis(N.ax2_h);
      if(N.logs.ax2_homing ~= 0)
        warndlg('Problem while z axis is trying to go home.', 'Zyxfu warning');
      end
    end
    
    
    
    function move_x(N, x)
    % Move to x (position from reference).
      
      % Execute a movement that occurs.
      qmc_move_2_pos(N.ax0_h, x, N.vel(1));
    end
    
    
    
    
    function move_dx(N, dx)
    % Movement of dx from actual position.
      
      % Execute the displacement.
      qmc_move_distance(N.ax0_h, dx, N.vel(1));
    end
      
    
    
    
    function move_y(N, y)
    % Move to y (position from reference).
    
      % Execute a movement that occurs.
      qmc_move_2_pos(N.ax1_h, y, N.vel(2));
    end
    
    
    
    
    function move_dy(N, dy)
    % Movement of dy from actual position.
    
      % Execute the displacement.
      qmc_move_distance(N.ax1_h, dy, N.vel(2));
    end
    
        
    
    function move_z(N, z)
    % Move to z (position from reference).
    
      % Execute a movement that occurs.
      qmc_move_2_pos(N.ax2_h, z, N.vel(3));
    end
    
    
    
    function move_dz(N, dz)
    % Movement of dz from actual position.
    
      % Execute the displacement.
      qmc_move_distance(N.ax2_h, dz, N.vel(3));
    end
    
    
    
    function v = get_max_vel(N)
    % A simple getter to obtain the maximum velocities useable with the
    % plateform.
    %
    % Return:
    %  - v: a vector with maximum velocities in x, y, and N.
    
      v = N.max_vel;
    end
    
    
    
    function p = get_max_pos(N)
    % A simple getter to obtain the maximum position useable with the
    % plateform.
    %
    % Return:
    %  - v: a vector with maximum positions in x, y, and z (minimum being
    %       [0, 0, 0]).
      
      p = N.max_pos;
    end
    
    
    
    function v = get_velocities(N)
    % Simple getter to obtain the currently defined velocities.
    %
    % Return:
    %  - v: a vector with velocities in x, y, and N.
    
      v = N.vel;
    end
    
    
    
    function set_velocities(N, v)
    % Renew the values of current velocities. They will be taken into
    % account at the next movement. All negatives components of the vector
    % will be changed to zero and all value superior to the maximum
    % allowed will be changee to the maximu value.
    %
    % Return:
    %  - v: the vector of velocities.
    
      % Set of components under 0.
      nv = [0, 0, 0];
      u = v < nv;
      
      % Set of components over maximum.
      o = v > N.max_vel;
      
      if(sum([u, o]) > 0)
        warndlg('Be carefull, components some components of the vector are out of range.', 'Zyxfu warning');
      end
        
      % Rectification of the components to keep something edible.
      v(u) = nv(u);
      v(o) = N.max_vel(o);
      
      % We update the vector in the class.
      N.vel = v;
    end
    
    
    
    function wait_for_x(N)
    % Function waiting for all axis to finish their moves.
    
      res = 0;
      
      while(res ~= 0)
        % Is X Axis position reached.
        res = qmc_is_pos_reached(N.ax0_h);
        
        % A pause needed for the while loop to not take all processor
        % ressources.
        pause(N.check_period);
      end
    end
    
    
    
    function wait_for_y(N)
    % Function waiting for all axis to finish their moves.
      
      res = 0;
      
      while(res ~= 0)
        % Is Y Axis position reached.
        res = qmc_is_pos_reached(N.ax1_h);
        
        % A pause needed for the while loop to not take all processor
        % ressources.
        pause(N.check_period);
      end
    end
    
    
    
    function wait_for_z(N)
    % Function waiting for all axis to finish their moves.
      
      res = 0;
      
      while(res ~= 0)
        % Is Z Axis position reached.
        res = qmc_is_pos_reached(N.ax2_h);
        
        % A pause needed for the while loop to not take all processor
        % ressources.
        pause(N.check_period);
      end
    end
    
    
    
    function delete(N)
    % Destructor that free all resources on the Zyxfu control.
    
      % Stop the bus.
      qb_stop_bus();
    
      % Close the labbCAN_Bus.
      qb_close_bus();
    end
    
    
    
    function disp(N)
    % 'disp' overloading in order to display important aspect of the Zyxfu
    % class.
      
      disp('Handle on the log file.');
      disp(N.hLog);
      
      disp('Vector of [x, y, z] max positions.');
      disp(N.max_pos);
      
      disp('Vector of [x, y, z] min positions.');
      disp(N.min_pos);
      
      disp('Vector of [vx, vy, vz] max velocities.');
      disp(N.max_vel);
      
      disp('All logs');
      disp(N.logs);
      if(isfield(N.logs, 'load_lcb'))
        disp('labbCAN_Bus_API not loaded functions:');
        disp(N.logs.load_lcb);
      end
      if(isfield(N.logs, 'load_lca'))
        disp('labbCAN_MotionControl_API not loaded functions:');
        disp(N.logs.load_lca);
      end
      
      disp('Handlers:');
      disp(N.ax0_h);
      disp(N.ax1_h);
      disp(N.ax2_h);
    end
  end
end

