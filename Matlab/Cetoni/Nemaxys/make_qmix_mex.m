% Set of command to compile all mex files linked to Qmix library.
%
% 2016.04.05 Solvay - G. Lebrun

% Define the different path and 
headers = '-I"C:\Users\glebrun\Documents\Outils\Qmix\QmixSDK-64bit\include"';
lib_path = '-L"C:\Users\glebrun\Documents\Outils\Qmix\QmixSDK-64bit"';
dll_path = '-L"C:\Users\glebrun\Documents\Outils\Qmix\QmixSDK-64bit\lib\mingw"';
lcan_bus = '-llabbCAN_Bus_API';
lcan_motion = '-llabbCAN_MotionControl_API';
lcan_pumps = '-llabbCAN_Pump_API';

%-- Set of bus functions.
disp('-- labbCAN_Bus_API functions');

% Compile the function to open and start a bus.
disp('Compile qb_init_bus');
mex(headers, lib_path, dll_path, lcan_bus, 'qb_init_bus.c');

% Compile the function that stop a bus.
disp('Compile qb_stop_bus');
mex(headers, lib_path, dll_path, lcan_bus, 'qb_stop_bus.c');

% Compile the function to close a bus.
disp('Compile qb_close_bus');
mex(headers, lib_path, dll_path, lcan_bus, 'qb_close_bus.c');


%-- Set of neMAXYS functions.
disp('-- labbCAN_MotionControl_API functions');

% Compile the function to get the neMAXYS Axis system handler.
disp('Compile qmc_get_system_handle');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_get_system_handle.c');

% Compile the function to get the number of axis systems.
disp('Compile qmc_get_nb_axis_systems');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_get_nb_axis_systems.c');

% Compile the function to get the number of neMAXYS axis.
disp('Compile qmc_get_nb_axis');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_get_nb_axis.c');

% Compile the function to get the number of neMAXYS axis.
disp('Compile qmc_get_axis_handle');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_get_axis_handle.c');

% Compile the function to enable an axis of neMAXYS.
disp('Compile qmc_enable_axis');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_enable_axis.c');

% Compile the function to search homing position of each axis of the neMAXYS.
disp('Compile qmc_find_home');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_find_home.c');

% Compile the function to search homing position of one axis of the neMAXYS.
disp('Compile qmc_home_axis');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_home_axis.c');

% Compile the function to get max position of one axis of the neMAXYS.
disp('Compile qmc_get_max_pos');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_get_max_pos.c');

% Compile the function to get min position of one axis of the neMAXYS.
disp('Compile qmc_get_min_pos');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_get_min_pos.c');

% Compile the function to get max velocity of one axis of the neMAXYS.
disp('Compile qmc_get_max_vel');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_get_max_vel.c');

% Compile the function to move to a position of one axis of the neMAXYS.
disp('Compile qmc_move_2_pos');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_move_2_pos.c');

% Compile the function to move to of a distance on one axis of the neMAXYS.
disp('Compile qmc_move_distance');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_move_distance.c');

% Compile the function to check if one position has been reached on one axis of the neMAXYS.
disp('Compile qmc_is_pos_reached');
mex(headers, lib_path, dll_path, lcan_motion, 'qmc_is_pos_reached.c');


%-- Set of neMESYS functions.
disp('-- labbCAN_Pump_API functions');

% Compile the function to retreive the number of pumps connected to a neMESYS.
disp('Compile qp_get_nb_pumps');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_get_nb_pumps.c');

% Compile the function to retreive the handle of a pump connected to a neMESYS.
disp('Compile qp_get_pump_handle');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_get_pump_handle.c');

% Compile the function to clear faults linked to a pump.
disp('Compile qp_clear_fault');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_clear_fault.c');

% Compile the function to retreive the handle of a pump connected to a neMESYS.
disp('Compile qp_enable_pump');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_enable_pump.c');

% Compile the function to ask if a pump is available.
disp('Compile qp_is_enable');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_is_enable.c');

% Compile the function to calibrate the syringe pump.
disp('Compile qp_syringe_calibrate');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_syringe_calibrate.c');

% Compile the function to define the volume unit.
disp('Compile qp_set_volume_unit');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_set_volume_unit.c');

% Compile the function to define the flow unit.
disp('Compile qp_set_flow_unit');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_set_flow_unit.c');

% Compile the function to define the syringe parameters.
disp('Compile qp_set_syr_params');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_set_syr_params.c');

% Compile the function to restore the position counter.
disp('Compile qp_restore_dpc');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_restore_dpc.c');

% Compile the function to aspirate with a pump.
disp('Compile qp_aspirate');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_aspirate.c');

% Compile the function to dispense with a pump.
disp('Compile qp_dispense');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_dispense.c');

% Compile the function to get the max flow rate of the pump.
disp('Compile qp_get_max_flow_rate');
mex(headers, lib_path, dll_path, lcan_pumps, 'qp_get_max_flow_rate.c');

