%-%
% Class to control HPLC Jasco pump PU-4086
% October 2018 Solvay - M. D�niel
% Based on Jasco Model PU-2080 Series
% Intelligent HPLC Pump RS-232C Communication Protocol
% Reference Manual
% The cable must be cross-wired (e.g.
% https://fr.rs-online.com/web/p/cordons-serie/0812926/)
% The pump RS-232c configuration must be set to LC-2000
%-%

classdef Jasco_pump < handle
    properties(SetAccess = private, GetAccess = private)
        % is simulation mode on ?
        simulation_mode = false;
        
        % [set point target, measure]
        val = [];
        
        % Keep track of the serial connection used.
        ComPort = 'COM5';
        
        % Handle to the serial connection
        sc = [];
    end
    
    methods(Access = public)
        function P = Jasco_pump(com_port)
            % Initialization of the interface by setting the com
            % port with which we are supposed to discuss.
            %
            % Parameter:
            %  - com_port: string defining the com port of the XY table.
            
            % To keep track of the com port used.
            P.ComPort = com_port;
            
            % Creation of the serial interface.
            try P.sc = serial(P.ComPort, 'BaudRate', 4800,...
                    'Parity', 'none',...
                    'DataBits', 8, ...
                    'StopBits',2,...
                    'FlowControl','Software',...
                    'Timeout',2,...
                    'Terminator','CR');
                
                % Open the connection.
                fopen(P.sc);
%                 fscanf(L.sc);
                disp('Jasco pump port com open')
                
            catch % if it's not possible to launch serial -> simulation_mode ON
                % Simulation mode will send random values
                
                P.simulation_mode = true;
                disp('Pb with serial com. simulation mode : ON');
                
            end
        end
                
        function disp(P)
            % Display information of the serial port used.
            disp('--- Jasco_pump PU-4086 ---');
            disp(['Com port used (B 4800, P none, DB 8, Trm CR): ', P.ComPort]);
        end
                
        function delete(P)
            % Every action to apply before quiting.
            % Close the serial handler.
            fclose(P.sc);
            delete(P.sc);
        end
    
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

% Start/stop the pump

         function start_pump(P)
             %Function to start the flow
             fprintf(P.sc, '0 pump set')
         end
         
         function stop_pump(P)
             %Function to start the flow
             fprintf(P.sc, '1 pump set')
         end
         
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

%Setting commands     

        function set_flowrate(P, flowrate)
            % Specify a value of flowrate (mL/min) to the pump
            % flowrate is a numeric value of the form X.XXX ml/min
            command = [num2str(flowrate),' flowrate set'];
            fprintf(P.sc,command);
        end

        function set_maxpress(P,maxpress)
            % Specify the value of the maxium pressure (default is 500
            % bar)
            % maxpress is a numeric value of the form XXX bar
            command = [num2str(maxpress),' pmax set'];
            fprintf(P.sc,command);
        end
        
        function set_minpress(P,minpress)
            % Specify the value of the maximum pressure (default is 0 bar)
            % maxpress is a numerical value of the form XXX bar
            command = [num2str(minpress),' pmin set'];
            fprintf(P.sc,command);
        end
        
        function set_deliverymode(P,delivery_mode)
            % Specify the delivery mode (CF : constant flow, 
            % CP : constant pressure)
            % delivery_mode is a numerical value : 0 is constant flow,
            % 1 is constant pressure mode
            command = [num2str(delivery_mode),' cfcp set'];
            fprintf(P.sc, command);
        end
        
        function set_pump_timeoff(P,timeoff)
            % Specify the pump-off timer to XX.X hours
            % time-off is a numerical value ranging from 0.0 to 99.9 hours
            command = [num2str(timeoff),' offtimer set'];
            fprintf(P.sc, command);
        end
        
        function set_pump_timeon(P,timeon)
            % Specify the pump-on timer to XX.X hours
            % time-on is a numerical value ranging from 0.0 to 99.9 hours
            command = [num2str(timeon),' ontimer set'];
            fprintf(P.sc, command);
        end
        
        function select_gradient_mode(P, gradient_mode)
            % Specify the gradient mode (high or low pressure)
            % gradient_mode is a numerical value : 0 is high pressure,
            % 1 is low pressure
            command = [num2str(gradient_mode),' gradmode set'];
            fprintf(P.sc, command);
        end
        
        function set_pressure(P, pressure)
            % Specify the pressure setpoint in constant pressure mode
            % pressure is a numerical value ranging from 0 to 500 bar in
            % steps of 1 bar
            command = [num2str(pressure),' press set'];
            fprintf(P.sc, command);
        end
        
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
        
%Reading commands

         function val=read_flowrate(P)
            % Read current flowrate (mL/min)
            fprintf(P.sc,'a_flow load p');
            val = str2num(fscanf(P.sc));
         end
        
         function val = read_pressure(P)
             % Read current pressure (bar)
             fprintf(P.sc,'a_press1 load p');
             val = fscanf(P.sc);
         end
         
         function read_maximum_pressure(P)
            % Read maximum pressure setting (bar)
            fprintf(P.sc,'a_pmax load p');
            fscanf(P.sc)
         end
         
         function read_minimum_pressure(P)
             % Read minimum pressure setting (bar)
             flushinput(P.sc);
             flushoutput(P.sc);
             fprintf(P.sc,'a_pmin load p');
             fscanf(P.sc)
         end
         
         function read_elapsed_time(P)
             % Read elapsed time (seconds) since pump on
             fprintf(P.sc,'current_time load p');
             fscanf(P.sc)
         end
         
         function read_trouble(P)
             % Read error messages when trouble occurs
             % 0 : no trouble
             % 1 : Under pressure
             % 2 : Over pressure
             % 4 : Ext. stop in
             % 8 : Pump timer off
             fprintf(P.sc,'trouble load p');
             fscanf(P.sc)
         end
         
         function clear_error(P)
             % Clear the trouble status
             fprintf(P.sc,'0 trouble set');
             fscanf(P.sc)
         end
          
    end %end Methods
end %end classdef