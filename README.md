# Objectives #

This repository aims at sharing codes developped for devices interfacing mainly on Python and Matlab platform.

## Current codes available ##

### Matlab ###
- Acquisition class -> store and display acquired data and export XLS file
- Alicat PCD Series -> Control the pressure controler 
- Arduino Card -> read/write analog&digital ports with specific protocols SPI, I2C etc. (usefull for small sensors as thermocouple etc.)
- Avantes Spectro -> Control and read avantes spectrometers
- Cetoni Nemesys syringe pumps (MATLAB) -> Control Nemesys serynge pump
- Hasahi Solar Lamp -> Switch on/off the solar simulator through RS232 connection
- Jasco Pump -> Control the liquid jasco pump
- Keithley 2400 & 2700 -> Control keithley apparatus for voltage/intensity reading and writing
- MiniCoreiFlow -> Control Bronkhorst flowmeter


### Python ###
- Universal Robot -> Control the universal robo from python
- C-Series Pump -> Control the C-Series pump from python
- uARM Robot -> Control uArm Robot
- pHProbe -> Read pH probe through Atlas card
- Camera -> Image acquisition of webcam

## Repository organization ##

Codes are grouped as followed:  
.Platform name ("Matlab" or "Python")  
..Device Name 

Ideally, each "Device Name" subfolder should contain a readme file exhibiting the basic function to use the code.
